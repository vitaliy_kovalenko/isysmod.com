package com.isysmod.services.rest;

import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.annotation.Priority;
import javax.ws.rs.Consumes;
import javax.ws.rs.Priorities;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.Marshaller;

import org.apache.cxf.jaxrs.provider.JAXBElementProvider;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;

import com.isysmod.services.jaxb.adapters.JoinListener;

@Provider
@Produces({"application/xml", "application/*+xml", "text/xml"})
@Consumes({"application/xml", "application/*+xml", "text/xml"})
@Priority(Priorities.ENTITY_CODER)
public class ExtendedJAXBElementProvider<T> extends JAXBElementProvider<T> {
	
	@Override
	protected void marshal(Object obj, Class<?> cls, Type genericType, 
            String enc, OutputStream os, 
            Annotation[] anns, MediaType mt, Marshaller marshaller) throws Exception {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		JoinListener joinListener = message.getExchange().get(JoinListener.class);
		
		if (joinListener != null) {
			marshaller.setListener(joinListener);
		}
		
		super.marshal(obj, cls, genericType, enc, os, anns, mt, marshaller);
	}

}
