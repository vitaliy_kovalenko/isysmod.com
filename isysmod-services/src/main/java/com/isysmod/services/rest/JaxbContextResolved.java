package com.isysmod.services.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Priority;
import javax.ws.rs.Consumes;
import javax.ws.rs.Priorities;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.security.SecurityContext;

import com.isysmod.common.IC;
import com.isysmod.common.security.ClientPrincipal;

@Provider
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_XML})
@Priority(Priorities.ENTITY_CODER)
public class JaxbContextResolved implements ContextResolver<JAXBContext> {

	private Map<String, JAXBContext> contextPool;
	
	public JaxbContextResolved() {
		contextPool = new HashMap<>();
	}
	
	@Override
	public JAXBContext getContext(Class<?> type) {
		JAXBContext jaxbContext;
		Message message = PhaseInterceptorChain.getCurrentMessage();
		SecurityContext sc = message.getExchange().get(SecurityContext.class);
		ClientPrincipal principal = (ClientPrincipal) sc.getUserPrincipal();
		String packages = IC.ISC_COMMON_JAXB_PACKAGE + ":" + IC.ISC_COMMON_MODEL_PACKAGE;
		List<String> datasources = principal.getDatasources();
		
		for (String factoryName : datasources) {
			packages += ":" + IC.ISYS_ORM_BASE_PACKAGE + "." + factoryName;
		}
		
		synchronized (contextPool) {
			if (! contextPool.containsKey(packages)) {
				try {
					jaxbContext = JAXBContext.newInstance( packages );
				} catch (JAXBException e) {
					throw new WebApplicationException(e);
				}
				
			    contextPool.put(packages, jaxbContext);
			}
			
			jaxbContext = contextPool.get(packages);
		}
		
		return jaxbContext;
	}
	
	public void rebuildJaxbContext(String factoryName) {
		List<String> keys = new ArrayList<>();
		
		synchronized (contextPool) {
			for (String key : contextPool.keySet()) {
				String[] packages = key.split(":");
				
				for (String pack : packages) {
					if (pack.endsWith("." + factoryName)) {
						keys.add(key);
					}
				}
			}
			
			for (String key : keys) {
				contextPool.remove(key);
			}
		}
	}

}
