//package com.isysmod.services.rest;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.ext.Provider;
//
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.jaxrs.cfg.Annotations;
//
///***
// *	<!-- JAXB -->
// *	<bean id="xmlMapper" class="com.fasterxml.jackson.dataformat.xml.XmlMapper"/>
// *	<bean id="xmlMapperConfigurator" class="com.fasterxml.jackson.jaxrs.xml.XMLMapperConfigurator">
// *	    <constructor-arg ref="xmlMapper" />
// *	    <constructor-arg>
// *	        <util:constant static-field="com.isysmod.services.rest.JacksonJaxbXMLProvider.JAXB_ANNOTATIONS"/>
// *	    </constructor-arg>
// *	    <property name="annotationsToUse">
// *	        <util:constant static-field="com.isysmod.services.rest.JacksonJaxbXMLProvider.JAXB_ANNOTATIONS"/>
// *	    </property>     
// *	</bean>
// *	<bean
// *	    class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
// *	    <property name="targetObject" ref="xmlMapper" />
// *	    <property name="targetMethod" value="enable" />
// *	    <property name="arguments">
// *	        <util:constant static-field="com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator.Feature.WRITE_XML_DECLARATION"/>
// *	    </property>
// *	</bean>
// *	<bean id="jacksonJaxbXMLProvider" class="com.isysmod.services.rest.JacksonJaxbXMLProvider">
// *	    <constructor-arg name="mapper" ref="xmlMapper" />
// *	    <constructor-arg name="formattedOutput" value="true" />
// *	</bean>
// *	<bean id="jacksonJaxbJsonProvider" class="com.isysmod.services.rest.JacksonJaxbJsonProvider">
// *		<constructor-arg name="formattedOutput" value="true" />
// *	</bean>
// */
//
//@Provider
//@Consumes(MediaType.WILDCARD)
//@Produces(MediaType.APPLICATION_JSON)
//public class JacksonJaxbJsonProvider extends com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider {
//
//	public final static Annotations[] DEFAULT_ANNOTATIONS = {
//        Annotations.JAXB
//    };
//	
//	public JacksonJaxbJsonProvider() {
//        super(null, DEFAULT_ANNOTATIONS);
//    }
//	
//	public JacksonJaxbJsonProvider(boolean formattedOutput) {
//		this();
//		
//		if (formattedOutput) {
//    		enable(SerializationFeature.INDENT_OUTPUT);
//    	}
//	}
//
//}
