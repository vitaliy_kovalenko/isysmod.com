package com.isysmod.services.rest;

import javax.persistence.EntityManager;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;

import com.isysmod.common.security.ClientPrincipal;

public class CloseSessionOutInterceptor extends AbstractPhaseInterceptor<Message> {
	
	public CloseSessionOutInterceptor() {
		super(Phase.MARSHAL_ENDING);
	}

	@Override
	public void handleMessage(Message message) throws Fault {
		SecurityContext sc = message.getExchange().get(SecurityContext.class);

		if (sc != null) {
			ClientPrincipal principal = (ClientPrincipal) sc.getUserPrincipal();
			
			for (EntityManager manager : principal.getOpenedSessions()) {
				if (manager.isOpen()) {
					manager.close();
				}
			}
		}
	}

}
