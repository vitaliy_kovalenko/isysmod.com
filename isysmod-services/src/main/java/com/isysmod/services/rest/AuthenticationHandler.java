package com.isysmod.services.rest;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.security.SecurityContext;
import org.apache.log4j.Logger;

import com.isysmod.common.jdo.DatasourceManager;
import com.isysmod.common.model.Client;
import com.isysmod.common.model.Database;
import com.isysmod.common.security.ClientPrincipal;
import com.isysmod.common.security.HMacEncoder;
import com.isysmod.services.dao.ClientDao;

/**
 * <p><code>Authorization: Digest
 *	username="apiKey",
 *	realm="host.com",
 *	nonce="base64(secret)",
 *	uri="/dir/index.html",
 *	response="signature"</code></p>
 * <p><code>WWW-Authenticate: Digest
 *	realm="host.com",
 *	nonce="base64(secret)"</code></p>
 *
 * <p><a href="https://www.ietf.org/rfc/rfc2617.txt">https://www.ietf.org/rfc/rfc2617.txt</a></p>
 * @author vitaliy
 *
 */
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationHandler implements ContainerRequestFilter {
	
	private static final Logger LOGGER = Logger.getLogger(AuthenticationHandler.class);
	
	@Context
	private HttpServletRequest servletRequest;
	
	@Inject
	private DatasourceManager datasourceManager;
	
	@Inject
	private ClientDao clientDao;
	
	@Inject
	private HMacEncoder macEncoder;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Map<String,String> credentials = parseAuthorizationHeader(requestContext);
		
		if (!isAllCredentialsExists(credentials)) {
			requestContext.abortWith(createFaultResponse(Status.BAD_REQUEST, null, null));
			return;
		}
		
        String realm = credentials.get("realm");
        String uri = credentials.get("uri");
        String serverName = servletRequest.getServerName();
        String equestURI = servletRequest.getRequestURI();
        
        if (!realm.equals(serverName) || !uri.equals(equestURI)) {
        	requestContext.abortWith(createFaultResponse(Status.BAD_REQUEST, null, null));
			return;
        }
        
        final String newSecret = UUID.randomUUID().toString();
        final String authHeader = createAuthHeader(newSecret);
        final Cookie authCookie = createAuthCookie(newSecret);

        String apiKey = credentials.get("username");
        String signature = credentials.get("response");
        String nonce = credentials.get("nonce");
        
        try {
        	nonce = new String(Base64.getDecoder().decode(nonce.getBytes(HMacEncoder.CHARSET)));
        } catch (IllegalArgumentException e) {
        	LOGGER.info(e);
        	requestContext.abortWith(createFaultResponse(Status.BAD_REQUEST, null, null));
			return;
		}
        
        String secret = null;
        String secretKey = null;
        Client client = null;
        Exception exception = null;
        
        EntityManager entityManager = (EntityManager) datasourceManager.openDefaultSession();
        
        try {
        	client = clientDao.fetchByApiKey(entityManager, apiKey);
        	
        	if (client != null) {
        		entityManager.getTransaction().begin();
    			secret = client.getSecret();
    			secretKey = client.getSecretKey();
    			client.setSecret(newSecret);
    			entityManager.getTransaction().commit();
    		}
        } catch (Exception e) {
        	exception = e;
        	LOGGER.error(e);
        }
		
        if (exception != null || client == null || !nonce.equals(secret)) {
        	entityManager.close();
			requestContext.abortWith(createFaultResponse(Status.UNAUTHORIZED, authHeader, authCookie));
			return;
		}
        
        try {
	        if (isAuthenticated(secretKey, secret, signature)) {
	        	final ClientPrincipal principal = new ClientPrincipal(client.getId(), datasourceManager, authHeader);
	        	principal.setAuthCookie(authCookie);
	        	
	        	for (Database datasource : client.getDatabases()) {
	        		principal.getDatasources().add(datasource.getCode());
				}
	
	        	final SecurityContext sc = new SecurityContext() {
	        		
					@Override
					public Principal getUserPrincipal() {
						return principal;
					}
	
					@Override
					public boolean isUserInRole(final String role) {
						return false;
					}
				};
				
	        	PhaseInterceptorChain.getCurrentMessage().getExchange().put(SecurityContext.class, sc);
	        	LOGGER.debug("Client successfully authenticated.");
	        } else {
	            requestContext.abortWith(createFaultResponse(Status.UNAUTHORIZED, authHeader, authCookie));
	            LOGGER.debug("Client authentication failed.");
	        }
        } catch (Exception e) {
        	LOGGER.error(e);
        } finally {
        	entityManager.close();
		}
    }
	
	private boolean isAllCredentialsExists(Map<String,String> credentials) {
		if (
				credentials == null ||
				!credentials.containsKey("username") ||
				!credentials.containsKey("realm") ||
				!credentials.containsKey("nonce") ||
				!credentials.containsKey("uri") ||
				!credentials.containsKey("response")) {
			return false;
		}
		
		String username = credentials.get("username");
        String realm = credentials.get("realm");
        String nonce = credentials.get("nonce");
        String uri = credentials.get("uri");
        String response = credentials.get("response");
        
        if (
        		username == null || username.isEmpty() ||
        		realm == null || realm.isEmpty() ||
        		nonce == null || nonce.isEmpty() ||
        		uri == null || uri.isEmpty() ||
        		response == null || response.isEmpty()) {
			return false;
		}
        
        return true;
	}
	
    private boolean isAuthenticated(String secretKey, String secret, String signature) {
    	boolean matches = false;
    	
    	try {
			matches = macEncoder.matches(secretKey, secret, signature);
		} catch (InvalidKeyException | NoSuchAlgorithmException e) {
			LOGGER.info(e);
		}
    	
		return matches;
	}
	
	private String createAuthHeader(String secret) {
		return macEncoder.createAuthHeader(servletRequest.getServerName(), secret);
	}
	
	private Cookie createAuthCookie(String secret) {
		return macEncoder.createAuthCookie(secret);
	}
    
	private Response createFaultResponse(Status status, String authHeader, Cookie authCookie) {
		ResponseBuilder responseBuilder;
		
		if (Status.BAD_REQUEST.equals(status)) {
			responseBuilder = Response.status(status);
		} else {
			responseBuilder = Response.status(status)
					.header(HttpHeaders.WWW_AUTHENTICATE, authHeader)
					.cookie(new NewCookie(
							authCookie.getName(),
							authCookie.getValue(),
							authCookie.getPath(),
							authCookie.getDomain(),
							authCookie.getComment(),
							authCookie.getMaxAge(),
							authCookie.getSecure(),
							authCookie.isHttpOnly()));
		}
		
        return responseBuilder.build();
    }
	
	private HashMap<String, String> parseAuthorizationHeader(ContainerRequestContext requestContext) {
		String auth = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		
		if (auth == null) {
            return null;
        }
		
        String headerStringWithoutScheme = auth.substring(auth.indexOf(" ") + 1).trim();
        HashMap<String, String> values = new HashMap<String, String>();
        String keyValueArray[] = headerStringWithoutScheme.split(",");
        
        for (String keyval : keyValueArray) {
            if (keyval.contains("=")) {
                String key = keyval.substring(0, keyval.indexOf("="));
                String value = keyval.substring(keyval.indexOf("=") + 1);
                values.put(key.trim(), value.replaceAll("\"", "").trim());
            }
        }
        
        return values;
    }

}
