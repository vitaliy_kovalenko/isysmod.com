package com.isysmod.services.rest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;
import org.apache.cxf.transport.http.AbstractHTTPDestination;

import com.isysmod.common.security.ClientPrincipal;

public class CookieOutInterceptor extends AbstractPhaseInterceptor<Message> {

	public CookieOutInterceptor() {
		super(Phase.PREPARE_SEND);
	}

	@Override
	public void handleMessage(Message message) throws Fault {
		SecurityContext sc = message.getExchange().get(SecurityContext.class);

		if (sc != null) {
			ClientPrincipal principal = (ClientPrincipal) sc.getUserPrincipal();
			Cookie cookie = principal.getAuthCookie();
			
			if (cookie != null) {
				HttpServletResponse response = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);
				response.addCookie(cookie);
			}
		}
	}

}
