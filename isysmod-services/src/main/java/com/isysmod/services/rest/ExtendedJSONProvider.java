//package com.isysmod.services.rest;
//
//import java.io.OutputStream;
//import java.lang.reflect.Type;
//
//import javax.annotation.Priority;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.Priorities;
//import javax.ws.rs.Produces;
//import javax.ws.rs.ext.Provider;
//import javax.xml.bind.Marshaller;
//
//import org.apache.cxf.jaxrs.provider.json.JSONProvider;
//import org.apache.cxf.message.Message;
//import org.apache.cxf.phase.PhaseInterceptorChain;
//
//import com.isysmod.services.jaxb.adapters.JoinListener;
//
//@Provider
//@Produces({"application/json", "application/*+json"})
//@Consumes({"application/json", "application/*+json"})
//@Priority(Priorities.ENTITY_CODER)
//public class ExtendedJSONProvider<T> extends JSONProvider<T> {
//
//	@Override
//	protected void marshal(Marshaller marshaller, Object actualObject, Class<?> actualClass, 
//            Type genericType, String enc, OutputStream os, boolean isCollection) throws Exception {
//		Message message = PhaseInterceptorChain.getCurrentMessage();
//		JoinListener joinListener = message.getExchange().get(JoinListener.class);
//		marshaller.setListener(joinListener);
//		
//		super.marshal(marshaller, actualObject, actualClass, genericType, enc, os, isCollection);
//	}
//
//}
