package com.isysmod.services.rest;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.security.SecurityContext;
import org.apache.cxf.transport.http.AbstractHTTPDestination;

import com.isysmod.common.security.ClientPrincipal;

public class AuthenticationHeaderInterceptor extends AbstractPhaseInterceptor<Message> {

	public AuthenticationHeaderInterceptor() {
		super(Phase.PREPARE_SEND);
	}

	@Override
	public void handleMessage(Message message) throws Fault {
		SecurityContext sc = message.getExchange().get(SecurityContext.class);

		if (sc != null) {
			HttpServletResponse response = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);
			ClientPrincipal principal = (ClientPrincipal) sc.getUserPrincipal();
			response.addHeader(HttpHeaders.WWW_AUTHENTICATE, principal.getAuthHeader());
		}
	}

}
