package com.isysmod.services.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.cxf.rs.security.cors.CrossOriginResourceSharing;

import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.jaxb.isc.IscRequest;
import com.isysmod.common.jaxb.isc.IscRequestBatch;
import com.isysmod.common.jaxb.isc.IscResponse;
import com.isysmod.common.jaxb.isc.IscResponseBatch;
import com.isysmod.common.jaxb.isc.IscSingleRequest;
import com.isysmod.common.model.EntityBean;
import com.isysmod.services.service.ApiService;

@CrossOriginResourceSharing(
		allowAllOrigins = true,
		allowCredentials = true,
		exposeHeaders = {"www-authenticate", "content-type"},
		maxAge = 3600  // indicates how long the response can be cached,
					// so that for subsequent requests, within the specified time,
					// no preflight request has to be made
)
public class Dispatcher<E extends EntityBean> {
	
	@Inject
	private ApiService<E> apiService;
	
	@POST
	@Path("/")
	@Consumes({"application/xml", "application/*+xml", "text/xml", "application/json", "application/*+json"})
	@Produces({"application/xml", "application/*+xml", "text/xml", "application/json", "application/*+json"})
	public Response api(IscRequestBatch<E> requestBatch) {
		IscResponseBatch<E> responseBatch;

		if (requestBatch.isTransactional()) {
			responseBatch = apiService.transactionalBatch(requestBatch);
		} else {
			responseBatch = apiService.batch(requestBatch);
		}

		return Response.ok(responseBatch).build();
	}
	
	@POST
	@Path("/admin")
	@Consumes({"application/xml", "application/*+xml", "text/xml", "application/json", "application/*+json"})
	@Produces({"application/xml", "application/*+xml", "text/xml", "application/json", "application/*+json"})
	public Response admin(IscRequest iscRequest) {
		IscResponse iscResponse;
		
		if (iscRequest instanceof IscRequestBatch) {
			@SuppressWarnings("unchecked")
			IscRequestBatch<E> requestBatch = (IscRequestBatch<E>) iscRequest;
			iscResponse = apiService.transactionalBatch(requestBatch);
		} else if (iscRequest instanceof IscSingleRequest) {
			@SuppressWarnings("unchecked")
			IscSingleRequest<E> singleRequest = (IscSingleRequest<E>) iscRequest;
			iscResponse = apiService.transactionalSingle(singleRequest);
		} else {
			throw new IsysException("Unknown request.");
		}
		
		return Response.ok(iscResponse).build();
	}

}
