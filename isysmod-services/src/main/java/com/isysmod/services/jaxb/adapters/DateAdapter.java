package com.isysmod.services.jaxb.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateAdapter extends XmlAdapter<String, Date> {

    private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Override
    public String marshal(Date v) throws Exception {
        return new SimpleDateFormat(DATE_FORMAT).format(v);
    }

    @Override
    public Date unmarshal(String v) throws Exception {
    	return new SimpleDateFormat(DATE_FORMAT).parse(v);
    }

}
