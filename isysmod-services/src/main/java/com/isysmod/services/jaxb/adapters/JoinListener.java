package com.isysmod.services.jaxb.adapters;

import java.util.List;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;

import com.isysmod.common.jaxb.adapters.AbstractJoinListener;
import com.isysmod.common.jaxb.adapters.JoinDetermination;
import com.isysmod.common.jaxb.isc.JoinedField;

public class JoinListener extends AbstractJoinListener {

    public JoinListener(Package p, List<JoinedField> joins) {
		super(p, joins);
	}

	@Override
    public void beforeMarshal(Object source) {
    	if (isEntityOrCollectionOfEntity(source)) {
    		if (this.depth == 0) {
    			cloneGraph();
    		}
    		
    		this.stack.add(source);
    		MapKey key = new MapKey(source.getClass(), this.depth);
    		Message message = PhaseInterceptorChain.getCurrentMessage();
    		
    		if (this.graphCopy.containsKey(key)) {
    			message.put(JoinDetermination.class, this.graphCopy.get(key));
    		} else if (message.get(JoinDetermination.class) != null) { // Bug CXF contains/remove
    			message.put(JoinDetermination.class, null);
    		}
    		
    		this.depth ++;
    	}
    }
    
    @Override
    public void afterMarshal(Object source) {
    	if (isEntityOrCollectionOfEntity(source) && this.depth > 0) {
    		this.depth --;
    		source = previousFromStack();
    		
    		if (source != null && this.depth > 0) {
        		Class<?> ownerClass = source.getClass();
        		MapKey key = new MapKey(ownerClass, this.depth - 1);
	    		
	    		if (this.graphCopy.containsKey(key)) {
	    			Message message = PhaseInterceptorChain.getCurrentMessage();
	    			message.put(JoinDetermination.class, this.graphCopy.get(key));
	    		}
    		}
    	}
    }

}
