package com.isysmod.services.jaxb.adapters;

import javax.xml.bind.Marshaller;

public class DepthListener extends Marshaller.Listener {

	private int targetDepth = 0;
    private int depth = 0;

    public DepthListener(int depth) {
		this.targetDepth = depth;
	}
    
    @Override
    public void beforeMarshal(Object source) {
        depth++;
    }

    @Override
    public void afterMarshal(Object source) {
        depth--;
    }

	public boolean isMarshalDepth() {
		return depth <= targetDepth;
	}

}
