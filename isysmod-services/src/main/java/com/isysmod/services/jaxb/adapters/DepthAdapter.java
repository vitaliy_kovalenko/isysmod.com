package com.isysmod.services.jaxb.adapters;


import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;

public class DepthAdapter extends XmlAdapter<Object, Object> {

	@Override
    public Object unmarshal(Object v) throws Exception {
        return v;
    }

    @Override
    public Object marshal(Object v) throws Exception {
    	Message message = PhaseInterceptorChain.getCurrentMessage();
    	DepthListener depthListener = message.get(DepthListener.class);
    	
    	if (depthListener != null && !depthListener.isMarshalDepth()) {
    		return null;
        }
        
        return v;
    }

}
