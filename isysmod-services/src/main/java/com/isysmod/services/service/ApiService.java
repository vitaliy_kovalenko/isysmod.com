package com.isysmod.services.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.springframework.transaction.annotation.Transactional;

import com.isysmod.common.IC;
import com.isysmod.common.dao.QueryResult;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.jaxb.isc.IscRequestBatch;
import com.isysmod.common.jaxb.isc.IscResponseBatch;
import com.isysmod.common.jaxb.isc.IscSingleRequest;
import com.isysmod.common.jaxb.isc.IscSingleResponse;
import com.isysmod.common.jaxb.isc.JoinedField;
import com.isysmod.common.model.EntityBean;
import com.isysmod.services.dao.ApiDao;
import com.isysmod.services.jaxb.adapters.JoinListener;

@Named
public class ApiService<E extends EntityBean> {
	
	@Inject
	private ApiDao<E> apiDao;
	
	public IscResponseBatch<E> batch(IscRequestBatch<E> requestBatch) {
		IscResponseBatch<E> responseBatch = new IscResponseBatch<E>();
		
		for (IscSingleRequest<E> singleRequest : requestBatch.getRequests()) {
			responseBatch.add(single(singleRequest));
		}

		return responseBatch;
	}
	
	public IscSingleResponse<E> single(IscSingleRequest<E> request) {
		if ("meta".equals(request.getOperationId())) {
			return performMetaRequest(request);
		} else {
			return performSingleRequest(request);
		}
	}
	
	private IscSingleResponse<E> performMetaRequest(IscSingleRequest<E> request) {
		@SuppressWarnings("unchecked")
		Class<E> entityClass = (Class<E>) com.isysmod.common.model.Client.class;
		
		JoinedField databases = new JoinedField(entityClass, "databases", true)
				.addJoin(new JoinedField(com.isysmod.common.model.Database.class, "entities", true)
//				.addJoin(new JoinedField(com.isysmod.common.model.Entity.class, "entityFields"))
				);
		
		List<JoinedField> joins = new ArrayList<>();
		joins.add(databases);
		
		Message message = PhaseInterceptorChain.getCurrentMessage();
		JoinListener joinListener = new JoinListener(entityClass.getPackage(), joins);
		message.getExchange().put(JoinListener.class, joinListener);
		
		IscSingleResponse<E> response = new IscSingleResponse<E>();
		QueryResult<E> queryResult = apiDao.meta(entityClass, IC.ISYS_MAIN_FACTORY_NAME);
		response.implementQueryResult(queryResult);
		response.setStatus(IC.STATUS_SUCCESS);
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private IscSingleResponse<E> performSingleRequest(IscSingleRequest<E> request) {
		String database = request.getDatabase();
		String method = request.getOperationType();
		
		if (database == null || method == null) {
			throw new WebApplicationException("You must specify data source and operation method.");
		}
		
		IscSingleResponse<E> response = new IscSingleResponse<E>();
		QueryResult<E> queryResult;
		
		switch (method) {
			case IC.METHOD_FETCH:
				String beanClassName = IC.ISYS_ORM_BASE_PACKAGE + "." + database + "." + request.getBeanClassName();
				Class<E> entityClass;
				
				try {
					entityClass = (Class<E>) Class.forName(beanClassName);
				} catch (ClassNotFoundException e) {
					throw new IsysException("Unknown entity: " + beanClassName);
				}

				Message message = PhaseInterceptorChain.getCurrentMessage();
				JoinListener joinListener = new JoinListener(entityClass.getPackage(), request.getJoins());
				message.getExchange().put(JoinListener.class, joinListener);
				
				queryResult = apiDao.fetch(database, entityClass, request.getJoins());
			break;
			case IC.METHOD_ADD:
				queryResult = apiDao.add(database, request.getEntity());
			break;
			case IC.METHOD_UPDATE:
				queryResult = apiDao.update(database, request.getEntity());
			break;
			case IC.METHOD_REMOVE:
				queryResult = apiDao.remove(database, request.getEntity());
			break;
			default:
				throw new WebApplicationException("The method should be one of: fetch, add, update or remove.");
		}
		
		response.implementQueryResult(queryResult);
		response.setStatus(IC.STATUS_SUCCESS);
		
		return response;
	}
	
	@Transactional
	public IscResponseBatch<E> transactionalBatch(IscRequestBatch<E> requestBatch) {
		return batch(requestBatch);
	}
	
	@Transactional
	public IscSingleResponse<E> transactionalSingle(IscSingleRequest<E> request) {
		return single(request);
	}
}
