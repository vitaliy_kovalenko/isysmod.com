package com.isysmod.services.jms;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;

import org.apache.log4j.Logger;

import com.isysmod.common.IC;
import com.isysmod.common.jdo.DatasourceManager;
import com.isysmod.common.jms.JmsServer;

public class FactoryMessageListener implements MessageListener {
	
	private static final Logger LOGGER = Logger.getLogger(FactoryMessageListener.class);
	
	private static final String ORM_DIR = System.getProperty("catalina.home") + "/WEB-INF/lib";

	@Inject
	private DatasourceManager datasourceManager;
	
	@Inject
	private JmsServer jmsServer;
	
	public FactoryMessageListener() {
		File file = new File(ORM_DIR);
		
		if (!file.exists()) {
			file.mkdirs();
		}
	}

	private void handleMapMessage(MapMessage message, String factoryName) throws JMSException {
		LOGGER.info("Received action for factory '" + factoryName + "'.");
		String action = message.getString(IC.JMS_ACTION);

		switch (action) {
			case IC.JMS_ACTION_REBUILD_FACTORY:
				LOGGER.info("Action parameters: [" + IC.JMS_ACTION + "=" + action + ", " + IC.JMS_FACTORY + "=" + factoryName + "].");
				datasourceManager.rebuildFactory(factoryName);
				break;
			default:
				LOGGER.warn("Unknown action: " + action);
				break;
		}
	}

	private void handleByteMessage(BytesMessage message, String factoryName) throws JMSException {
		LOGGER.info("Received ORM archive for factory '" + factoryName + "'.");
		String archivePathName = getOrmArchivePathName(factoryName);
		byte[] data = new byte[(int) message.getBodyLength()];
		message.readBytes(data);
		FileOutputStream outputStream = null;
		IOException exception = null;
		
		try {
			outputStream = new FileOutputStream(archivePathName); 
			outputStream.write(data);
			LOGGER.info("ORM archive saved as " + archivePathName);
		} catch (IOException e) {
			exception = e;
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					exception = e;
				}
			}
		}
		
		if (exception != null) {
			throw new RuntimeException(exception);
		}
	}
	
	private String getOrmArchivePathName(String factory) {
		return ORM_DIR + "/" + factory + "-client.jar";
	}

	@Override
	public void onMessage(Message message) {
		if (message == null) {
			throw new IllegalArgumentException("Received null-message.");
		}
		
		try {
			String factoryName = message.getStringProperty(IC.JMS_FACTORY);
		
			if (factoryName == null || factoryName.length() < 1) {
				throw new IllegalArgumentException("Received message without factory name.");
			}
			
			if (message instanceof BytesMessage) {
				handleByteMessage((BytesMessage) message, factoryName);
			} else if (message instanceof MapMessage) {
				handleMapMessage((MapMessage) message, factoryName);
			} else {
				LOGGER.warn("Unknown message received.");
			}
			
			Session session = jmsServer.getSession();
			Message response = session.createMessage();
			response.setIntProperty(IC.JMS_STATUS, IC.JMS_STATUS_OK);
			jmsServer.replyTo(message, response);
		} catch (JMSException e) {
			throw new RuntimeException(e);
		}
	}

}
