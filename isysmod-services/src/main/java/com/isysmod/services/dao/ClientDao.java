package com.isysmod.services.dao;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.isysmod.common.IC;
import com.isysmod.common.model.Client;

@Named
public class ClientDao {

	public Client fetchByApiKey(EntityManager entityManager, String apiKey) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Client> criteriaQuery = builder.createQuery(com.isysmod.common.model.Client.class);
		Root<Client> root = criteriaQuery.from(com.isysmod.common.model.Client.class);
		criteriaQuery.select(root);
		criteriaQuery.where( builder.equal( root.get( IC.APIKEY ), apiKey ) );
		TypedQuery<Client> query = entityManager.createQuery(criteriaQuery);
		
		return query.getSingleResult();
	}
	
	public Client update(EntityManager entityManager, Client client) {
		return null;
	}

}
