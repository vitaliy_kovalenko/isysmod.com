package com.isysmod.services.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.ws.rs.WebApplicationException;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.security.SecurityContext;

import com.isysmod.common.dao.QueryResult;
import com.isysmod.common.jaxb.isc.JoinedField;
import com.isysmod.common.model.EntityBean;
import com.isysmod.common.security.ClientPrincipal;

@Named
public class ApiDao<E extends EntityBean> {
	
	public QueryResult<E> meta(Class<E> entityClass, String factoryName) {
		ClientPrincipal principal = getClientPrincipal();
		Serializable primaryKey = principal.getPrimaryKey();
		EntityManager manager = principal.currentSession(factoryName);
		E client = manager.find(entityClass, primaryKey);
		List<E> data = new ArrayList<E>();
		
		if (client != null) {
			data.add(client);
		}
		
		QueryResult<E> result = new QueryResult<E>();
		result.setStartRow(0);
		result.setEndRow(data.size());
		result.setTotalRows(Long.valueOf( data.size() ));
		result.setData(data);
		
		return result;
	}
	
	public QueryResult<E> fetch(String database, Class<E> clazz, List<JoinedField> joins) {
		int startRow = 0;
		int endRow = 10;

		EntityManager manager = getClientPrincipal().currentSession(database);
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<E> q = builder.createQuery(clazz);
		Root<E> entity = q.from(clazz);
		this.buildJoinsAndFetches(clazz.getPackage(), entity, joins);
		q.select( entity );
//		Predicate predicate = builder.equal(entity.get("id"), "3264");
//		q.where(predicate);
		
		List<E> data = manager.createQuery(q)
				.setFirstResult(startRow)
				.setMaxResults(endRow - startRow)
				.getResultList();
		
		Long totalRows = getCount(manager, builder, clazz);
		
		QueryResult<E> result = new QueryResult<E>();
		result.setStartRow(startRow);
		result.setEndRow(startRow + data.size());
		result.setTotalRows(totalRows);
		result.setData(data);
		
		return result;
	}
	
	public QueryResult<E> add(String database, E entity) {
		ClientPrincipal principal = getClientPrincipal();
		EntityManager manager = principal.currentSession(database);
		manager.persist(entity);
		List<E> data = new ArrayList<E>();
		data.add(entity);
		
		QueryResult<E> result = new QueryResult<E>();
		result.setData(data);
		
		return result;
	}

	public QueryResult<E> update(String database, E entity) {
		ClientPrincipal principal = getClientPrincipal();
		EntityManager manager = principal.currentSession(database);
		entity = manager.merge(entity);
		
		List<E> data = new ArrayList<E>();
		data.add(entity);
		
		QueryResult<E> result = new QueryResult<E>();
		result.setData(data);
		
		return result;
	}

	public QueryResult<E> remove(String database, E entity) {
		ClientPrincipal principal = getClientPrincipal();
		EntityManager manager = principal.currentSession(database);
		entity = manager.merge(entity);
		manager.remove(entity);
		
		return new QueryResult<E>();
	}
	
	private ClientPrincipal getClientPrincipal() {
		Message message = PhaseInterceptorChain.getCurrentMessage();
		SecurityContext sc = message.getExchange().get(SecurityContext.class);
		
		return (ClientPrincipal) sc.getUserPrincipal();
	}
	
	private Long getCount(EntityManager manager, CriteriaBuilder b, Class<E> clazz) {
		CriteriaQuery<Long> q = b.createQuery(Long.class);
		q.select( b.count( q.from(clazz) ) );
		
		return manager.createQuery(q).getSingleResult();
	}
	
	private void buildSubJoins(Package p, Join<E, E> parent, List<JoinedField> joins) {
		if (joins == null || joins.size() < 1) {
			return;
		}
		
		for (JoinedField jaxbField : joins) {
			Join<E, E> child = parent.join(jaxbField.getFieldName(), jaxbField.getType());
			buildSubJoins(p, child, jaxbField.getJoins());
		}
	}
	
	private void buildSubFetches(Package p, Fetch<E, E> parent, List<JoinedField> joins) {
		if (joins == null || joins.size() < 1) {
			return;
		}
		
		for (JoinedField jaxbField : joins) {
			boolean isCollection;
			
			try {
				isCollection = jaxbField.isCollection(p);
			} catch (NoSuchFieldException | SecurityException | ClassNotFoundException e) {
				throw new WebApplicationException(e);
			}
			
			if (! isCollection) {
				Fetch<E, E> child = parent.fetch(jaxbField.getFieldName(), jaxbField.getType());
				buildSubFetches(p, child, jaxbField.getJoins());
			}
		}
	}

	private void buildJoinsAndFetches(Package p, Root<E> root, List<JoinedField> joins) {
		if (joins == null || joins.size() < 1) {
			return;
		}
		
		for (JoinedField jaxbField : joins) {
			boolean isCollection;
			
			try {
				isCollection = jaxbField.isCollection(p);
			} catch (NoSuchFieldException | SecurityException | ClassNotFoundException e) {
				throw new WebApplicationException(e);
			}
			
			// По коллекциям можно фильтровать, но выбирать их нельзя.
			// Если нужно их вывести в распечатке, то следует подлючить JoinListener.
			// Тогда при формировании XML обращение к прокси вызовет запрос, который
			// вернет коллекцию (все 100500 записей в ней).
			if (isCollection || !jaxbField.isSelectable()) {
				Join<E, E> child = root.join(jaxbField.getFieldName(), jaxbField.getType());
				buildSubJoins(p, child, jaxbField.getJoins());
			} else {
				Fetch<E, E> child = root.fetch(jaxbField.getFieldName(), jaxbField.getType());
				buildSubFetches(p, child, jaxbField.getJoins());
			}
		}
	}
}
