package com.isysmod.services.test;

import java.net.MalformedURLException;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import com.isysmod.common.dao.QueryResult;
import com.isysmod.common.model.EntityBean;
import com.isysmod.services.dao.ApiDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("beans.xml")
@WebAppConfiguration
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ApiServiceTests<T extends EntityBean> extends AbstractTests {

	@Inject
	private ApiDao<T> apiService;
	
	@SuppressWarnings("unchecked")
	@Test
    public void fetchTest() throws ClassNotFoundException, MalformedURLException {
		Class<T> clazz = (Class<T>) Class.forName("com.isysmod.orm.modeling.Client");
		QueryResult<T> result = apiService.fetch("modeling", clazz, null);
		Assert.notNull(result);
	}
}
