package com.isysmod.services.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.inject.Inject;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import com.isysmod.common.IC;
import com.isysmod.common.jms.JmsClient;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("beans.xml")
@WebAppConfiguration
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FactoryDispatcherJmsTests extends AbstractTests {
	
	private static final String FACTORY_NAME = "modeling";
	
	@Inject
	private JmsClient jmsClient;
	
	@Test
	public void AsendOrmArchive() throws JMSException {
		Session session = jmsClient.getSession();
		BytesMessage request = session.createBytesMessage();
		Path path = ormArchive.toPath();
		
		try {
			byte[] data = Files.readAllBytes(path);
			request.writeBytes(data);
			request.setStringProperty(IC.JMS_FACTORY, FACTORY_NAME);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Message response = jmsClient.send(request);
		Assert.isTrue(response.getIntProperty(IC.JMS_STATUS) == IC.JMS_STATUS_OK);
	}
    
	@Test
    public void BsendRebuildFactoryAction() throws JMSException {
		Session session = jmsClient.getSession();
		MapMessage request = session.createMapMessage();
		request.setString(IC.JMS_ACTION, IC.JMS_ACTION_REBUILD_FACTORY);
		request.setStringProperty(IC.JMS_FACTORY, FACTORY_NAME);
    	Message response = jmsClient.send(request);
    	Assert.isTrue(response.getIntProperty(IC.JMS_STATUS) == IC.JMS_STATUS_OK);
    }

}
