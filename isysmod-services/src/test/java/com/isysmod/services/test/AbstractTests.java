package com.isysmod.services.test;

import java.io.File;
import java.io.IOException;

import javax.naming.Context;

import org.springframework.util.FileCopyUtils;

public abstract class AbstractTests {

	static String projectDir;
	
	static File ormArchive;
	
	static {
		System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                "org.apache.naming.java.javaURLContextFactory");
        System.setProperty(Context.URL_PKG_PREFIXES,
                "org.apache.naming");
        projectDir = System.getProperty("user.dir");
        System.setProperty("catalina.home",
        		projectDir + "/target");
        
        try {
        	ormArchive = new File(projectDir + "/src/main/webapp/WEB-INF/lib/modeling-client.jar");
    		
    		if (ormArchive.exists()) {
	    		File file = new File(projectDir + "/target/lib");
	    		
	    		if (!file.exists()) {
	    			file.mkdir();
	    		}
	    		
				FileCopyUtils.copy(
						ormArchive,
						new File(projectDir + "/target/lib/modeling-client.jar")
				);
    		}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
