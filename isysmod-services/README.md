## End-user services.
### Exclusive features:
* Hot-plug model support. No need to restart the server when adding or changing the model.
* Proxy object prevents infinite loops in the formation of XML / JSON. But not compatible with SmartClient
### Additional features:
* Multiple database support
* XA-transactions support

Надо переделать джойны -- убрать JoinListener.
Должно быть так: если заглушка -- выдаем заглушку, если нет -- распечатывам
Адаптеры уже есть, запросы на джойны дописать в моделировании для Hibernate.
Вот только тогда, коллекции распечатываться не будут никогда. А сейчас
можно одним HTTP запросом выьрать всю базу. Хорошо ли это?
Для формирования страницы сайта однозначно большой плюс, а от дураков
защиты все равно нет.