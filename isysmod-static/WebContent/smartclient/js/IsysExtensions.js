// Extensions
String.prototype.toLowerCaseFirst = function() {
    return this.charAt(0).toLowerCase() + this.slice(1);
}

Date.prototype._xmlSerialize = function (name, type, namespace, prefix) {
    return isc.Comm._xmlValue(name, this.toSchemaDate(null, isc.Comm._trimMillis),
            type, namespace, prefix);
}

Date.prototype.relativeYear = function () {
    var relativeTime = Date.now() - this.getTime();
    var epochDate = new Date(relativeTime);
    return Math.abs(epochDate.getUTCFullYear() - 1970);
}

formatComplexCellValue = function(value, record) {
	var items = [];
	for (var code in value) {
		if (!value.hasOwnProperty(code) || !value[code] || code == "id") {
			continue;
		}
		items.push(value[code]);
	}
	return items.join(", ");
}

isc.guid = function() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

isc.uuid = function(form, item, icon) {
	var newValue = isc.guid();
	form.setValue(item, newValue);

	if (isA.Function(form.itemChanged)) {
		form.itemChanged(item, newValue);
	}
}

isc.IsysCacheEntry = function(totalRows) {
	this.ttl = 0;
	this.data = [];
	this.totalRows = totalRows != null ? totalRows : false;
	
	this.isExpired = function() {
		return Date.now() > this.ttl;
	}
	
	this.isRangeExists = function(startRow, endRow) {
		for (var i = startRow; i < endRow; i++) {
			if (!this.data[i]) return false;
		}
		
		return true;
	}
	
	this.isLengthKnown = function() {
		return this.totalRows !== false;
	}
	
	this.addRange = function(startRow, data) {
		for (var i = 0; i < data.length; i++) {
			this.data[i + startRow] = data[i];
		}
		
		this.ttl = Date.now() + 60000;
		//console.log("addRange:", startRow, startRow + data.length);
	}
	
	this.getRange = function(startRow, endRow) {
		if (this.isLengthKnown() && endRow > this.totalRows) {
			endRow = this.totalRows;
		}
		
		if (!this.isRangeExists(startRow, endRow)) {
			//console.log("getRange: not found", startRow, endRow);
			return null;
		}
		
		//console.log("getRange:", startRow, endRow, this.totalRows);
		
		return {
			startRow: startRow,
			endRow: endRow,
			totalRows: this.totalRows === false ? null : this.totalRows,
			data: this.data.slice(startRow, endRow)
		}
	}
}

isc.IsysManagedCache = {
	_cache : {},
	
	_getEntryPath : function(sortBy) {
		if (!sortBy.length) return "nosort";
		var path = "";
		for (var i = 0; i < sortBy.length; i++) {
			if (isc.isAn.Object(sortBy[i])) {
				path += "_" + (sortBy[i].direction == "descending" ? "-" : "") + sortBy[i].property;
			} else {
				path += "_" + sortBy[i];
			}
		}
		return path;
	},
	
	_getEntry : function(datasource, path) {
		if (!this._cache[datasource]) return null;
		return path ? this._cache[datasource][path] : this._cache[datasource];
	},
	
	_putEntry : function(datasource, path, entry) {
		if (! this._cache[datasource]) {
			this._cache[datasource] = {};
		}
		
		if (path) {
			this._cache[datasource][path] = entry;
		} else {
			this._cache[datasource] = entry;
		}
	},
	
	_removeEntry : function(datasource, path) {
		if (path) {
			delete this._cache[datasource][path];
		} else {
			delete this._cache[datasource];
		}
		
		//console.log("Remove", datasource, path);
	},
	
	isCacheAble : function(dsRequest, operationType, data) {
    	if (!dsRequest && !operationType) {
    		return false;
    	}

    	var emptyCriteria = !dsRequest || (dsRequest && !dsRequest.resultSet) || dsRequest.resultSet._emptyCriteria;
    	emptyCriteria = emptyCriteria && (!data || isc.isAn.emptyObject(data));
    	
    	return (!operationType || operationType == "fetch") && emptyCriteria;
    },
	
	put: function(datasource, sortBy, startRow, totalRows, data) {
		data = data ? data : {};
		sortBy = sortBy ? sortBy : [];
		startRow = startRow ? startRow : 0;
		var path = this._getEntryPath(sortBy);
		var entry = this._getEntry(datasource, path);
		// Clear cache on put or not?
//		if (entry && entry.isExpired()) {
//			this._removeEntry(datasource, path);
//			entry = null;
//		}
		
		if (! entry) {
			entry = new isc.IsysCacheEntry(totalRows);
			this._putEntry(datasource, path, entry);
		}

		entry.addRange(startRow, data);
	},
	
	get: function(datasource, sortBy, startRow, endRow) {
		sortBy = sortBy ? sortBy : [];
		startRow = startRow ? startRow : 0;
		endRow = endRow ? endRow : 0;
		var path = this._getEntryPath(sortBy);
		var entry = this._getEntry(datasource, path);
		
		if (!entry) {
			return null;
		} else if (entry.isExpired()) {
			this._removeEntry(datasource, path);
			return null;
		}
		
		return entry.getRange(startRow, endRow);
	},
	
	invalidate : function() {
		this._cache = {};
	}
}

isc.addMethods(isc.isA, {
	
	XMLNode : function (object) {
        if (object == null) return false;
        if (isc.Browser.isIE) {
            return object.specified != null && object.parsed != null &&
                   object.nodeType != null && object.hasChildNodes != null;
        }
        var doc = object.ownerDocument;
        if (doc == null) return false;
        return doc.contentType == this._$textXML || doc.contentType == "application/xml";
    }
	
});

isc.Canvas.addClassMethods({
	getRuleScopeDataSources : function (targetRuleScope) {
	    if (!targetRuleScope) return [];
	    targetRuleScope = (isc.isA.String(targetRuleScope) ? window[targetRuleScope] : targetRuleScope);
	    if (!targetRuleScope.getRuleScopeDataBoundComponents) return [];
	    var dataSources = [],
	        dbcList = targetRuleScope.getRuleScopeDataBoundComponents()
	    ;
	    for (var i = 0; i < dbcList.length; i++) {
	        if (dbcList[i] == targetRuleScope) {
	            continue;
	        }
	        if (dbcList[i].dataSource) {
	        	// FIXME: SmartClient bug
	        	var ds = isc.isA.String(dbcList[i].dataSource) ? isc.DS.get(dbcList[i].dataSource) : dbcList[i].dataSource;
	            if (!dataSources.contains(ds)) {
	                dataSources.add(ds);
	            }
	        }
	        if (dbcList[i] != targetRuleScope) {
	            // Auto-generate
	            dataSources.add(dbcList[i].makeDataSourceFromFields(dbcList[i].ID));
	        }
	    }
	    return dataSources;
	}
});

isc.RPCManager.addClassMethods({
	_restXMLTransactionStart : "<responses xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">",
	
	transformResponse : function (rpcResponse, rpcRequest, data) {
		if (isc.isA.String(rpcResponse.results) && isc.startsWith(rpcResponse.results, this._restXMLResponseStart)) {
			var xml = rpcResponse.results.substring(this._restXMLResponseStart.length);
			xml = "<response xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" + xml;
			rpcResponse.results = xml;
		}
		
		return rpcResponse;
	}

});

//Converting all criteria to advanced
isc.DataSource.addClassMethods({
	filterCriteriaForFormValues : function (formValues) {
		if (!isc.DS.isAdvancedCriteria(formValues)) {
			formValues = isc.DS.convertCriteria(formValues);
		}
		
		return formValues;
	}
});

//isc.defineClass("IsysSplitPane", "SplitPane").addProperties({
//	leftLayoutDefaults: {
//        _constructor: "VLayout",
//        width: 200
//    }
//});

isc.defineClass("IsysSelectItem", "SelectItem").addProperties({
	// Дрбавляем сортировку для фильтра
	makePickList : function (show, requestProperties) {
		if (this.multiple && this.pickList && this.pickList.showFilterEditor) {
			return Super("makePickList", arguments);
		}
		
		if (this.grid) {
			var field = this.grid.getFieldByName(this.name);
			
			if (this.pickListProperties == null && field.pickListProperties) {
				this.pickListProperties = field.pickListProperties;
			}
			
			if (this.sortField == null && field.sortField) {
				this.sortField = field.sortField;
			}
			
			if (this.grid.actionType == "filter") {
				// FIXME: некорректный подсчет startRow / endRow
				this.allowEmptyValue = true;
			}
		}
		
		if (this.pickListProperties == null) {
            this.pickListProperties = {};
		}
		
        if (this.progressiveLoading === true || this.progressiveLoading === false) {
            this.pickListProperties.progressiveLoading = this.progressiveLoading;
        }
        
        var interfaceMakePickList = isc.PickList.getPrototype().makePickList;
        
        return interfaceMakePickList.apply(this, arguments);
    }
});

isc.defineClass("IsysComboBoxItem", "ComboBoxItem").addProperties({
	pickListProperties: {
		drawAllMaxCells:0
	},
	// Исправляем опЯчатку =)
	makePickList : function (show) {

        // setting 'showFilterEditor' is unsupported for ComboBoxItem - actually catch this case and warn
        // about it
        // (one time warning only - we don't want to spam them for every ComboBoxItem created off a common
        // editorProperties block or similar)

        if (!isc.ComboBoxItem._showFilterEditorWarningShown && this.pickListProperties != null &&
           this.pickListProperties.showFilterEditor)
        {
            this.logWarn("ComboBoxItem.pickListProperties specified with 'showFilterEditor' set to true. " +
                         "The 'showFilterEditor' property is not supported for the pickList of a ComboBoxItem " +
                         "and may result in unexpected user experience.");
            isc.ComboBoxItem._showFilterEditorWarningShown = true;

        }

        // Create cached special values if applicable
        this._getSpecialValues(false);

        if (this.grid) {
			var field = this.grid.getFieldByName(this.name);
			
			if (this.pickListProperties == null && field.pickListProperties) {
				this.pickListProperties = field.pickListProperties;
			}
			
			if (this.sortField == null && field.sortField) {
				this.sortField = field.sortField;
			}
		}
		
		if (this.pickListProperties == null) {
            this.pickListProperties = {};
		}
		
        if (this.progressiveLoading === true || this.progressiveLoading === false) {
            this.pickListProperties.progressiveLoading = this.progressiveLoading;
        }

        if (!this.filterLocally &&
            this.specialValues && !this.separateSpecialValues &&
            this._getOptionsFromDataSource())
        {
            if (this.pickListProperties.dataProperties == null)
                this.pickListProperties.dataProperties = {};
            // Using basic rather than local means if we do have outstanding filter criteria
            // we'll fetch fewer rows from the server, while still being able to manipulate
            // the cache to insert the empty row at the top.
            this.pickListProperties.dataProperties.fetchMode = "basic";
        }
        var interfaceMakePickList = isc.PickList.getPrototype().makePickList;
        return interfaceMakePickList.apply(this, arguments);
    }
});

isc.Comm.addClassMethods( {
	// helper method - returns an xml open tag with the (optional) type.
    _xmlOpenTag : function (tagName, type, namespace, prefix, leaveOpen, isRoot) {

        var output = isc.SB.create();

        var writeNamespace = namespace != null;

        // if "prefix" is passed as an object, use it to accrue a map from namespace to namespace
        // prefix, but don't actually write out any namespaces, relying on the calling code to do
        // so
        if (namespace != null && isc.isAn.Object(prefix)) {
            writeNamespace = false;
            prefix = this._getPrefix(prefix, namespace);
        }

        // encode the name in '_isc_name' if it's not a valid XML identifier
        var extraXML = '';
        if (!this._isValidXMLIdentifier(tagName)) {
            extraXML = ' _isc_name="' + isc.makeXMLSafeAttribute(tagName) + '"';
            tagName = "Object";
        }

        if (namespace) {
            prefix = prefix || "schNS";
            output.append("<", prefix, ":", tagName);
            if (writeNamespace) output.append(" xmlns:", prefix, "=\"", namespace, "\"");
        } else {
            output.append("<", tagName);
        }
        
        if (extraXML) output.append(extraXML);

        // if an xsi type is passed in for this object, mark the object with that type
        if (type && !this.omitXSI) {
            output.append(" xsi:type=\"", isc.makeXMLSafeAttribute(type), "\"");
        }

        if (!leaveOpen) output.append(">");

        return output.release(false);
    },
    
    _xmlSerializeArray : function (name, object, objPath, objRefs, prefix, isRoot) {
        // open xml tag
        var result = isc.Comm._xmlOpenTag(name);

        // spin through the array and create <elem>value</elem> strings
        for (var i = 0, len = object.length; i < len; i++) {
            var value = object[i];
            var context = {
                objRefs : objRefs,
                objPath : isc.JSONEncoder._serialize_addToPath(objPath, i),
                isRoot : false
            };
            var nodeName = null;
            
            if (value.$schemaId) {
            	nodeName = value.$schemaId;
            } else if (value.operator) {
            	nodeName = "elem";
            	value._constructor = "AdvancedCriteria";
            } else if (value.owner && value.field) {
            	nodeName = "join";
            } else {
            	nodeName = "elem";
            }
            
            var childXml = isc.Comm._xmlSerialize( nodeName, value, (prefix ? prefix + "\t" : null), context );
            result = isc.StringBuffer.concat( result, (prefix ? prefix + "\t" : null), childXml );
        }

        // close xml tag
        result = isc.StringBuffer.concat(result, prefix, isc.Comm._xmlCloseTag(name));

        return result;
    },
    
    _xmlSerializeObject : function (name, object, objPath, objRefs, prefix, isRoot) {
    	var self = this;
        // if it's a class or has the special _constructor property, then the name is the class name
        // this allows us to hand the output of this method to the server-side xml parser and get back
        // a DataSource-validated object back.
        // Aug 2008 - moved this check before the call to isc.Comm._xmlOpenTag, to ensure that it
        // uses the correct name for non-Class objects with a _constructor - without this change,
        // it was returning mismatched open and close tags
        // April 2010 - added "RelativeDate" as a class-name to ignore, so that relative dates
        // can be sent up to the server as part of criteria without having their "value" property
        // renamed
        if (isc.isAn.Instance(object)) {
        	name = object.getClassName();
        } else if (object._constructor && object._constructor != "AdvancedCriteria" &&
            object._constructor != "RelativeDate") {
        	name = object._constructor;
        } else if (object._constructor && (object._constructor == "AdvancedCriteria" ||
            object._constructor == "RelativeDate")) {
        	if (name == "elem" && object.fieldName) {
        		object["xsi:type"] = "Criterion";
        	} else {
        		object["xsi:type"] = object._constructor;
        	}
        }

        var xsiType;
        
        if (object["xsi:type"]) {
        	xsiType = object["xsi:type"];
        	delete object["xsi:type"];
        }
        
        // open xml tag
        var result = isc.Comm._xmlOpenTag(name, xsiType, null, null, null, isRoot);
        object = isc.JSONEncoder._serialize_cleanNode(object);

        // for each key in the object
        for (var key in object) {
            if (key == null)
            	continue;
            if (key == isc.gwtRef || key == isc.gwtModule)
            	continue;
            // XML identifiers can't start with $ (parser crashes)
            if (key.startsWith('$') || key.startsWith('_') || key.startsWith('xsi:') || key.startsWith('xmlns:'))
            	continue;

            var value = object[key];
            if (value === "") value = null;

            // NOTE: null is a real value. undefined should be treated as null for cases where
            // a field's value is cleared.
            // if the value is a function, skip it
            // Exception - we can serialize actions by looking at function.iscAction - in this
            // case retain it
            if (isc.isA.Function(value) && !value.iscAction)
            	continue;
            // convert the key to a string
            var keyStr = key.toString();

            var context = {
                objRefs: objRefs,
                objPath: isc.JSONEncoder._serialize_addToPath(objPath, key),
                isRoot: false
            };

            // transform the value
            result = isc.StringBuffer.concat(
                    result,
                    (prefix != null ? isc.StringBuffer.concat(prefix, "\t") : ""),
                    isc.Comm._xmlSerialize(keyStr, value,
                                           (prefix != null ? isc.StringBuffer.concat(prefix, "\t") : null),
                                           context)
                    );
        }

        // close xml tag
        result = isc.StringBuffer.concat(
                result,
                (prefix != null ? isc.StringBuffer.concat(prefix) : ""),
                isc.Comm._xmlCloseTag(name)
                );

        return result;
    },
    
    _xmlSerialize : function (name, object, prefix, context) {

        // Avoid attempting to manipulate SGWT Java objects
        if (isc.Browser.isSGWT && window.SmartGWT.isNativeJavaObject(object)){

            if (object == null) object = null;
            // If the global flag has been set to warn when we hit an unconvertible
            // object, do this.
            else {
                if (window.SmartGWT.warnOnSerializeError) {
                    window.SmartGWT.throwUnconvertibleObjectException(
                        object, window.SmartGWT.serializeErrorMessage
                    );
                }
                object = String.asSource(object + "");
            }
        }

        // record whether a name was explicitly passed
        var namePassed = name != null;

        // NOTE: allow context as a partial object, so eg isRoot can be set
        if (!context || !context.objRefs) {
            context = isc.addProperties({}, context);
            context.objRefs = {obj:[],path:[]};
            if (!context.objPath) {
                if (object && object.getID) context.objPath = object.getID();
                else context.objPath = "";
            }
            if (name == null) {
                if (isc.isA.Class(object)) name = object.getClassName();
                else if (isc.isAn.Array(object)) name = "Array";
                else if (isc.isA.Object(object)) name = object.$schemaId || "Object";
                else name = "ISC_Auto";
            }
        }

        // handle simple types

        // NOTE: in some use cases we need be able to send null, which potentially has a distinct
        // meaning from empty string (""), for example, nulling out a text field vs setting to
        // empty string.  In this case null is encoded distinctly by setting the attribute xsi:nil.
        // Note schema-driven serialization in DataSource.js does a similar thing but only for
        // fields marked nillable:true in schema.
        if (object == null) {
            if (isc.Comm.xmlSchemaMode || !isc.Comm._explicitNils) {
                return isc.Comm._xmlValue(name, "");
            } else {
                // send explicit null
                return isc.Comm._xmlValue(name, null, "nil");
            }
        }

        if (isc.isA.String(object))    {
            return isc.Comm._xmlValue(name, isc.makeXMLSafe(object),
                                      (isc.Comm.xmlSchemaMode ? "string" : null));
        }
        if (isc.isA.Function(object)) {
            if (object.iscAction)
            	return isc.StringMethod._xmlSerializeAction(object.iscAction);
            return null;
        }

        if (object == window) {
            this.logWarn("Serializer encountered the window object at path: " + context.objPath
                        +" - returning null for this slot.");
            return null;
        }

        // XML comm supports strong typing of numbers and booleans, but JS comm does not (the type
        // information is not propagated). Preserving the type is useful, so we default to that - but
        // this can be disabled
        
        if (
        		isc.isA.Number(object) ||
        		isc.isA.SpecialNumber(object) ||
        		isc.isA.Boolean(object)
        	) {
        	return isc.Comm._xmlValue(name, object);
        }

        // for complex types:

        // detect infinite loops by checking if we've seen this object before.
        // disambiguate between true loops vs the same leaf object being encountered twice
        // (such as a simple Array containing two Strings which appears in two spots).  Only
        // consider this a loop if the preceding occurrence of the object was some parent of
        // ours.
        var prevPath = isc.JSONEncoder._serialize_alreadyReferenced(context.objRefs, object);
        if (prevPath != null && context.objPath.contains(prevPath)) {
            // Note: check that the first char after "prevPath" is a path separator char in order
            // to avoid false loop detection with "prop" and "prop2" having the same non-looping
            // object (since "prop2" contains "prop").
            var nextChar = context.objPath.substring(prevPath.length, prevPath.length+1);
            //this.logWarn("backref: prevPath: " + prevPath + ", current: " + context.objPath +
            //             ", char after prevPath: " + nextChar);
            if (nextChar == "." || nextChar == "[" || nextChar == "]") {
                if (this.serializeBackrefs) {
                    return isc.Comm._xmlOpenTag(name) +
                                    isc.Comm.XML_BACKREF_PREFIX + prevPath +
                           isc.Comm._xmlCloseTag(name);
                }
                return isc.emptyString;
            }
        }

        // remember Objects and Arrays to avoid infinite loops
        isc.JSONEncoder._serialize_remember(context.objRefs, object, context.objPath);

        // if there is an xmlSerialize method associated with this object, call that
        if (isc.isA.Function(object._xmlSerialize)) {
            return object._xmlSerialize(name, null, null, prefix, context.objRefs, context.objPath);
        } else if (isc.isA.Class(object)) {
            this.logWarn("Attempt to serialize class of type: " + object.getClassName()
                         + " at path: " + context.objPath + " - returning null for this slot.");
            return null;
        }

        // we define the xsi namespace on the first nested object that we encounter.  The first such
        // object sets the value isRoot on the context to 'false' explicitly.  If it's not defined, then
        // it's true.
        var isRoot = context.isRoot == false ? false : true;

        // handle arrays as a special case
        if (isc.isAn.Array(object))
            return isc.Comm._xmlSerializeArray(name, object, context.objPath,
                                               context.objRefs, prefix, isRoot);

        var data;
        // if the object has a getSerializeableFields, use whatever it returns, otherwise just use the object
        if (object.getSerializeableFields) {
            data = object.getSerializeableFields([], []);
        } else {
            data = object;
        }

        return isc.Comm._xmlSerializeObject(name, data, context.objPath,
                                            context.objRefs, prefix, isRoot);
    }

});

isc.JSONEncoder.addProperties({
	_serializeObject : function (object, objPath, objRefs, prefix) {
	    // add the start object marker
	    var output = isc.SB.create(),
	        undef;

	    object = isc.JSONEncoder._serialize_cleanNode(object);

	    try {

	        for (var key in object) break;
	    } catch (e) {
	        if (this.showDebugOutput) {
	            if (isc.isAn.XMLNode(object)) return isc.echoLeaf(object);

	            var message;
	            if (e.message) {
	                message = (e.message.asSource != null ? e.message.asSource()
	                                                      : String.asSource(e.message));
	                return "{ cantEchoObject: " + message + "}";
	            } else {
	                return "{ cantEchoObject: 'unspecified error' }";
	            }
	        } else return null;
	    }
	    
	    for (var key in object) {
	    	if (key == null) continue;
	    	var keyStr = key.toString();
	    	
	    	if (keyStr == "_constructor" && (object._constructor == "AdvancedCriteria" ||
	                object._constructor == "RelativeDate")) {
        		if (object._constructor == "AdvancedCriteria" && isc.isAn.Array(object.criteria)) {
        			var elem = object.criteria;
        			
        			for (var i = 0; i < elem.length; i++) {
        				elem[i].type = "Criterion";
        				delete elem[i]._constructor;
        			}
        			
        			object.criteria = {};
        			object.criteria.elem = elem;
        		}
        		
        		object.type = object._constructor;
            }
	    }

	    output.append("{");
	    // for each key in the object
	    for (var key in object) {
	        // skip null keys
	        if (key == null) continue;
	        // skip internal properties, if the flag is set
	        if (this.skipInternalProperties && (isc.startsWith(key, isc._underscore) || isc.startsWith(key, isc._dollar))) continue;
	        var value = object[key];

	        // if the value is a function, skip it
	        if (isc.isA.Function(value)) continue;

	        // we don't want to access attributes of the object if it's a Java object
	        var isJavaObj = isc.Browser.isSGWT ? window.SmartGWT.isNativeJavaObject(value) : false;
	        // omit instances entirely if so configured

	        if (key != isc.gwtRef && !isJavaObj && isc.isAn.Instance(value) && this.serializeInstances == "skip") continue;

	        // otherwise return the key:value pair

	        // convert the key to a string
	        var keyStr = key.toString();
	        // and if it isn't a simple identifier, quote it
	        if (this.strictQuoting || !isc.Comm._simpleIdentifierRE.test(keyStr)) {
	            if (keyStr.contains('"')) {
	                keyStr = '"' + this.convertToEncodedQuotes(keyStr) + '"';
	            } else {
	                keyStr = '"' + keyStr + '"';
	            }
	        }

	        var otherObjPath = isc.JSONEncoder._serialize_addToPath(objPath, key);
	        var serializedValue;

	        if (key == isc.gwtRef) {
	            // don't try to serialize references to GWT Java objects
	            if (!this.showDebugOutput) continue;
	            // show a marker if asked for debug output

	            serializedValue = String.asSource("{GWT Java Obj}");
	        // We could return the string value via an implicit toString using "" + value
	        // but this won't eval successfully
	        } else if (key == isc.gwtModule) {
	            if (!this.showDebugOutput) continue;
	            serializedValue = String.asSource("{GWT Module}");
	        } else if (isJavaObj) {
	            serializedValue = (value == null ? null : String.asSource(value + ""));

	        } else {
	            serializedValue =
	                this._serialize(value,
	                                    (prefix != null ? prefix + isc.Comm.indent : null),
	                                    otherObjPath);
	        }

	        // skip values that resolve to undefined
	        //if (serializedValue === undef) {
	        //    continue;
	        //}

	        // now output the key : value pair
	        if (prefix != null) output.append("\n", prefix, isc.Comm.indent);

	        // NOTE: need to concat serializedValue to have null/undef properly handled, normally
	        // skipped by StringBuffers
	        output.append(keyStr, ":" + serializedValue, ",");

	        if (prefix != null) output.append(" ");
	    }
	    // get rid of the trailing comma, if any
	    output = output.release(false);
	    var commaChar = output.lastIndexOf(",");
	    if (commaChar > -1) output = output.substring(0, commaChar);

	    // add the end object marker
	    if (prefix != null) output += "\n" + prefix;
	    output += "}";

	    // and return the output
	    return output;
	},
});
