isc.defineClass("IsysDetailViewer", "DetailViewer").addProperties({

	formatCellValue: function (value, record, field) {
		if (value) {
			if (record._isc_tree && field.name == "parentId") {
				var parentKey = "_parent_" + record._isc_tree;
				
				if (record[parentKey] && record[parentKey].name) {
					return record[parentKey].name;
				}
			} else if(isc.isA.Date(value)) {
				return this._formatDataType(record, field, value);
			} else if (field.type == "phoneNumber" && field.mask) {
				var newValue = new String(field.mask);
				
		        for (var i = 0; i < value.length; i++) {
		            newValue = newValue.replace(/#/, value[i]);
		        }
		        
		        return newValue;
			}
		}

		var ds = isc.DS.get(this.dataSource);
		var dsField = ds.fields[field.name];
		var key = record[field.name];
		
		if (dsField && dsField.valueMap && key) {
			return dsField.valueMap[key];
		}
		
		return (value ? value : "");
	}
	
});
