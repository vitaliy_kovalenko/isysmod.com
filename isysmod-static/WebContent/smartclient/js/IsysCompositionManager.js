isc.defineClass("IsysCompositionManager").addProperties({
	
//	dataSource:null,
	
	treeMenu:null,
	
	listGrid:null,
	
	treeGrid:null,
	
	currentGrid:null,
	
	tabManager:null,
	
	statusBar:null,
	
	splitPane:null,

	getRootGrid : function() {
		return this.listGrid;
	},
	
	/*** From EnterPoint ***/
	onMenuNodeSelected : function (dataSource, criteria, title) {
		this.splitPane.setDetailPane(this.tabManager);
		var ds = isc.DS.get(dataSource);
		this.currentGrid = this._isHierarchical(ds) ? this.treeGrid : this.listGrid;
		this.currentGrid.setDataSource(ds);
		this.splitPane.setListPane(this.currentGrid);
		this.tabManager.addRelatedTabs(ds);
		this.tabManager.selectDataSource(ds);
		this.splitPane.showListPane(title);
		this.currentGrid.performFetch(criteria);
		this.resetStatus();
	},
	
	onMenuNodeDeselected : function () {
		this.currentGrid.deselectDataSource();
		this.tabManager.deselectDataSource();
		this.tabManager.removeRelationTabs();
		this.resetStatus();
	},
	
	onRecordSelected : function (record) {
		this.tabManager.selectRecord(record);
		this.splitPane.showDetailPane();
		this.resetStatus();
	},
	
	onRecordDeselected : function () {
		this.tabManager.deselectRecord();
		this.resetStatus();
	},
	
	onRecordAddition : function () {
		this.tabManager.deselectRecord();
		this.tabManager.showMainForm();
		this.resetStatus();
		this.statusBar.setStatus(isc.msg.Addition);
	},
		
	/*** From Grid ***/
	onBeforFetch : function() {
		this.statusBar.setStatus(isc.msg.Pending_response);
	},
	
	onFetchComplete : function() {
		this.resetStatus();
	},
	
	/*** Form events ***/
	onRecordEditing : function() {
		this.statusBar.setStatus(isc.msg.Editing);
	},
	
	onFormReset : function() {
		this.resetStatus();
	},
	
	onBeforSubmit : function(operationType) {
		this.statusBar.setStatus(isc.msg.Saving);
	},
	
	onSuccessSubmit: function(operationType, fromRelation) {
		if (fromRelation) {
			// Использую только для того, чтобы обновить valueMap автоматом во всех панелях.
			// Возможно, есть способ лучше
			this.listGrid.refreshData();
		}
		
		this.resetStatus();
	},
	
	onValidationFail : function(operationType) {
		this.statusBar.setStatus(isc.msg.Validation_errors);
	},
	
	onSubmitErrors : function(operationType, dsResponse) {
		this.statusBar.setStatus(isc.msg.Completed_with_errors);
	},
	
	resetStatus : function() {
		this.tabManager.resetStatus();
		this.statusBar.setStatus(isc.msg.Ready);
	},
	
	_isHierarchical : function(dataSource) {
		return dataSource.fields && isc.isAn.Object(dataSource.fields) && !!dataSource.fields.parentId;
	}
	
});
