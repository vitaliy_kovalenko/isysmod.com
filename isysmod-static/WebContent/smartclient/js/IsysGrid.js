isc.IsysGridProperties = {
	
	height:"100%",
	
	autoFetchDisplayMap:false, // It prevents selection one million rows from the related table
	
	drawAllMaxCells: 0, // Fix paging
	
	alternateRecordStyles:true,
	
	showEmptyMessage:false,
	
	canFreezeFields:isc.Browser.isDesktop,
	
	canEdit: true,
	
	modalEditing: true,
	
	canEditHilites: true,
	
	dataProperties:{useClientSorting: false, useClientFiltering: false},
	
	/*** add-ons ***/
	tabManager:null,
	
	tabIndex:null,
	
	relationType:null,
	
	foreignKey:null,
	
	manyToManyConfig:{},
	
	statusBarMembers:[],
	
	_statusBarLabel:null,
	
	_addButton:null,
	
	addButtonTitle:null,
	
	_removeButton:null,
	
	removeButtonTitle:null,
	
	suppressDefaultButtons:null,
	
	/**
	 * Redefined in EnterPoint
	 */
	onSelectionChanged : function(record, state) {
	},
	
	onBeforFetch : function() {
		if (compositionManager) {
			compositionManager.onBeforFetch();
		}
	},
	
	onFetchComplete : function() {
		if (compositionManager) {
			compositionManager.onFetchComplete();
		}
	},
	
	editorEnter : function(record, value, rowNum, colNum) {
		if (compositionManager) {
			compositionManager.onRecordEditing();
		}
	},
	
	editorExit : function(editCompletionEvent, record, newValue, rowNum, colNum) {
		if (compositionManager) {
			if (this.hasChanges()) {
				compositionManager.onBeforSubmit();
			} else {
				compositionManager.onFormReset();
			}
		}
	},
	
	editComplete : function(rowNum, colNum, newValues, oldValues, editCompletionEvent, dsResponse) {
		if (compositionManager) {
			compositionManager.onSuccessSubmit();
		}
	},
	
	editFailed : function(rowNum, colNum, newValues, oldValues, editCompletionEvent, dsResponse) {
		if (compositionManager) {
			if (!dsResponse || dsResponse.status == isc.DSResponse.STATUS_VALIDATION_ERROR) {
				compositionManager.onValidationFail();
			} else {
				compositionManager.onSubmitErrors();
			}
		}
	},
	
	onOperationComplete : function() {
		if (this._addButton) {
			this._addButton.setDisabled(false);
		}
		
		this._statusBarLabel.setContents(isc.msg.Total + ": " + this.getTotalRows());
		
		this.onFetchComplete();
	},
	
	initWidget : function () {
		this._statusBarLabel = isc.Label.create({
			align: "center"
		});
		
		this._addButton = isc.IButton.create({
			grid:this,
			title: this.addButtonTitle ? this.addButtonTitle : isc.msg.Add,
			disabled: true,
			click: function() {
				this.grid.onAddButtonClick();
				this.setDisabled(true);
				return false;
			}
		});
		
		this._removeButton = isc.IButton.create({
			grid:this,
			title: this.removeButtonTitle ? this.removeButtonTitle : isc.msg.Remove,
			disabled: true,
			click: function() {
				this.grid.onRemoveButtonClick();
				this.setDisabled(true);
				return false;
			}
		});
		
		var toolStripMembers = [this._statusBarLabel].concat(this.statusBarMembers);
		
		if (this.suppressDefaultButtons !== true) {
			toolStripMembers = toolStripMembers.concat([isc.LayoutSpacer.create({ width:"*" }), this._addButton, this._removeButton]);
		}

		this.gridComponents = ["filterEditor", "header", "body", isc.ToolStrip.create({
			width: "100%",
			membersMargin:2,
			layoutLeftMargin:4,
			layoutRightMargin:16,
		    members: toolStripMembers
		})];
		
		this.Super("initWidget", arguments);
	},
	
	/**
	 * Redefined in EnterPoint
	 */
	onAddButtonClick: function() {
		if (!this.tabManager) {
			return;
		}
		
		var rootRecord = this.tabManager.getSelectedRecordFromRootGrid();
		
		if (!rootRecord) {
			return;
		}
		
		if (this.relationType == "manyToMany") {
			this.onBeforFetch();
			this._fetchUnrelated_manyToMany(rootRecord);
		} else if (this.relationType == "oneToMany") {
			this.startEditingNew();
		}
	},
	
	onRemoveButtonClick: function() {
		if (this.relationType == "manyToMany") {
			this._fetchSelected_manyToMany();
		} else /*if (this.relationType == "oneToMany")*/ {
			var grid = this;
			this.removeSelectedData(function(dsResponse, data, dsRequest) {
				if (!grid.relationType) { // this grid is root
					grid.onSelectionChanged(); // Clear child panels
				}
			});
		}
	},

	_fetchValueMapData : function () {
		;// Вы блядь охуели!
	},
	
	_unsortOnChange : function (newValues, oldValues) {
		; // Это вообще пиздец... слов нет...
	},
	
	_setFieldsValueMap : function() {
		var ds = isc.DS.get(this.dataSource);
		
		for (var code in ds.fields) {
			if (!ds.fields.hasOwnProperty(code)) {
				continue;
			}
			
			var valueMap = ds.fields[code].valueMap;
			
			if (valueMap && !isc.isAn.emptyObject(valueMap)) {
				this.setValueMap(ds.fields[code].name, ds.fields[code].valueMap);
			}
		}
	},
	
	dataArrived : function (startRow,endRow,dataFromCache) {
		this._setFieldsValueMap();
		this.Super("dataArrived", [startRow,endRow,dataFromCache]);
		
		if (startRow == 0) {
			this.onOperationComplete();
		}
	},
	
	selectionChange: function(record, state) {
		var selectedRecords = this.getSelectedRecords();
		
		if (state) {
			if (this._removeButton) {
				this._removeButton.setDisabled(false);
			}
			//var mixed = selectedRecords.length > 1 ? selectedRecords.length : this.getRecordIndex(record) + 1;
		} else if (selectedRecords.length == 0) {
			if (this._removeButton) {
				this._removeButton.setDisabled(true);
			}
			//this._statusBarLabel.setContents(isc.msg.Total + ": " + this.getTotalRows());
		}
		
		if (this._addButton) {
			this._addButton.setDisabled(false);
		}
		
		this._statusBarLabel.setContents(selectedRecords.length + " " + isc.msg.of + " " + this.getTotalRows());
		this.onSelectionChanged(record, state);
	},
	
	_fetchRelated_oneToMany : function(record) {
		var criteria = {};
		criteria[this.foreignKey] = record.id;
		this.performFetch(criteria);
	},
	
	_fetchRelated_manyToMany : function(record) {
		var criteria = {
				_constructor:"AdvancedCriteria",
				operator:"and",
				criteria:[{
				          fieldName:this.foreignKey,
				          operator:"equals",
				          value:record.id
				}]
		};
		
		this.performFetch(criteria);
	},
	
	_fetchUnrelated_manyToMany : function(record) {
		var ds = isc.DS.get(this.dataSource);
		var parentGrid = this;
		
		var criteria = {
				_constructor:"AdvancedCriteria",
				operator:"and",
				criteria:[{
				          fieldName:"id", // any
				          operator:"notExists",
				          value:record.id,
				          relationBean:this.manyToManyConfig.relationBean,
				          selfKey:this.manyToManyConfig.selfKey,
				          foreignKey:this.manyToManyConfig.foreignKey
				}]
		};
		
		var popup = isc.Window.create({
		    autoSize: isc.Browser.isDesktop,
		    canDragReposition: isc.Browser.isDesktop,
		    canDragResize: isc.Browser.isDesktop,
		    autoCenter: isc.Browser.isDesktop,
		    isModal: true,
		    showMinimizeButton: false,
		    title: "Items to add:",
		    items: [
		        isc.IsysListGrid.create({
		        	ID:"popupGrid",
		        	width: isc.Browser.isDesktop ? 1000 : "100%",
		        	height: isc.Browser.isDesktop ? 500 : "100%",
		        	canEdit: false,
		        	showFilterEditor: true,
		        	filterOnKeypress: false,
		        	selectionAppearance: "checkbox",
		        	canSelectAll: true,
		        	suppressDefaultButtons: true,
		        	dataSource: ds,
		        	initialSort: ds.sortBy,
		        	initialCriteria: criteria,
		        	autoFetchData: true,
				    statusBarMembers:[
	  			        isc.LayoutSpacer.create({ width:"*" }),
	  			        isc.IButton.create({
	  						disabled:true,
	  						title: isc.msg.Add,
	  						click: function() {
	  							popup.close();
	  							parentGrid._linkUnrelated_manyToMany(popupGrid.getSelectedRecords());
	  							popup.destroy();
	  							return false;
	  						}
	  					})
	  			    ],
	  			    selectionUpdated: function(record, recordList) {
	  			    	if (recordList && recordList.length) {
	  			    		this.statusBarMembers[1].setDisabled(false);
	  			    	} else {
	  			    		this.statusBarMembers[1].setDisabled(true);
	  			    	}
	  			    }
		        })
		    ],
		    closeClick : function() {
		    	this.close();
		    	
		    	if (parentGrid._addButton) {
		    		parentGrid._addButton.setDisabled(false);
		    	}
		    	
		    	this.destroy();
		    	return false;
		    }
		});
		
		popup.show();
	},
	
	_linkUnrelated_manyToMany : function(records) {
		if (!this.tabManager) {
			return;
		}
		
		var rootRecord = this.tabManager.getSelectedRecordFromRootGrid();
		
		if (!rootRecord || !records.length) {
			//this._addButton.setDisabled(false);
			return;
		}

		var ds = isc.DS.get(this.manyToManyConfig.relationBean);
		var grid = this;

		if (records.length > 1) {
			RPCManager.startQueue();
		
			for (var i = 0; i < records.length; i++) {
				var data = {}
				data[this.manyToManyConfig.selfKey] = rootRecord.id;
				data[this.manyToManyConfig.foreignKey] = records[i].id;
				ds.addData(data);
			}

			RPCManager.sendQueue(function(response) {
				grid.invalidateCache();
			});
		} else {
			var data = {}
			data[this.manyToManyConfig.selfKey] = rootRecord.id;
			data[this.manyToManyConfig.foreignKey] = records[0].id;
			
			ds.addData(data, function(dsResponse, data, dsRequest) {
				grid.invalidateCache();
			});
		}
	},
	
	_fetchSelected_manyToMany : function() {
		if (!this.tabManager) {
			return;
		}
		
		var rootRecord = this.tabManager.getSelectedRecordFromRootGrid();
		var records = this.getSelectedRecords();
		
		if (!rootRecord || !records.length) {
			//this._addButton.setDisabled(false);
			return false;
		}
		
		var ds = isc.DS.get(this.manyToManyConfig.relationBean);
		var grid = this;
		
		if (records.length > 1) {
			var queue = [];
			RPCManager.startQueue();
			
			for (var i = 0; i < records.length; i++) {
				var criteria = {
						_constructor:"AdvancedCriteria",
						operator:"and",
						criteria:[
						          {
							          fieldName:this.manyToManyConfig.selfKey,
							          operator:"equals",
							          value:rootRecord.id
							      },
							      {
							          fieldName:this.manyToManyConfig.foreignKey,
							          operator:"equals",
							          value:records[i].id
							      }
						]
				};
				
				ds.fetchData(criteria, function (dsResponse, data, dsRequest) {
					queue = queue.concat(data);
				});
			}
			
			RPCManager.sendQueue(function(responses) {
				grid._unlinkRelated_manyToMany(queue);
			});
		} else {
			var criteria = {
					_constructor:"AdvancedCriteria",
					operator:"and",
					criteria:[
					          {
						          fieldName:this.manyToManyConfig.selfKey,
						          operator:"equals",
						          value:rootRecord.id
						      },
						      {
						          fieldName:this.manyToManyConfig.foreignKey,
						          operator:"equals",
						          value:records[0].id
						      }
					]
			};
			
			ds.fetchData(criteria, function (dsResponse, data, dsRequest) {
				grid._unlinkRelated_manyToMany(data);
			});
		}
	},
	
	_unlinkRelated_manyToMany : function(records) {
		if (!records.length) {
			//this._addButton.setDisabled(false);
			return false;
		}
		
		var primaryKeys = [];
		var ds = isc.DS.get(this.manyToManyConfig.relationBean);
		var grid = this;
		
		for (var i = 0; i < records.length; i++) {
			primaryKeys.push({id:records[i].id});
		}
		
		if (primaryKeys.length > 1) {
			RPCManager.startQueue();
			
			for (var i = 0; i < primaryKeys.length; i++) {
				ds.removeData(primaryKeys[i]);
			}
			
			RPCManager.sendQueue(function(responses) {
				grid.invalidateCache();
			});
		} else {
			ds.removeData(primaryKeys[0], function (dsResponse, data, dsRequest) {
				grid.invalidateCache();
			});
		}
	},
	
	// Called from TabSet for children grids
	selectRecord: function(record) {
		if (this.relationType == "oneToMany") {
			this._fetchRelated_oneToMany(record);
		} else if (this.relationType == "manyToMany") {
			this._fetchRelated_manyToMany(record);
		}
	},
	
	// Called from TabSet for children grids
	deselectRecord: function() {
		this.setData([]);
		
		if (this._addButton) {
			this._addButton.setDisabled(true);
		}
	},
	
	deselectDataSource : function() {
		if (this.filterEditor) {
			var dynamicForm = this.filterEditor.getEditForm();
			
			if (dynamicForm) {
				/**
				 * Не сбрасывается фильтрация
				 */
				dynamicForm.setData({}); // FIXME: SmartClient bug
			}
		}
		
		this.clearSort();
		this.deselectAllRecords();
		this.setData([]);
		
		if (this.body) {
			/**
			 * Приходится перерисовывать, иначе ошибка start/endRow при переключении
			 * с грида, на котором отображена не первая страница,
			 */
			this.body.redraw("setData"); // FIXME: SmartClient bug
		}
	},
	
	setDataSource : function(dataSource) {
		if (this.filterEditor) {
			/**
			 * Иначе он ищет dataPath на старом dataSource
			 */
			this.filterEditor.setDataSource(dataSource); // FIXME: SmartClient bug
		}
		
		this.Super("setDataSource", [dataSource]);
		var ds = isc.DS.get(dataSource);
		
		if (!this.autoFetchData && ds.sortBy) {
			this.initialSort = ds.sortBy;
		}
	},

	formatCellValue : function(value, record, rowNum, colNum) {
		var field = this.getField(colNum);
		
		if (field && value) {
			if (field.format && (isc.isA.Number(value) || isc.isA.Date(value))) {
	            value = isc.isA.Number(value) ? isc.NumberUtil.format(value, field.format)
	                                          : isc.DateUtil.format(value, field.format);
	
	        } else if (field.type == "phoneNumber" && field.mask) {
				var newValue = new String(field.mask);
				
		        for (var i = 0; i < value.length; i++) {
		            newValue = newValue.replace(/#/, value[i]);
		        }
		        
		        var plus = field.mask[0] == "+" ? "+" : "";
		        value = "<a href='tel:" + plus + value + "' class='sc_phoneNumber'>" + newValue + "</a>";
			} else if (field && !field._suppressTypeFormatting) {
                value = this.applyCellTypeFormatters(value, record, field, rowNum, colNum);
            }
		}
		
		return value;
	},
	
	editCriteria : function () {
	    var filterBuilder = isc.FilterBuilder.create({
	        dataSource:this.dataSource,
	        criteria: this.data && this.data.getCriteria ? this.data.getCriteria() : this.initialCriteria
	    });
	    var _this = this;
	    var theWindow = isc.Window.create({
	    	autoDraw:true,
	    	autoSize: isc.Browser.isDesktop,
		    canDragReposition: isc.Browser.isDesktop,
		    canDragResize: isc.Browser.isDesktop,
		    autoCenter: isc.Browser.isDesktop,
		    showMinimizeButton: false,
	    	overflow: "visible",
			isModal: true,
			showModalMask: true,
			bodyProperties : {
			    layoutMargin: 8,
			    membersMargin: 8,
			    overflow: "visible"
			},
	        title:"Define Filter",
	        items : [
	            filterBuilder,
	            isc.IButton.create({
					title:"Apply",
					layoutAlign:"right",
					click : function () {
						var criteria = filterBuilder.getCriteria();
						_this.editCriteriaReply(criteria);
						this.parentElement.parentElement.closeClick();
					}
				})
	        ]
	    });
	},
	
	/**
	 * Вы еще данные на сервере не сохранили, а уже саммари обновляете...
	 */
	storeUpdatedEditorValue : function (suppressChange, editCol) {
	    if (!this.isDrawn() || !this._editorShowing) return;

	    var editRow = this.getEditRow();

	    if (editCol == null) editCol = this.getEditCol();


	    var editField = this.getField(editCol),
	        editForm = this._editRowForm,
	        editItem = this.getEditFormItem(editCol),
	        value;
	    if (editItem) {
	        if (editItem._itemValueIsDirty()) editItem.updateValue();

	        var itemValue = editItem.getValue();
	        value = this._parseEditorValue(itemValue,
	                                         editField, editRow, editCol);
	        // Convert undefined to explicit null so we actually clear out the value
	        // for the field if appropriate.

	        var undef;
	        if (value === undef) value = null;


	        var oldVal = this._getEditValue(editRow, editCol),
	            undef;
	        if (oldVal === undef || !this.fieldValuesAreEqual(editField, oldVal, value)) {
	            var type = isc.SimpleType.getType(editField.type);
	            if (type && type.getAtomicValue != null) {
	                // calling form.getValue(fieldName) rather than item.getValue() will give
	                // us the opaque value - what we actually want to store.
	                var opaqueValue = editForm.getValue(editItem.name);
	                // If parseEditorValue changed it, we'll have to apply the change to this
	                // opaqueValue of course.

	                if (itemValue != value && type.updateAtomicValue != null) {
	                    type.updateAtomicValue(value, opaqueValue);
	                }
	                value = opaqueValue;
	            }
	            this.setEditValue(editRow, editCol, value, true, suppressChange, true);
	        }
	    }
	    // Pick up the entire values object from the form in case developer code directly
	    // manipulated the edit-form values without going through setEditValue()

	    if (this.allowEditFormValueManipulation) {
	        var changed = false,
	            formValues = editForm.getChangedValues(),
	            currentValues = this.getEditDisplayValues(editRow, editCol, true),
	            untestedFields = isc.getKeys(formValues);

	        // If we're editing a Tree, ensure we don't attempt to store out
	        // tree meta data or the children array

	        if (isc.isA.Tree(this.data)) {
	            formValues = this.data.getCleanNodeData(formValues, false);
	        }

	        var completeFields = {};
	        this.getAllFields().map(function (field) { completeFields[field.name] = field; });
	        var undef;
	        for (var fieldName in currentValues) {
	            untestedFields.remove(fieldName);

	            // We already pulled the value from the current focused edit item.
	            if (editField && fieldName == editField.name) continue;

	            var field = this.getSpecifiedField(fieldName),
	                fieldVal = formValues[fieldName];

	            // Skip anything which is unpopulated in the "changed" form values object
	            if (fieldVal === undef) continue;

	            if (editForm.getItem(fieldName) != null && field) {
	                fieldVal = this._parseEditorValue(fieldVal,
	                                     field, editRow, this.getFieldNum(fieldName));
	            }
	            // do not propagate differences in user formula/summary fields
	            if (field && (field.userFormula || field.userSummary ||
	                          field.type == this._$summary))
	            {

	                continue;
	            }

	            // if the value is unchanged, carry on.
	            if (this.fieldValuesAreEqual(
	                    this.getSpecifiedField(fieldName),
	                    fieldVal, currentValues[fieldName]))
	            {
	                continue;
	            }

	            // if the field and value appear to be a local scribbling, skip it
	            if (completeFields[fieldName] == null && fieldVal === undef) {
	                continue;
	            }
	            // No need to worry about converting from atomic to opaque value - the
	            // "values" object on the form should already be the opaque version.


	            changed |= this.setEditValue(editRow, fieldName, fieldVal,
	                                         true, true, true) != false;
	        }
	        // Check for any field-values specified directly on the form which weren't
	        // present in the original record object.
	        for (var i = 0; i < untestedFields.length; i++) {
	            var fieldName = untestedFields[i];
	            // do not propagate changes to the user field caches!
	            if (fieldName == null || fieldName == "_cache_" + this.ID) continue;

	            var value = formValues[fieldName];
	            // if the value is unchanged, carry on.
	            if (this.fieldValuesAreEqual(
	                    this.getSpecifiedField(fieldName),
	                    value, currentValues[fieldName]))
	            {
	                continue;
	            }

	            // if the field and value appear to be a local scribbling, skip it
	            if (completeFields[fieldName] == null && value === undef) continue;
	            changed |= this.setEditValue(editRow, fieldName, value,
	                                         true, true, true) != false;
	        }

//	        if (changed) {
//	            this.calculateRecordSummaries(this.data.get(editRow), null, true, true, true);
//	        }
	    }
	}

};

isc.ClassFactory.defineClass("IsysListGrid", "ListGrid").addProperties(isc.IsysGridProperties);
isc.ClassFactory.defineClass("IsysTreeGrid", "TreeGrid").addProperties(isc.IsysGridProperties);

isc.IsysListGrid.addProperties({
	performFetch: function(criteria, callback) {
		this.onBeforFetch();
		
		if (this.initialSort) {
			this.setSort(this.initialSort);
		}
		
		var grid = this;
		this.fetchData(criteria, function(dsResponse, data, dsRequest) {
			// Группируем после получения данных, иначе он сделает запрос на 1000 строк
			if (this.groupBy) {
				grid.groupBy(this.groupBy);
				
				if (grid.initialSort) {
					grid.setSort(grid.initialSort);
				}
			}
			
			if (callback) {
				callback(dsResponse, data, dsRequest);
			}
		});
	}
});

isc.IsysTreeGrid.addProperties({
	useAllDataSourceFields:true, // По дефолту он не работает с полями из DataSource
	createDefaultTreeField:false, // Убираем дефолтный нод дерева
	autoFetchTextMatchStyle:"equals",
	dataFetchMode:"paged",
	nodeIcon:"[APPFILES]/icons/16/document-2.png",
	folderIcon:"[APPFILES]/icons/16/folder.png",
	closedIconSuffix:"",
	
	performFetch: function(criteria, callback) {
		this.onBeforFetch();
		
		if (this.initialSort) {
			this.setSort(this.initialSort);
		}

		this.fetchData(criteria, function(dsResponse, data, dsRequest) {
			if (callback) {
				callback(dsResponse, data, dsRequest);
			}
		});
	}
});
