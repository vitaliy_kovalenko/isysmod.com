// Top panel
isc.Menu.create({
	ID: "langaugeMenu",
	showShadow: true,
	data: [
		{
			title: isc.msg.langaugeEnMsg,
			icon: "[APPFILES]/icons/flags/16/US.png",
			click: function() {
				window.location.href = "?language=en";
			}
		},
		{
			title: isc.msg.langaugeRuMsg,
			icon: "[APPFILES]/icons/flags/16/RS.png",
			click: function() {
				window.location.href = "?language=ru"
			}
		}
	]
});

isc.ToolStrip.create({
	ID: "toolStrip",
	width: "100%",
	height:28,
	membersMargin: 2,
	layoutLeftMargin: 8,
	layoutRightMargin: 8,
	members: [
		isc.Label.create({
			ID: "nameLabel",
			contents: isc.msg.wellcomeMsg
		}),
		isc.LayoutSpacer.create({ width:"*" }),
		isc.ToolStripMenuButton.create({
			ID: "localeBtn",
			title: Canvas.imgHTML("[APPFILES]/icons/flags/16/" + isc.msg.flag + ".png"),
			menu: langaugeMenu
		}), "separator",
		isc.ToolStripButton.create({
			ID: "logoutBtn",
			title: isc.msg.logoutMsg,
			click: function() {
				document.logoutForm.submit();
			}
		})
	]
});

//Main widgets and managers
var compositionManager = isc.IsysCompositionManager.create();

compositionManager.listGrid = isc.IsysListGrid.create({
	compositionManager:compositionManager,
	minFieldWidth: 120,
	showFilterEditor: true,
	allowFilterOperators: true,
	filterOnKeypress: false, // Иначе не работает комбобокс в фильтре
	canGroupBy:true,
	groupStartOpen:"all",
	canAddFormulaFields: true,
	canAddSummaryFields: true,
	useAdvancedFieldPicker: true,
	advancedFieldPickerThreshold: 0,
	//spanContextMenu: true,
	//showCellContextMenus: true,
	filterUsingText: "Фильтр...",
	statusBarMembers:[
		"separator",
   		isc.ToolStripButton.create({
 			icon:"[APPFILES]/icons/16/excel.png",
 			click: function() {
 				isc.say("Эксопрт как EXCEL");
 			}
 		}),
   		isc.ToolStripButton.create({
 			icon:"[APPFILES]/icons/16/filter1.png",
 			click: function() {
 				compositionManager.listGrid.editCriteria();
 			}
 		}),
// 		isc.ToolStripButton.create({
// 			icon:"[APPFILES]/icons/16/idea.png",
// 			click: function() {
// 				compositionManager.listGrid.editHilites();
// 			}
// 		}),
// 		isc.ToolStripButton.create({
// 			title:"Formula",
// 			click: function() {
// 				compositionManager.listGrid.addFormulaField();
// 			}
// 		}),
// 		isc.ToolStripButton.create({
// 			title:"Summary",
// 			click: function() {
// 				compositionManager.listGrid.addSummaryField();
// 			}
// 		}),
 		isc.ToolStripButton.create({
 			icon:"[APPFILES]/icons/16/gear.png",
 			click: function() {
 				var state = compositionManager.listGrid.getViewState();
 				isc.Offline.put("exampleState", state);
 			}
 		})
	],
	onSelectionChanged : function(record, state) {
		if (state) {
			compositionManager.onRecordSelected(record);
		} else {
			compositionManager.onRecordDeselected();
		}
	},
	onAddButtonClick: function() {
		this.deselectAllRecords();
		compositionManager.onRecordAddition();
	}
});

compositionManager.treeGrid = isc.IsysTreeGrid.create({
	compositionManager:compositionManager,
	minFieldWidth:120,
	showFilterEditor:true,
	allowFilterOperators:true,
	filterOnKeypress:false, // Иначе не работает комбобокс в фильтре
	showConnectors: true,
	canReorderRecords: true,
	canAcceptDroppedRecords: true,
	showDropIcons: false,
	statusBarMembers:[
		"separator",
   		isc.ToolStripButton.create({
 			icon:"[APPFILES]/icons/16/excel.png",
 			click: function() {
 				isc.say("Эксопрт как EXEL");
 				return false;
 			}
 		})
	],
	onSelectionChanged : function(record, state) {
		if (state) {
			compositionManager.onRecordSelected(record);
		} else {
			compositionManager.onRecordDeselected();
		}
	},
	onAddButtonClick: function() {
		this.deselectAllRecords();
		compositionManager.onRecordAddition();
	}
});

compositionManager.tabManager = isc.IsysTabManager.create({
	margin:2,
	compositionManager:compositionManager,
	initialDataSource:"User"
});

compositionManager.treeMenu = isc.IsysTreeMenu.create({
	dataSource: "MenuDS",
	autoFetchData: true,
	showHeader: false,
	selectionType:"single",
	nodeIcon:"[APPFILES]/icons/16/document-2.png",
	folderIcon:"[APPFILES]/icons/16/folder.png",
	showDropIcons: false,
	dataProperties:{useClientSorting: false, useClientFiltering: false},
	closedIconSuffix:"",
    selectionChanged : function(record, state) {
		if (state) {
			compositionManager.onMenuNodeSelected(record.datasource, record.isysCriteria, record.name);
		} else {
			compositionManager.onMenuNodeDeselected();
		}
	}
});

compositionManager.statusBar = isc.IsysStatusBar.create({
	height:28,
	membersMargin:2,
	layoutLeftMargin:8,
	layoutRightMargin:8
});

// Page layouts
compositionManager.splitPane = isc.SplitPane.create({
	navigationPaneWidth:200,
	showListToolStrip:!isc.Browser.isDesktop,
	showNavigationBar:!isc.Browser.isDesktop,
	showDetailToolStrip:!isc.Browser.isDesktop,
	navigationTitle:isc.msg.Navigation,
	detailPane:isc.Canvas.create(), //compositionManager.tabManager,
	listPane:null, //compositionManager.listGrid,
	navigationPane:compositionManager.treeMenu
});

isc.VLayout.create({
	ID:"mainVLayout",
	width:"100%",
	height:"100%",
	autoDraw: true,
	members:[
	    toolStrip,
	    compositionManager.splitPane,
	    compositionManager.statusBar
	]
});
