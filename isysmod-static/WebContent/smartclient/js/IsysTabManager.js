isc.defineClass("IsysTabManager", "TabSet").addProperties({
	
	initialDataSource: null,
	
	compositionManager:null,
	
	detailViewer:null,
	
	formPane:null,
	
	destroyPanes:true,
	
	initWidget : function () {
		this.detailViewer = isc.IsysDetailViewer.create({
			dataSource:this.initialDataSource,
			margin:12,
			showHiddenFields:true,
			canSelectText:true,
			showEmptyMessage:false,
			showDetailFields:true,
			canPickFields:true
		});
		
		this.formPane = isc.IsysFormPane.create({
			tabManager:this,
			tabIndex:1
		});
		
		this.tabs = [
 	   		{title:isc.msg.View, pane:this.detailViewer},
 			{title:isc.msg.Edit, pane:this.formPane}
 		];

		this.Super("initWidget", arguments);
	},
	
	showMainForm : function() {
		this.selectTab(1);
	},
	
	selectRecord: function(record) {
		for (var i =  2; i < this.tabs.length; i++) {
			this.tabs[i].pane.selectRecord(record);
		}
		
		this.detailViewer.setData(record);
		this.formPane.selectRecord(record);
	},
	
	deselectRecord: function() {
		for (var i =  2; i < this.tabs.length; i++) {
			this.tabs[i].pane.deselectRecord();
		}
		
		this.formPane.deselectRecord();
		this.detailViewer.setData();
	},
	
	selectDataSource: function(dataSource) {
//		for (var i =  2; i < this.tabs.length; i++) {
//			this.tabs[i].pane.selectDataSource(dataSource);
//		}
		
		this.formPane.selectDataSource(dataSource);
		this.detailViewer.setDataSource(dataSource);
	},
	
	deselectDataSource: function() {
//		for (var i =  2; i < this.tabs.length; i++) {
//			this.tabs[i].pane.deselectDataSource();
//		}
		
		this.formPane.deselectDataSource();
		this.detailViewer.setDataSource();
	},
	
	addRelatedTabs: function(dataSource) {
		if (dataSource.relations && isc.isAn.Array(dataSource.relations)) {
			for (var i = 0; i < dataSource.relations.length; i++) {
				var relation = dataSource.relations[i];
				var title = relation.title;
				
				if (! title) {
					var fds = isc.DS.get(relation.foreignDataSource);
					title = fds.ID;
				}
				
				this.addTab({title:title});
				this.addRelationPane(relation, i + 2);
			}
		}
	},
	
	removeRelationTabs: function() {
		for (var i = this.tabs.length - 1; i >= 2; i --) {
			this.tabs[i].pane.destroy();
			this.removeTab(this.tabs[i]);
		}
		
		this.selectTab(0);
	},
	
	addRelationPane : function(relation, tabIndex) {
		var pane;
		var ds = isc.DS.get(relation.foreignDataSource);
		
		if (relation.type == "oneToMany" || relation.type == "manyToMany") {
			var props = {
				tabManager: this,
				tabIndex:tabIndex,
				relationType:relation.type,
				initialSort:ds.sortBy,
				dataSource:ds,
				selectionAppearance: relation.type == "manyToMany" ? "checkbox" : "rowStyle",
				manyToManyConfig:relation.type == "manyToMany" ? relation.manyToManyConfig : null,
				foreignKey:relation.foreignKey,
				addButtonTitle:relation.type == "manyToMany" ? isc.msg.Select : null
			};
			
			if (ds.isHierarchical && ds.isHierarchical()) {
				pane = isc.IsysTreeGrid.create(props);
			} else {
				pane = isc.IsysListGrid.create(props);
			}
		} else if (relation.type == "manyToOne" || relation.type == "oneToOne") {
			pane = isc.IsysFormPane.create({
				tabManager: this,
				tabIndex:tabIndex,
				relationType:relation.type,
				foreignKey:relation.foreignKey
			});
			
			pane.selectDataSource(ds);
		}
		
		if (pane) {
			this.setTabPane(tabIndex, pane);
		}
	},
	
	getSelectedRecordFromRootGrid : function() {
		return this.compositionManager.getRootGrid().getSelectedRecord();
	},
	
	resetStatus : function() {
		for (var i = 1; i < this.tabs.length; i++) {
			if (this.tabs[i].pane.resetStatus && this.tabs[i].pane.isEdittingStarts) {
				this.setTabTitle(i, this.tabs[i].realTitle);
				//this.redraw("setTitle");
				this.tabs[i].pane.resetStatus();
			}
		}
	},
	
	/*** Form events ***/
	onRecordEditing : function(tabIndex) {
		if (! this.tabs[tabIndex].realTitle) {
			this.tabs[tabIndex].realTitle = this.tabs[tabIndex].title;
		}
		
		this.setTabTitle(tabIndex, this.tabs[tabIndex].realTitle + "<sup>*</sup>");
		this.compositionManager.onRecordEditing();
	},
	
	onFormReset : function() {
		this.compositionManager.onFormReset();
	},
	
	onBeforSubmit : function(operationType) {
		this.compositionManager.onBeforSubmit(operationType);
	},
	
	onSuccessSubmit: function(operationType, fromRelation) {
		this.compositionManager.onSuccessSubmit(operationType, fromRelation);
	},
	
	onValidationFail : function(operationType) {
		this.compositionManager.onValidationFail();
	},
	
	onSubmitErrors : function(operationType, dsResponse) {
		this.compositionManager.onSubmitErrors();
	}

});
