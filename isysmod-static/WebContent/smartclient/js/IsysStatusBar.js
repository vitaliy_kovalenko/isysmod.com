isc.defineClass("IsysStatusBar", "ToolStrip").addProperties({
	
	statusLabel:null,
	
	initWidget : function () {
		this.statusLabel = isc.Label.create({
			width: "*",
			contents: isc.msg.Ready
		});
		
		this.members = [this.statusLabel];
		
		this.Super("initWidget", arguments);
	},
	
	setStatus : function(text) {
		this.statusLabel.setContents(text);
		this.statusLabel.redraw("setContents");
	}
	
});
