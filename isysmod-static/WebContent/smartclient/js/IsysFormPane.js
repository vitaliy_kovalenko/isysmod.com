isc.defineClass("IsysFormPane", "VLayout").addProperties({
	layoutTopMargin:10,
	layoutRightMargin:10,
	tabIndex:null,
	editForm: null,
	resetButton: null,
	submitButton: null,
	foreignDataSource:null,
	relationType:null,
	foreignKey:null,
	tabManager:null,
	isEdittingStarts:false,
	
	initWidget : function () {
		this.editForm = isc.DynamicForm.create({
			formPane:this,
			//dataSource:this.foreignDataSource,
			autoFetchData:false,
			saveOnEnter:true,
			//showHiddenFields:true,
			useAllDataSourceFields:false,
			showErrorText:true,
			showErrorIcons:false,
			width:"100%",
			colWidths:isc.Browser.isDesktop ? ["15%", "*", "15%", "*"] : ["20%", "*"],
			numCols: isc.Browser.isDesktop ? 4 : 2,
			itemChanged: function(item, newValue) {
				if (!this.formPane.isEdittingStarts) {
					this.formPane.isEdittingStarts = true;
					this.formPane.onRecordEditing();
				}
			}
		});
		
		this.resetButton = isc.IButton.create({
			formPane: this,
			title: isc.msg.Reset,
			disabled:true,
			click: function() {
				this.setDisabled(true);
				this.formPane.editForm.reset();
				this.formPane.afterFormReset();
				return false;
			}
        });
		
		this.submitButton = isc.IButton.create({
			formPane: this,
			title: isc.msg.Submit,
			disabled:true,
			click: function() {
				this.setDisabled(true);
				this.formPane.isEdittingStarts = false;
				
				if (this.formPane.editForm.validate()) {
					var validatedValues = this.formPane.editForm.getValidatedValues();
					
					if (validatedValues) {
						var pane = this.formPane;
						pane.onBeforSubmit();
						var callback = function(dsResponse, data, dsRequest) {
							if (dsResponse.status == isc.DSResponse.STATUS_SUCCESS) {
								pane.afterSuccessSubmit(dsRequest.operationType);
							} else if (dsResponse.status == isc.DSResponse.STATUS_VALIDATION_ERROR) {
								pane.editForm.showErrors();
								pane.afterValidationFail(dsRequest.operationType);
							} else {
								pane.afterSubmitErrors(dsRequest.operationType, dsResponse);
							}
						};
						
						this.formPane.editForm.saveData(callback, {willHandleError:true});
					}
				} else {
					this.formPane.afterValidationFail();
				}
			}
        });
		
		this.members = [this.editForm, isc.HLayout.create({
			height:28,
			layoutTopMargin:10,
			layoutRightMargin:4,
			membersMargin:2,
			align:"right",
			members:[this.submitButton, this.resetButton]
		})];
		
		this.Super("initWidget", arguments);
	},
	
	disableButtons: function() {
		this.resetButton.setDisabled(true);
		this.submitButton.setDisabled(true);
	},
	
	_selectRecordFromGrid: function(record) {
		var ds = isc.DS.get(this.editForm.dataSource);
		
		for (var code in record){
			if (record.hasOwnProperty(code)) {
				var field = ds.fields[code];
				
				if (!field || !field.valueMap) {
					continue;
				}
				
				this.editForm.setValueMap(code, field.valueMap);
			}
		}
		
		this.editForm.editRecord(record);
	},
	
	_selectRecordFromRelation: function(record) {
		var path;
		
		if (isc.isA.String(this.editForm.dataSource)) {
			path = this.editForm.dataSource.toLowerCaseFirst();
		} else {
			path = this.editForm.dataSource.ID.toLowerCaseFirst();
		}
		
		if (record[path]) {
			this.editForm.editRecord(record[path]);
		}
	},
	
	selectRecord: function(record) {
		this.disableButtons();
		this.editForm.clearErrors(true);

		if (this.relationType) {
			this._selectRecordFromRelation(record);
		} else {
			this._selectRecordFromGrid(record);
		}
	},
	
	deselectRecord: function() {
		this.disableButtons();
		this.editForm.editNewRecord();
	},
	
	selectDataSource: function(dataSource) {
		this.disableButtons();
		var fields = [];
		
		if (isc.isA.String(dataSource)) {
			dataSource = isc.DS.get(dataSource);
		}
		
		for (var code in dataSource.fields) {
			if (!dataSource.fields.hasOwnProperty(code)) {
				continue;
			}
			
			if (dataSource.fields[code].canEdit !== false) {
				fields.push(dataSource.fields[code]);
			}
		}
		
		this.editForm.setDataSource(dataSource, fields);
	},
	
	deselectDataSource: function() {
		this.deselectRecord();
	},
	
	/*** Form events ***/
	onRecordEditing: function() {
		this.resetButton.setDisabled(false);
		this.submitButton.setDisabled(false);
		this.tabManager.onRecordEditing(this.tabIndex);
	},
	
	afterFormReset: function() {
		this.submitButton.setDisabled(true);
		this.tabManager.onFormReset();
	},
	
	onBeforSubmit : function(operationType) {
		this.tabManager.onBeforSubmit(operationType);
	},
	
	afterSuccessSubmit: function(operationType) {
		this.resetButton.setDisabled(true);
		
		if (operationType == "add") {
			this.editForm.editNewRecord();
		}
		
		this.tabManager.onSuccessSubmit(operationType, this.relationType != null);
	},
	
	afterValidationFail: function(operationType) {
		this.tabManager.onValidationFail(operationType);
	},
	
	afterSubmitErrors: function(operationType, dsResponse) {
		this.tabManager.onSubmitErrors(operationType, dsResponse);
	},
	
	resetStatus : function() {
		this.isEdittingStarts = false;
	}

});
