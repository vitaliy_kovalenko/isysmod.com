isc.defineClass("IsysTreeMenu", "TreeGrid").addProperties({
	
	folderOpened : function(node) {
		if (node.datasource) {
			if (!node.children || !node.children.length) {
				var tree = this;
				var ds = isc.DS.get(node.datasource);
				var prop = node.childrenProperties && node.childrenProperties[node.datasource] ? node.childrenProperties[node.datasource] : {};

				ds.fetchData(node.isysCriteria, function(dsResponse, data, dsRequest) {
					var undef;
					
					for (var i = 0; i < data.length; i ++) {
						var record = {
								id: data[i].id, //isc.guid(),
								parentId: data[i].parentId === undef ? node.id : data[i].parentId,
								datasource: prop.datasource,
								name: data[i].name,
								isFolder: prop.isFolder,
								childrenProperties: node.childrenProperties
						}
						
						if (prop.foreignKey) {
							var isysCriteria = {}
							isysCriteria[prop.foreignKey] = data[i].id;
							record.isysCriteria = isysCriteria;
						}
						
						tree.addData(record);
					}
				}, {sortBy:ds.sortBy, endRow:75});
				
				// return false;
			}
		}
	}
	
});
