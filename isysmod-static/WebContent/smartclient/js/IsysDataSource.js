isc.defineClass("IsysDataSource", "RestDataSource").addProperties({

	autoCacheAllData: false, // Кеш говно
	
	//jsonPrefix: "<SCRIPT>//'\"]]>>isc_JSONResponseStart>>",
	jsonPrefix:null,
	
	//jsonSuffix: "//isc_JSONResponseEnd",
	jsonSuffix:null,

    operationBindings:[
       {operationType:"fetch", dataProtocol:"postMessage"},
       {operationType:"add", dataProtocol:"postMessage"},
       {operationType:"remove", dataProtocol:"postMessage"},
       {operationType:"update", dataProtocol:"postMessage"}
    ],
    
    isHierarchical : function() {
    	return this.fields && !!this.fields.parentId;
    },
    
    // Turning on IsysManagedCache
    performDSOperation : function (operationType, data, callback, dsRequest) {
    	if (isc.IsysManagedCache && dsRequest) {
    		if (isc.IsysManagedCache.isCacheAble(dsRequest, operationType, data)) {
	    		var range = isc.IsysManagedCache.get(
	    				this.ID,
	    				dsRequest.sortBy,
	    				dsRequest.startRow,
	    				dsRequest.endRow
	    		);
	    		
	    		if (range) {
	    			var dsResponse = {
	    					httpResponseCode: 200,
	    					status: 0,
	    					queueStatus: 0,
	    					totalRows: range.totalRows,
	    					startRow: range.startRow,
	    					endRow: range.endRow,
	    					data: range.data,
	    					invalidateCache: false,
	    					operationType: "fetch",
	    					internalClientContext: dsRequest.internalClientContext
	    			};
	    			
	    			this.fireCallback( callback, "dsResponse,data,request,fromCache", [dsResponse,range.data,dsRequest,true] );
	    			return;
	    		}
    		} else if (operationType == "add" ||
    				operationType == "remove" ||
    				operationType == "update") {
    			isc.IsysManagedCache.invalidate();
    		}
    	}

    	this.Super("performDSOperation", arguments);
    },
    
    transformRequest : function (dsRequest) {
        var protocol = this.getDataProtocol(dsRequest);
        
        if (protocol != "postMessage") {
        	console.log("Only postMessage protocol supported.");
        	return null;
        }

        dsRequest.isRestRequest = !(this.disableQueuing || this.clientOnly);
        dsRequest.dataFormat = this.dataFormat;
        dsRequest.contentType = (dsRequest.dataFormat == "json" ? "application/json" : "text/xml")
        // Set parameter specifying request/response data format
        if (dsRequest.params == null) {
            dsRequest.params = {};
        }
        
        dsRequest.params[this.dataFormatParamName] = this.dataFormat;

        var params = {};
        if (this.database) params.database = this.database;
        if (this.beanClassName) params.beanClassName = this.beanClassName;
        if (dsRequest.operationType != null) params.operationType = dsRequest.operationType;
        if (dsRequest.startRow != null) params.startRow = dsRequest.startRow;
        if (dsRequest.endRow != null) params.endRow = dsRequest.endRow;
        if (dsRequest.textMatchStyle != null) params.textMatchStyle = dsRequest.textMatchStyle;
        if (dsRequest.summaryFunctions != null) params.summaryFunctions = dsRequest.summaryFunctions;
        if (dsRequest.groupBy != null) params.groupBy = dsRequest.groupBy;
        if (dsRequest.componentId != null) params.componentId = dsRequest.componentId; // Useful for debugging
        if (dsRequest.operationId != null) params.operationId = dsRequest.operationId;
        //if (dsRequest.parentNode != null) params.parentNode = isc.Tree.getCleanNodeData(dsRequest.parentNode);
        //if (dsRequest.useStrictJSON != null) params.useStrictJSON = dsRequest.useStrictJSON;
        //if (this.sendClientContext) params.clientContext = dsRequest.clientContext;
        //if (dsRequest.oldValues != null) params.oldValues = dsRequest.oldValues;
        
        if (dsRequest.sortBy && !(dsRequest.sortBy == "-undefined" || dsRequest.sortBy == "undefined")) {
        	params.sortBy = this._getSortBy(dsRequest.sortBy);
        } else if (params.operationType == "fetch" && this.sortBy) {
        	params.sortBy = this._getSortBy(this.sortBy);
        	dsRequest.sortBy = isc.DS.getSortBy(this.sortBy);
        }
        
        if (this.joins && this.joins.length) {
        	//params.joins = this._buildJoins(this.joins);
        	params.joins = [];
        	for (var i = 0; i < this.joins.length; i++) {
        		params.joins.push({ join: this.joins[i] });
        	}
        }

        if (this.autoConvertRelativeDates == true) {
        	dsRequest.data = this.convertRelativeDates(dsRequest.data);
        }

        if (dsRequest.data && !isc.isAn.emptyObject(dsRequest.data)) {
        	params.data = dsRequest.data;
        }
        
        var returnVal;
        
        if (dsRequest.dataFormat == "json") {
            if (params.data != null) {
            	params.data = this.serializeFields(params.data);
            
	            if (params.operationType == "fetch") {
	            	params.data.type = "map";
	        	} else if (params.operationType == "update" && !params.data.type) {
	        		params.data.type = "map";
	        	} else if (this.beanClassName) {
	        		params.data.type = this.beanClassName.toLowerCaseFirst();
	        	}
            }
            
            var settings = {
                prettyPrint: isc.Comm.formatedOutput,
                trimMilliseconds: !!this.trimMilliseconds,
                skipInternalProperties: true
            };

            returnVal = "{\"request\":" + isc.JSON.encode(params, settings) + "}";

        } else {
	        var flags = {
	            ignoreConstructor: true,
	            schema: this
	        };
	
	        var indent = isc.Comm.formatedOutput ? "\n\t" : null;
	        var rootTag = "request";
	        returnVal = this.xmlSerialize(params, flags, indent, rootTag);
        }

        return returnVal;
    },

    transformResponse : function (dsResponse, dsRequest, data) {
        if (dsResponse.status < 0 || !data) {
            if (dsResponse.status == isc.DSResponse.STATUS_LOGIN_REQUIRED
            		|| dsResponse.httpResponseCode == 403) { // Forbidden (when session expired and CSRF incorrect)
            	window.location.replace(isc.RPCManager.credentialsURL);
            	// Prevents warning window
            	dsResponse.status = isc.DSResponse.STATUS_SUCCESS;
            	dsResponse.data = null;
            	return dsResponse;
            }
            
            // If any request failed, queueStatus must be -1
            dsResponse.queueStatus = -1;
            return dsResponse;
        }

        if (dsRequest.dataFormat == "json") {
            if (isc.isAn.Array(data)) {
                var useSingleEntry = data.length == 1 && data[0] && data[0].response != null;
                this.logWarn(
                    "RestDataSource transformResponse(): JSON response text is " +
                    "incorrectly formatted as an Array rather than a simple response " +
                    "object."
                    + (useSingleEntry ?
                        " Array contains a single entry which appears to be a validly " +
                        "formatted response object - using this."
                        : "")
                );
                if (useSingleEntry) data = data[0];
            // If we're passed something with no {response:{...}} block log a warning, and
            // continue.
            } else if (data == null || data.response == null) {
                this.logWarn("RestDataSouce transformResponse(): JSON response text does " +
                "not appear to be in standard response format.");
            }
            
            var rawResponse = data.response || {};
            dsResponse.status = this.getValidStatus(rawResponse.status);
            dsResponse.invalidateCache = rawResponse.invalidateCache == null ? false : rawResponse.invalidateCache;
            dsResponse.queueStatus = this.getValidStatus(rawResponse.queueStatus);

            // if the status is a validation error, convert the errors from XML
            if (dsResponse.status == isc.DSResponse.STATUS_VALIDATION_ERROR) {
                dsResponse.errors = rawResponse.errors;
                // if there's also a general error message grab that too
                if (rawResponse.data != null) dsResponse.data = rawResponse.data;
            // handle being passed a failure status with 'data' being an error string to display
            } else if (dsResponse.status < 0) {
                dsResponse.data = rawResponse.data;
            }
            
            if (rawResponse.data != null && rawResponse.data.record != null) dsResponse.data = rawResponse.data.record;
            if (rawResponse.totalRows != null) dsResponse.totalRows = rawResponse.totalRows;
            if (rawResponse.startRow != null) dsResponse.startRow = rawResponse.startRow;
            if (rawResponse.endRow != null) dsResponse.endRow = rawResponse.endRow;

        } else {
	        if (dsRequest.clientOnly) return dsResponse;
	        dsResponse.status = this.getValidStatus(data.selectString("//status"));
	        dsResponse.invalidateCache = this.getValidInvalidateCache(data.selectString("//invalidateCache"));
	        dsResponse.queueStatus = this.getValidStatus(data.selectString("//queueStatus"));
	        //dsResponse.operationType = data.selectString("//operationType");
	        
	        // if the status is a validation error, convert the errors from XML
	        if (dsResponse.status == isc.DSResponse.STATUS_VALIDATION_ERROR) {
	            var errors = data.selectNodes("//errors");
	            dsResponse.errors = isc.xml.toJS(errors);
	            // if there's also a general error message grab that too
	            var errorMessage = data.selectString("//data");
	            if (errorMessage) dsResponse.data = errorMessage;
	        // handle being passed a raw response where 'data' is an error string to display
	        } else if (dsResponse.status < 0) {
	            dsResponse.data = data.selectString("//data");
	        }
	
	        var totalRows = data.selectNumber("//totalRows");
	        if (totalRows != null) dsResponse.totalRows = totalRows;
	
	        var startRow = data.selectNumber("//startRow");
	        if (startRow != null) dsResponse.startRow = startRow;
	
	        var endRow = data.selectNumber("//endRow");
	        if (endRow != null) dsResponse.endRow = endRow;
        }
        
        if (isc.IsysManagedCache && isc.IsysManagedCache.isCacheAble(dsRequest)) {
    		isc.IsysManagedCache.put(
    				this.ID,
    				dsRequest.sortBy,
    				dsRequest.startRow,
    				dsResponse.totalRows,
    				dsResponse.data
    		);
    	}
        
        if (dsResponse.data && isc.isAn.Array(dsResponse.data)) {
        	this._buildFieldValueMapFromXPath(dsResponse.data);
        }
        
        return dsResponse;
    },
    
//    _buildJoins : function(data) {
//    	var joins = [];
//    	
//    	for (var i = 0; i < data.length; i ++) {
//    		var join = {
//    				owner: data[i].owner,
//    				field: data[i].field
//    		}
//    		
//    		if (data[i].joins) {
//    			join.joins = this._buildJoins(data[i].joins);
//    		}
//    		
//    		if (data[i].select) join.select = data[i].select;
//    		if (data[i].type) join.type = data[i].type;
//    		
//    		joins.push( { join: join } );
//    	}
//    	
//    	return joins;
//    },
    
    _buildFieldValueMapFromXPath : function(data) {
    	for (var code in this.fields) {
			if (!this.fields.hasOwnProperty(code)) {
				continue;
			}
			
			if (
					!this.fields[code].name ||
					!this.fields[code].optionDataSource ||
					!this.fields[code].displayValueXPath) {
				continue;
			}
			
			if (!this.fields[code].valueMap) {
				this.fields[code].valueMap = {};
			}
			
			for (var i = 0; i < data.length; i++) {
				if (! data[i][this.fields[code].name]) {
					continue;
				}
				
				var path = this.fields[code].displayValueXPath.split(/\//g);
				
				if (path.length < 1) {
					continue;
				}
				
				var fieldValue = data[i][this.fields[code].name];
				var displayValue = data[i];
				var notFound = false;
				
				for (var j = 0; j < path.length; j ++) {
					if (!displayValue[path[j]]) {
						notFound = true;
						break;
					}
					
					displayValue = displayValue[path[j]];
				}
				
				if (notFound) {
					continue;
				}
				
				this.fields[code].valueMap[fieldValue] = displayValue;
			}
			
			if (isc.isAn.emptyObject(this.fields[code].valueMap)) {
				delete this.fields[code].valueMap;
			}
		}
	},

    // Helpers for sorting by XPath
    _getSortEntry : function (sortBy) {
    	var isDescending = false, sortField, sortEntry;
		
		if (isc.isA.String(sortBy)) {
			if (sortBy[0] == "-") {
				isDescending = true;
				sortBy = sortBy.substring(1, sortBy.length);
			}
			
			if (this.fields[sortBy]) {
				sortField = this.fields[sortBy];
			}
		} else if (sortBy.property && this.fields[sortBy.property]) {
			isDescending = sortBy.direction && sortBy.direction == "descending";
			sortField = this.fields[sortBy.property];
		}
		
		if (sortField) {
			sortEntry = sortField.displayValueXPath ? sortField.displayValueXPath.replace(/\//g, ".") : sortField.name;
			
			if (isDescending) {
				sortEntry = "-" + sortEntry;
			}
		}
		
		return sortEntry;
    },
    
    _getSortBy : function(sortBy) {
    	var sortEntry, sort = [];

    	if (isc.isAn.Array(sortBy)) {
			for (var i = 0; i < sortBy.length; i++) {
				sortEntry = this._getSortEntry(sortBy[i]);
				
				if (sortEntry) {
					sort[i] = sortEntry;
				}
			}
		} else {
			sortEntry = this._getSortEntry(sortBy);
    		
    		if (sortEntry) {
    			sort[0] = sortEntry;
    		}
		}
    	
    	return sort;
    },
    
    xmlSerialize : function (data, flags, indent, tagName) {
    	isc.Comm._explicitNils = true; // Включаем явные налы
        if (!flags) flags = {};
        
        if (!isc.Comm.omitXSI && this.xmlNamespaces) {
        	flags.nsPrefixes = isc.addProperties({}, isc.makeReverseMap(this.xmlNamespaces));
        }

        var output = isc.SB.create();
        output.append(isc.Comm._xmlOpenTag(tagName, null, null, flags.nsPrefixes, true, false));
        if (flags.nsPrefixes)
        	output.append(this.outputNSPrefixes(flags.nsPrefixes, null, flags));
        data = this.serializeAttributes(data, output, flags);
        var subElements = this.xmlSerializeFields(data, flags, (indent ? indent : null));
        output.append(">", subElements, (indent ? "\n" : null));
        output.append(isc.Comm._xmlCloseTag(tagName, null, flags.nsPrefixes));

        return output.release(false);
    },
    
    outputNSPrefixes : function (prefixes, indent, flags) {
        delete prefixes._nsCount;
        prefixes = isc.makeReverseMap(prefixes);
        var nsDefs = isc.xml._generateNamespaces(prefixes, null, indent);
        return nsDefs;
    },
    
    xmlSerializeFields : function (data, flags, indent) {
        var output = isc.SB.create(),
            flags = flags || {},
            flatData = flags.flatData,
            spoofData = flags.spoofData;

        // output each known field *in order*
        var data = isc.addProperties({}, data); // copy data so we can delete known fields

        if (data.__ref) {
            delete data.__ref;
            delete data.__module;
        }

        var fields = this.getFields();
        for (var fieldName in fields) {
            var field = this.getField(fieldName),
                value = data[fieldName],
                fieldIsComplex = this.fieldIsComplexType(fieldName);

            var childData = data[fieldName];
            if (flags.startRowTag == field.name && childData == null) {
                childData = flags.dsRequest ? flags.dsRequest.startRow : null;
            } else if (flags.endRowTag == field.name && childData == null) {
                childData = flags.dsRequest ? flags.dsRequest.endRow : null;

            // flatData mode: we assume the inbound data is an object with properties
            // that hold values for same-named subelements at various levels in the
            // structure.  This is for flattening structures that have gratuitous nesting
            // and are reasonably editable in a single form.
            // Note that even with flatData=true, "data" may have nested objects, intended
            // as data for a same-named complexType subelement, so flatData amounts to always
            // passing data to child elements of complexType
            } else if (fieldIsComplex && flatData && childData == null) {
                childData = data;
            }

            // NOTE: the xml notion of required is just that an element needs to be there - not
            // that it has to be non-empty
            var mustOutput = (field.xmlRequired && !field.xmlAttribute) ||
                                (data[fieldName] != null || (spoofData && !field.xmlAttribute));

            if (flatData && fieldIsComplex) {
                // HACK: for flatData, if a tag is not marked required, we can't easily
                // tell whether there will be output since subobjects may produce output if
                // the data happens to intersect with some simpleType field at any level of
                // nesting, so we just try serializing and throw the result away if there's
                // nothing
                var tagSchema = this.getSchema(field.type),
                    // HACK: this serialization run doesn't produce output, so disable caching
                    // of NS prefixes
                    origPrefixes = isc.clone(flags.nsPrefixes),
                    fieldsOutput = tagSchema.xmlSerializeFields(childData, flags);
                //this.logWarn("fieldName: '" + fieldName +
                //             "', mustOutput before flatData check: " + mustOutput +
                //             ", fieldsOutput: " + this.echoLeaf(fieldsOutput));
                if (fieldsOutput != null && !isc.isAn.emptyString(fieldsOutput)) {
                    mustOutput = true;
                }
                flags.nsPrefixes = origPrefixes;
            }

            if (mustOutput) {

                if (flatData && fieldIsComplex &&
                    // non-simpleType data is provided
                    data[fieldName] != null && !isc.DS.isSimpleTypeValue(data[fieldName]) &&
                    !flags.recursiveFlatFields)
                {
                    // NOTE: we intentionally addProperties() rather than clone() here, because
                    // we do want flags.nsPrefixes to have any additional nsPrefixes added to
                    // it
                    flags = isc.addProperties({}, flags);
                    flags.flatData = false;
                }
                
                output.append(this.xmlSerializeField(fieldName, childData, flags, indent));
            }
            // in flatData mode, we don't delete fields that we've output, because we don't
            // write out undeclared data at the end
            if (!flatData && data[fieldName] != null) delete data[fieldName];
        }

        // if there's any data left, tack them on the end, but *not* if this DataSource came
        // form XML Schema, in which case the extra data is sure to be invalid
        if (!flatData && !isc.isA.Schema(this)) {
            for (var fieldName in data) {
            	if (!isc.Comm._explicitNils &&
            			(isc.isAn.emptyString(data[fieldName]) || isc.isAn.emptyObject(data[fieldName])))
            		continue;
            	
            	if (! data[fieldName]) continue;
            	// Will be overridden in isc.Comm._xmlSerializeObject for AdvancedCriteria and RelativeDate
            	if (fieldName == "data" && data.operationType == "fetch") {
            		data[fieldName]["xsi:type"] = "map";
            	} else if (fieldName == "data"  && data.operationType == "update" && !data[fieldName]["xsi:type"]) {
            		data[fieldName]["xsi:type"] = "map"; // partial update from grid
            	} else if (fieldName == "data" && this.beanClassName) {
            		data[fieldName]["xsi:type"] = this.beanClassName.toLowerCaseFirst();
            	}
            	
                output.append(this.xmlSerializeField(fieldName, data[fieldName], flags, indent));
            }
        }

        return output.release(false);
    },
    
    xmlSerializeField : function (fieldName, value, flags, indent) {
        var output = isc.SB.create(),
            field = this.getField(fieldName);
        // skip undeclared, generated properties
        if (field == null && (fieldName.startsWith("_") || fieldName.startsWith("$"))) return;

        var fieldType = (field ? field.type : null),
            flatData = flags && flags.flatData,
            spoofData = flags && flags.spoofData;

        if (spoofData) value = this.getSpoofedData(field);

        // namespace the element if either everything must be qualifed or this particular field
        // is declared and must be qualified
        var namespace = ((field && field.mustQualify) || flags.qualifyAll ?
                         this.getSchemaSet().schemaNamespace : null);

        // if this field matches the textContentProperty, and either a textContent field has
        // been declared or there are no elements, return the value of the textContentProperty
        // with no surrounding tag and no indent.
        // NOTE: this handling appears here so that it works with flatData and spoofData
        var textContentProperty = flags.textContentProperty || this.textContentProperty,
            textContentField = this.getTextContentField();

        if (fieldName == textContentProperty &&
            (textContentField != null || !this.hasXMLElementFields(textContentProperty)))
        {
            // one time flag prevents normal whitespace/indent from being added
            this._suppressIndent = true;
            return this._serializeSimpleTypeValue(textContentField, value);
        }

        // special case for "Action"s - get the iscAction we stored on the function object
        if (fieldType == this._$Action && value != null) {
            if (value.iscAction) {
                value = value.iscAction
            // sanity check for StringMethods (may not be necessary since addMethods() /
            // addProperties() should have converted StringMethods to functions with iscAction
            // stored on them
            } else if (isc.isA.StringMethod(value)) {
                value = value.value;
            }
        }

	    var xsiType = this.shouldWriteSchemaType(field) ?
	                    this._getXMLSchemaType(field ? field.type : null, value) : null;

        var fieldStart = isc.Comm._xmlOpenTag(fieldName, xsiType,
                                              namespace, flags.nsPrefixes),
            fieldEnd = isc.Comm._xmlCloseTag(fieldName, namespace, flags.nsPrefixes);

        //this.logWarn("schemaSet is: " + this.echo(schemaSet) + ", fieldStart: " + fieldStart);

        var values = isc.isAn.Array(value) ? value : [value];

        // If this DataSource has no field of the given name, see if we were passed another
        // DataSource to use as schema by the calling code.  RestDataSource uses this pattern
        // to allow it to control the overall style of XML serialization (the tag names to use
        // for criteria, criterion, etc) and the detailed serialization of individual fields
        // (most critically for fields of type "date", "time" and "datetime")
        if (!field && flags.schema && flags.schema.getField) {
            field = flags.schema.getField(fieldName);
        }

        // for a DataSource type, ask the DataSource to serialize the value
        if (this.fieldIsComplexType(fieldName)) {
            var oldPSN = flags.parentSchemaNamespace;
            flags.parentSchemaNamespace = this.schemaNamespace;
            //if (field && field.xsElementRef) this.logWarn("looking up *element* only: " + fieldType);

            var ds = this.getFieldDataSource(field, field && field.xsElementRef ? "element" : null);
            //this.logWarn("complexType field: " + fieldName +
            //             " with schema: " + ds +
            //             " has value: " + this.echo(value));

            if (field.multiple) {
                // if this is a field declaration with multiple=true, we write out a
                // container tag containing one or more tags for the values, where the values
                // have the typeName, eg
                //      <children> // container tag: fieldName
                //          <Canvas> // value tag: field dataSource
                //          </Canvas>
                //          <Canvas>
                //          </Canvas>
                //      </children>
                output.append(indent, fieldStart);
                for (var i = 0; i < values.length; i++) {

                    var elementDS = ds;
                    if (values[i] != null && values[i]._constructor) {
                        elementDS = isc.DS.get(values[i]._constructor) || ds;
                    }
                    output.append(elementDS.xmlSerialize(values[i], flags, indent,
                                                  // allow the field to specify a name for
                                                  // child tags, eg, ds.fields -> "field"
                                                  values[i] == null
                                                   ? field.childTagName
                                                   : values[i]._tagName || field.childTagName));
                }
                output.append(indent, fieldEnd);

            } else if (ds.canBeArrayValued && isc.isAn.Array(value)) {
                // very special case for the valueMap and point types, which when it appears as a simple
                // JS Array, shouldn't be considered multiple valueMaps/points.

                output.append(ds.xmlSerialize(value, flags, indent, fieldName));
            } else if (values.length == 1 && isc.isA.String(values[0]) && isc.startsWith(values[0],"ref:")) {
                // special case of a field like DrawItem.fillGradient which can be a Gradient or
                // a reference but the output should not be <fillGradient><Gradient ref="xxx"></fillGradient>
                // but rather <fillGradient ref="xxx"/>.
                fieldStart = isc.Comm._xmlOpenTag(fieldName, xsiType, namespace, flags.nsPrefixes, true);
                output.append(indent);
                output.append(fieldStart);
                output.append(" ref=\"", value.substring(4), "\"/>");
            } else {
                // if this is a field that isn't declared multiple but has multiple values,
                // we write out one or more tags named after the field name since that's the
                // inbound format that would produce an array in a field not declared
                // multiple
                for (var i = 0; i < values.length; i++) {
                    var value = values[i];
                    if (value == null) { // null = empty tag
                        output.append(indent);
                        output.append(fieldStart, fieldEnd);
                    // handle being unexpectedly passed a simple type.  If we're serializing a
                    // SOAP message, the resulting XML isn't going to adhere to the schema, but
                    // there's nothing we can do.
                    } else if (isc.DS.isSimpleTypeValue(value)) {
                        if (isc.isA.String(value) && isc.startsWith(value,"ref:")) {
                            output.append(indent);
                            output.append(fieldStart);
                            var refTagName = (field ? field.childTagName || field.type : "value");
                            output.append("<", refTagName, " ref=\"", value.substring(4), "\"/>");
                            output.append(fieldEnd);
                        } else {
                            this.logWarn("simple type value " + this.echoLeaf(value) +
                                         " passed to complex field '" + field.name + "'",
                                         "xmlSerialize");
                            output.append(indent);
                            output.append(isc.Comm.xmlSerialize(fieldName, value));
                        }
                    } else {
                        output.append(ds.xmlSerialize(value, flags, indent, fieldName));
                    }
                }
            }
            flags.parentSchemaNamespace = oldPSN;

        // declared field of simple type
        } else if (field != null) {

            // if this field came from a simple type element, we have to use the namespace that
            // the element was defined in.  Note that both xs:simpleTypes and xs:elements of
            // simpleType become SimpleTypes in SmartClient - both essentially represent a
            // reusable atomic type.
            if (field.xsElementRef) {
                var simpleType = this.getType(field.type);
                //this.logWarn("in DataSource: " + this +
                //             ", simpleType field: " + this.echo(field) +
                //             ", got simpleType: " + this.echo(simpleType));
                if (simpleType && simpleType.schemaNamespace)
                {
                    namespace = simpleType.schemaNamespace;
                }
            }

            if (field.multiple) {
                output.append(indent, fieldStart);
                for (var i = 0; i < values.length; i++) {
                    // field.childTagName dictates the childTagName.  If it's null a generic
                    // name will be used
                    output.append(
                         this._serializeSimpleTypeTag(field.childTagName, field,
                                                      values[i], namespace, flags),
                         indent);
                }
                output.append(indent, fieldEnd);
            } else {
                for (var i = 0; i < values.length; i++) {
                    output.append(
                        indent,
                        this._serializeSimpleTypeTag(fieldName, field,
                                                       values[i], namespace, flags));
                }
            }

        // undeclared field, simple or complexType
        } else {
            //this.logWarn("serializing values for undeclared field: " + fieldName +
            //             ", values: " + this.echoAll(values));
            for (var i = 0; i < values.length; i++) {
                if (values[i] == null || isc.isAn.emptyObject(values[i])) {
                    output.append(indent, fieldStart, fieldEnd);
                } else {
                    output.append(indent,
                                 isc.Comm._xmlSerialize(fieldName, values[i], indent,
                                                        { isRoot:false }));
                }
            }
        }

        return output.release(false);
    }

});
