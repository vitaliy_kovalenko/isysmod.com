package com.isysmod.orm.bitrix;
// Generated Dec 22, 2016 7:18:32 PM by Hibernate Tools 5.0.0.Final



/**
 * BIblockSectionElementId generated by hbm2java
 */
public class BIblockSectionElementId  implements java.io.Serializable {


     private String iblockSectionId;
     private String iblockElementId;

    public BIblockSectionElementId() {
    }

    public BIblockSectionElementId(String iblockSectionId, String iblockElementId) {
       this.iblockSectionId = iblockSectionId;
       this.iblockElementId = iblockElementId;
    }
   
    public String getIblockSectionId() {
        return this.iblockSectionId;
    }
    
    public void setIblockSectionId(String iblockSectionId) {
        this.iblockSectionId = iblockSectionId;
    }
    public String getIblockElementId() {
        return this.iblockElementId;
    }
    
    public void setIblockElementId(String iblockElementId) {
        this.iblockElementId = iblockElementId;
    }




}


