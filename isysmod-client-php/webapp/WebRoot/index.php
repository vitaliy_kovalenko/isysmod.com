<?php

/**
 * Получить загадку (secret) можно двумя способами:
 * 1. Из заголовка WWW-Authenticate (поле nonce)
 * 2. Из cookie с именем secret
 * Здесь я использую Cookie, так как это простой способ
 * хранения загадки между запросами. Но для систмных
 * приложений проще хранить загадку в памяти и получать
 * ее из заголовка.
 */

define("APIKEY", "ff2e549a-9277-a1c4-7fbb-38728cdbef18");
define("SECRETKEY", "26b32237-6535-db7e-4b40-f370cab37e29");
define("SECRET_COOKIE", "secret");
define("SERVICE_HOST", "api.isysmod.com");
define("SERVICE_URI", "/isysmod-services/admin"); // because ajp://localhost:8009/isysmod-modeling/
define("SERVICE_URL", "http://api.isysmod.com/admin");
define("COOKIE_FILE", __DIR__ . "/../cookie.json");


require_once '../vendor/autoload.php';
require_once '../jaxb/JaxbFetchRequest.php';
require_once '../jaxb/JaxbRequestBatch.php';

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\FileCookieJar;

/**
 * TODO:
 * Эту логику надо обернуть в классы и возможно, подумать над тем,
 * чтобы выбирать ответ сервера в один объект, в нем же изменять данные
 * и его же сохранять.
 * Можно переделать джойны на такой формат:
 * <join select="false" type="inner">iblock.iblockType</join>
 * <join select="true" type="outer">iblockElementProperties</join>
 * <join select="true" type="outer">iblockSections.iblockChildSections</join>
 * <join select="true" type="outer">iblockSections.iblock.iblockType</join>
 * Старый формат дает больше контроля только в том, что позволяет не
 * распечатывать уже выбранные джойны. И возможно, старый удобней для
 * клиентского кода... Надо попробовать.
 */

function createAuthHeader($secret) {
	if (!$secret) {
		$secret = 'www';
	}
	
	$decoded = base64_decode($secret);
	$signature = hash_hmac("md5", $decoded, SECRETKEY);
	
	return "Digest username=\"".APIKEY."\", realm=\"".SERVICE_HOST."\", nonce=\"".$secret."\", uri=\"".SERVICE_URI."\", response=\"".$signature."\"";
}

function parseAuthHeader($authHeader) {
	$matches = array();
	$pattern = "/nonce=\"(?P<secret>.*)\"/";
	preg_match($pattern, $authHeader, $matches);

	return isset($matches["secret"]) ? $matches["secret"] : false;
}

function sendRequest($secret, $data, $is_json = false) {
	global $client;
	
	$body = $is_json ? json_encode($data) : $data;
	//$body = '{"request":[{"dataSource":"bitrix","type":"FetchRequest","entity":{"type":"BIblockType"}}]}';
	//echo $body;
	$response = $client->request("POST", SERVICE_URL, [
			'body' => $body,
			'headers' => [
					'Accept' => $is_json ? 'application/json' : 'application/xml',
					'Content-Type' => $is_json ? 'application/json' : 'application/xml',
					'Authorization' => createAuthHeader($secret)
			],
			'http_errors' => false
	]);
	
	return $response;
}

//$requestBatch = new JaxbRequestBatch();
//$requestBatch->addRequest(new JaxbFetchRequest("bitrix", "BIblockType"));
// $requestBatch->addRequest(new JaxbFetchRequest("bitrix", "BIblock"));
$xml =<<<XML
<?xml version="1.0" encoding="UTF-8"?>
<transaction>
	<operations>
		<request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
			<database>bitrix</database>
			<beanClassName>BIblockElement</beanClassName>
			<operationType>fetch</operationType>
			<startRow>0</startRow>
			<endRow>3</endRow>
			<joins>
				<join>
					<owner>BIblockElement</owner>
					<field>iblock</field>
					<select>true</select>
					<joins>
						<join>
							<owner>BIblock</owner>
							<field>iblockType</field>
							<select>true</select>
						</join>
					</joins>
				</join>
				<join>
					<owner>BIblockElement</owner>
					<field>iblockElementProperties</field>
					<select>false</select>
				</join>
				<join>
					<owner>BIblockElement</owner>
					<field>iblockSections</field>
					<select>true</select>
					<joins>
						<join>
							<owner>BIblockSection</owner>
							<field>iblockChildSections</field>
							<select>true</select>
						</join>
						<join>
							<owner>BIblockSection</owner>
							<field>iblock</field>
							<select>true</select>
							<joins>
								<join>
									<owner>BIblock</owner>
									<field>iblockType</field>
									<select>true</select>
								</join>
							</joins>
						</join>
					</joins>
				</join>
			</joins>
		</request>
	</operations>
</transaction>
XML;

$attempt = 0;
$response = null;
$secret = null;
$jar = new FileCookieJar(COOKIE_FILE, true);
$client = new Client(['cookies' => $jar]);

while ($attempt < 2 && (!$response || $response->getStatusCode() == 401)) {
	foreach ($jar->toArray() as $cookie) {
		if ($cookie["Name"] == SECRET_COOKIE) {
			$secret = $cookie["Value"];
		}
	}
	
	//$response = sendRequest($secret, $requestBatch, true);
	$response = sendRequest($secret, $xml);
	if (!$response) break;
	
	if (!$secret && $response->hasHeader('WWW-Authenticate')) {
		$authHeader = $response->getHeader('WWW-Authenticate');
		$secret = parseAuthHeader($authHeader[0]);
	}
	
	$attempt += 1;
}


if ($response && $response->getStatusCode() == 200) {
	//print_r($response->getHeaders());
	$json = (string) $response->getBody();
	echo $json;
	//print_r(json_decode($json));
}

echo PHP_EOL, "Authorization attempts: " . $attempt;
