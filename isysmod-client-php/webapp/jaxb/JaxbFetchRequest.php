<?php

class JaxbFetchRequest implements JsonSerializable {

	/**
	 *
	 * @var string
	 */
	private $xsiType = "FetchRequest";
	
	/**
	 * 
	 * @var string
	 */
	private $dataSource;
	
	/**
	 * 
	 * @var string
	 */
	private $entityXsiType;
	
	public function __construct($dataSource, $entityName) {
		$this->dataSource = $dataSource;
		$this->entityXsiType = $entityName;
	}

	/**
	 * 
	 * @param string $entityXsiType
	 */
	public function fetch($entityXsiType) {
		$this->entityXsiType = $entityXsiType;
	}
	
	/**
	 * Implementation of JsonSerializable interface
	 *
	 * @return array
	 */
	public function jsonSerialize() {
		$json = array();
		$json["dataSource"] = $this->dataSource;
		$json["type"] = $this->xsiType;
		$json["entity"] = array(
				"type" => $this->entityXsiType
		);
		
		return $json;
	}

}
