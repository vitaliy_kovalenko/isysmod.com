<?php

class JaxbRequestBatch implements JsonSerializable {
	
	private $requests;
	
	public function __construct() {
		$this->requests = array();
	}
	
	/**
	 * Implementation of JsonSerializable interface
	 *
	 * @return array
	 */
	public function jsonSerialize() {
		$json = array(
				"requests" => array(
					"request" => $this->requests
				)
		);
		
		return $json;
	}
	
	public function addRequest($request) {
		array_push($this->requests, $request);
	}
}
