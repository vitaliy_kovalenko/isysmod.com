package com.isysmod.common.i18n;

import java.util.Locale;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.springframework.context.i18n.LocaleContextHolder;

public class ContextualMessageInterpolator extends ResourceBundleMessageInterpolator {

	@Override
	public String interpolate(String template, Context context) {
		return super.interpolate(template, context, LocaleContextHolder.getLocale());
	}

	@Override
	public String interpolate(String template, Context context, Locale locale) {
		return super.interpolate(template, context, LocaleContextHolder.getLocale());
	}

}
