package com.isysmod.common.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class JmsServer implements InitializingBean, DisposableBean {

	private ConnectionFactory connectionFactory;
	private String queueName;
	private MessageListener messageListener;
	private boolean transacted = false;
	
	private Connection connection;
	private Session session;
	private MessageProducer replyProducer;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.connection = connectionFactory.createConnection();
		connection.start();
		this.session = connection.createSession(this.transacted, Session.AUTO_ACKNOWLEDGE);
		Destination adminQueue = this.session.createQueue(queueName);

		//Setup a message producer to respond to messages from clients, we will get the destination
        //to send to from the JMSReplyTo header field from a Message
		this.replyProducer = this.session.createProducer(null);
		this.replyProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
 
		//Set up a consumer to consume messages off of the admin queue
        MessageConsumer consumer = this.session.createConsumer(adminQueue);
        consumer.setMessageListener(messageListener);
	}
	
	public void replyTo(Message request, Message response) throws JMSException {
		response.setJMSCorrelationID(request.getJMSCorrelationID());
		this.replyProducer.send(request.getJMSReplyTo(), response);
	}

	public void setMessageListener(MessageListener messageListener) {
		this.messageListener = messageListener;
	}

	public void setConnectionFactory(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public void setTransacted(boolean transacted) {
		this.transacted = transacted;
	}
	
	public Session getSession() {
		return session;
	}

	@Override
	public void destroy() throws Exception {
		this.session.close();
		this.connection.close();
	}

}
