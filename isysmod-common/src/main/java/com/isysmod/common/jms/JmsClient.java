package com.isysmod.common.jms;

import java.util.UUID;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class JmsClient implements InitializingBean, DisposableBean {
	private ConnectionFactory connectionFactory;
	private String queueName;
	private boolean transacted = false;
	private long serverTimeout = 5000;
	
	private Connection connection;
	private Session session;
	private MessageProducer producer;
	private MessageConsumer responseConsumer;
	private Destination tempDest;

	@Override
	public void afterPropertiesSet() throws Exception {
		this.connection = connectionFactory.createConnection();
		connection.start();
		session = connection.createSession(transacted, Session.AUTO_ACKNOWLEDGE);
		Destination adminQueue = session.createQueue(queueName);
		
		// Setup a message producer to send message to the queue the server
		// is consuming from
		this.producer = session.createProducer(adminQueue);
		this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		
		// Create a temporary queue that this client will listen for
		// responses on then create a consumer
		// that consumes message from this temporary queue...for a real
		// application a client should reuse
		// the same temp queue for each message to the server...one temp
		// queue per client
		tempDest = session.createTemporaryQueue();
		responseConsumer = session.createConsumer(tempDest);

		// This class will handle the messages to the temp queue as well
		// responseConsumer.setMessageListener(this);
	}
	
	public Message send(Message message) throws JMSException {
		// Set the reply to field to the temp queue you created above, this
		// is the queue the server
		// will respond to
		message.setJMSReplyTo(this.tempDest);

		// Set a correlation ID so when you get a response you know which
		// sent message the response is for
		// If there is never more than one outstanding message to the server
		// then the
		// same correlation ID can be used for all the messages...if there
		// is more than one outstanding
		// message to the server you would presumably want to associate the
		// correlation ID with this
		// message somehow...a Map works good
		message.setJMSCorrelationID(UUID.randomUUID().toString());
		this.producer.send(message);

		return this.responseConsumer.receive(this.serverTimeout);
	}

	public Session getSession() {
		return session;
	}

	public void setConnectionFactory(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public void setServerTimeout(long serverTimeout) {
		this.serverTimeout = serverTimeout;
	}

	@Override
	public void destroy() throws Exception {
		this.session.close();
		this.connection.close();
	}

}
