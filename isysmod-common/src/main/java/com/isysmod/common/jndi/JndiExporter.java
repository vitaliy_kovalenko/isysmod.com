package com.isysmod.common.jndi;

import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.hibernate.engine.jndi.JndiException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class JndiExporter implements InitializingBean, DisposableBean {
	
	private final Logger LOGGER = Logger.getLogger(JndiExporter.class);

    private Map<String, Object> jndiMapping;

    public JndiExporter() {
//    	System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
//                "org.apache.naming.java.javaURLContextFactory");
//        System.setProperty(Context.URL_PKG_PREFIXES,
//                "org.apache.naming");
	}

    @Override
    public void afterPropertiesSet() {
    	InitialContext initialContext = buildInitialContext();
    	
    	try {
	        for (Map.Entry<String, Object> jndi : jndiMapping.entrySet()) {
	        	String jndiName = jndi.getKey();
	        	Object object = jndi.getValue();
	        	bindObject(initialContext, jndiName, object);
	        	LOGGER.info(object.toString() + " bound to " + jndiName);
	        }
    	} catch (NamingException e) {
    		LOGGER.error(e);
		} finally {
			cleanUp(initialContext);
		}
    }
    
    private void bindObject(Context context, String jndiName, Object object) throws NamingException {
    	String path = getRootPath(jndiName);
        Context subContext = createSubContextIfNeed(context, path);
        String[] dirs = jndiName.replaceFirst(path + "/", "").split("/");
        
        if (dirs.length > 1) {
        	for (int i = 0; i < dirs.length - 1; i++) {
        		path = dirs[i];
        		subContext = createSubContextIfNeed(subContext, path);
        	}
        }
        
        context.bind(jndiName, object);
    }
    
    private String getRootPath(String jndiName) {
    	String[] parts = jndiName.split(":");
    	String path;
    	
    	if (parts.length > 1) {
    		path = parts[0] + ':';
    	} else {
    		path = "java:";
    	}
    	
    	return path;
    }

	private Context createSubContextIfNeed(Context context, String path) throws NamingException {
		Context subContext = context;
    	Object o = null;
		
		try {
			o = subContext.lookup(path);
		} catch (NamingException e) {
    		// ignored
		}
		
		if (o == null) {
			subContext = subContext.createSubcontext(path);
		}
		
		return subContext;
	}

	@Override
    public void destroy() {
		InitialContext initialContext = buildInitialContext();
		
		try{
	        for (Map.Entry<String, Object> jndi : jndiMapping.entrySet()) {
	            String jndiName = jndi.getKey();
	            Object o = jndi.getValue();
	            
	            if (o != null && jndiName != null && o == initialContext.lookup(jndiName)) {
	            	initialContext.unbind(jndiName);
	            }
	        }
		} catch (NamingException e) {
			LOGGER.error(e);
		} finally {
			cleanUp(initialContext);
		}
    }

	public Map<String, Object> getJndiMapping() {
		return jndiMapping;
	}

	public void setJndiMapping(Map<String, Object> jndiMapping) {
		this.jndiMapping = jndiMapping;
	}
	
	private InitialContext buildInitialContext() {
		try {
			return new InitialContext();
		}
		catch ( NamingException e ) {
			throw new JndiException( "Unable to open InitialContext", e );
		}
	}
	
	private void cleanUp(Context context) {
		try {
			context.close();
		}
		catch ( NamingException e ) {
			LOGGER.error(e);
		}
	}
}
