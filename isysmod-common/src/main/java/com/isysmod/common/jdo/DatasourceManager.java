package com.isysmod.common.jdo;

import javax.inject.Named;

import org.springframework.beans.factory.DisposableBean;

@Named
public interface DatasourceManager extends DisposableBean {

	public boolean init();
	
	public void rebuildFactory(String factoryName);
	
	public Object openDefaultSession();
	
	public Object openSession(String factoryName);
	
//	public String getJndiName();
//	
//	public void setJndiName(String jndiName);
	
}
