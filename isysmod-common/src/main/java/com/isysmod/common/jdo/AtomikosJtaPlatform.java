//package com.isysmod.common.jdo;
//
//import javax.transaction.SystemException;
//import javax.transaction.TransactionManager;
//import javax.transaction.UserTransaction;
//
//import org.hibernate.engine.transaction.jta.platform.internal.AbstractJtaPlatform;
//
//import com.atomikos.icatch.jta.UserTransactionImp;
//import com.atomikos.icatch.jta.UserTransactionManager;
//
//public class AtomikosJtaPlatform extends AbstractJtaPlatform {
//
//	private static final long serialVersionUID = -6767821166889815642L;
//
//	private TransactionManager transactionManager;
//	
//	private UserTransaction userTransaction;
//
//	@Override
//	protected TransactionManager locateTransactionManager() {
//		if (this.transactionManager == null) {
//			this.transactionManager = new UserTransactionManager();
//		}
//		
//		return this.transactionManager;
//	}
//
//	@Override
//	protected UserTransaction locateUserTransaction() {
//		if (this.userTransaction == null) {
//			this.userTransaction = new UserTransactionImp();
//			
//			try {
//				this.userTransaction.setTransactionTimeout(300);
//			} catch (SystemException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return this.userTransaction;
//	}
//
//	public TransactionManager getTransactionManager() {
//		return transactionManager;
//	}
//
//	public void setTransactionManager(TransactionManager transactionManager) {
//		this.transactionManager = transactionManager;
//	}
//
//	public UserTransaction getUserTransaction() {
//		return userTransaction;
//	}
//
//	public void setUserTransaction(UserTransaction userTransaction) {
//		this.userTransaction = userTransaction;
//	}
//
//}
