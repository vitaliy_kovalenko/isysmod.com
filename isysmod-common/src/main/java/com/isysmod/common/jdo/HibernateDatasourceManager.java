package com.isysmod.common.jdo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

public class HibernateDatasourceManager {

	private static final Logger LOGGER = Logger.getLogger(HibernateDatasourceManager.class);
	
	private static final String DEFAULT_SESSION_FACTORY = "default";
	
	private static final String SUFFIX = ".cfg.xml";
	
	static private Map<String, SessionFactory> datasources;
	
	static private String locationPattern = "classpath:orm/*" + SUFFIX;
	
	static SessionFactory getSessionFactory() {
		return getSessionFactory(null);
	}
	
	static SessionFactory getSessionFactory(String factoryName) {
		if (factoryName == null) {
			factoryName = DEFAULT_SESSION_FACTORY;
		}
		
		if (datasources.containsKey(factoryName)) {
			datasources.get(factoryName);
		}
		
		throw new RuntimeException("Session factory configuration '" + factoryName + "' does not exists.");
	}
	
	static SessionFactory rebuildSessionFactory(String factoryName) throws IOException {
		if (! datasources.containsKey(factoryName)) {
			throw new RuntimeException("No such SessionFactory for configuration '" + factoryName +"'");
		}
		
		SessionFactory factory = datasources.get(factoryName);
		
		if (factory != null && ! factory.isClosed()) {
			factory.close();
		}
		
		Resource resource = getFactoryConfiguration(factoryName);
		
		if (resource == null) {
			throw new RuntimeException("No such configuration for name '" + factoryName +"'");
		}
		
	    factory = buildSessionFactory(resource);
		datasources.put(factoryName, factory);
		
		return getSessionFactory(factoryName);
	}
	
	static private Resource[] findFactoryConfigurations() {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

		try {
			return resolver.getResources(locationPattern);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		return null;
	}
	
	static private Resource getFactoryConfiguration(String factoryName) {
		Resource[] resources = findFactoryConfigurations();
		
		for (Resource resource : resources) {
			if (resource.getFilename().equals(factoryName + SUFFIX)) {
				return resource;
			}
		}
		
		return null;
	}
	
	static private SessionFactory buildSessionFactory(Resource resource) {
		SessionFactory factory = null;
		Configuration configuration = new Configuration();
		
	    try {
			configuration.configure(resource.getFile());
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
			factory = configuration.buildSessionFactory(builder.build());
		} catch (HibernateException | IOException e) {
			LOGGER.error(e);
		}
	    
	    return factory;
	}

	public static void initialize() {
		initialize(null);
	}
	
	public static void initialize(String locationPattern) {
		if (locationPattern != null) {
			HibernateDatasourceManager.locationPattern = locationPattern;
		}
		
		datasources = new HashMap<String, SessionFactory>();
		Resource[] resources = findFactoryConfigurations();

		if (resources != null) {
			for (Resource resource : resources) {
				SessionFactory factory = buildSessionFactory(resource);
				
				if (factory != null) {
					String factoryName = resource.getFilename().replaceAll(SUFFIX, "");
					datasources.put(factoryName, factory);
					LOGGER.info("SessionFactory initialized for configuration '" + factoryName +"'");
				} else {
					LOGGER.error("Failed to build SessionFactory from configuration '" + resource.getFilename() +"'");
				}
			}
		}
	}

	public static void destroy() {
		for (Entry<String, SessionFactory> entry : datasources.entrySet()) {
			SessionFactory factory = entry.getValue();
			
			if (factory != null && ! factory.isClosed()) {
				factory.close();
				
				LOGGER.info("SessionFactory destroyed for configuration '" + entry.getKey() +"'");
			}
		}
		
		datasources.clear();
	}

}
