package com.isysmod.common.jdo;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@Named
public class JpaDatasourceManager implements DatasourceManager {
	
//	@Inject
//	private JpaVendorAdapter jpaVendorAdapter;
	
	private final Logger LOGGER = Logger.getLogger(JpaDatasourceManager.class);
	
	private String defaultUnitName = "default";
	
	private Map<String, LocalContainerEntityManagerFactoryBean> datasources;
	
//	private String jndiName;
	
	@Override
	public void rebuildFactory(String unitName) {
		if (! datasources.containsKey(unitName)) {
			throw new RuntimeException("There no initialized factory for name '" + unitName +"'");
		}
		
		LocalContainerEntityManagerFactoryBean factoryBean = datasources.get(unitName);
		factoryBean.destroy();
		factoryBean = createFactoryBean(unitName);
		datasources.put(unitName, factoryBean);
	}
	
	private List<String> parseXmlUnits() throws SAXException, IOException, ParserConfigurationException {
		List<String> units = new ArrayList<String>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream is = JpaDatasourceManager.class.getClassLoader().getResourceAsStream("META-INF/persistence.xml");
		Document document = builder.parse(is);
		Element root = document.getDocumentElement();
		root.normalize();
		NodeList unitsNodes = root.getElementsByTagName("persistence-unit");
		
		for (int i = 0; i < unitsNodes.getLength(); i++) {
			Node unitNode = unitsNodes.item(i);
			
			if (unitNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) unitNode;
				String unit = element.getAttribute("name");
				
				if (unit != null) {
					units.add(unit);
				}
			}
		}
		
		return units;
	}
	
	private LocalContainerEntityManagerFactoryBean createFactoryBean(String unitName) {
		LocalContainerEntityManagerFactoryBean  factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setBeanName(unitName + "EntityManagerFactory");
		factoryBean.setPersistenceUnitName(unitName);
//		factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
		factoryBean.afterPropertiesSet();
		
		return factoryBean;
	}

	@Override
	public boolean init() {
		boolean success = false;
		
		try {
			datasources = new HashMap<String, LocalContainerEntityManagerFactoryBean>();
			List<String> units = parseXmlUnits();
			
			for (String unitName : units) {
				LocalContainerEntityManagerFactoryBean  factoryBean = createFactoryBean(unitName);
				datasources.put(unitName, factoryBean);
				LOGGER.info("JPA factory container initialized for persistence unit '" + unitName +"'");
			}
			
			success = true;
			// bind via JNDI
//			if (StringUtils.hasLength(jndiName)) {
//				Context context = buildInitialContext();
//				
//				try {
//					context.bind(jndiName, this);
//					LOGGER.info("Bound datasource manager to JNDI name: " + jndiName);
//				} catch (NamingException e) {
//					LOGGER.error(e);
//				} finally {
//					cleanUp(context);
//				}
//			}
		} catch (IOException | SAXException | ParserConfigurationException e) {
			LOGGER.error(e);
		}
		
		return success;
	}

	@Override
	public void destroy() {
		if (datasources != null) {
			for (Entry<String, LocalContainerEntityManagerFactoryBean> entry : datasources.entrySet()) {
				LocalContainerEntityManagerFactoryBean factoryBean = entry.getValue();
				factoryBean.destroy();
				LOGGER.info("JPA factory container destroyed for persistence unit '" + entry.getKey() +"'");
			}
			
			datasources.clear();
		}
		
//		if (StringUtils.hasLength(jndiName)) {
//			Context context = buildInitialContext();
//
//			try {
//				context.unbind(jndiName);
//				LOGGER.info("Unbound datasource manager from JNDI name: " + jndiName);
//			} catch (NamingException e) {
//				LOGGER.error(e);
//			} finally {
//				cleanUp(context);
//			}
//		}
	}
	
	@Override
	public EntityManager openDefaultSession() {
		return openSession(defaultUnitName);
	}
	
	@Override
	public EntityManager openSession(String unitName) {
		if (unitName == null) {
			unitName = defaultUnitName;
		}
		
		if (! datasources.containsKey(unitName)) {
			return null;
		}
		
		EntityManagerFactory factory = datasources.get(unitName).getNativeEntityManagerFactory();
		
		return factory.createEntityManager();
	}

//	@Override
//	public String getJndiName() {
//		return jndiName;
//	}
//
//	@Override
//	public void setJndiName(String jndiName) {
//		this.jndiName = jndiName;
//	}
//	
//	private InitialContext buildInitialContext() {
//		try {
//			return new InitialContext();
//		}
//		catch ( NamingException e ) {
//			throw new JndiException( "Unable to open InitialContext", e );
//		}
//	}
//	
//	private void cleanUp(Context context) {
//		try {
//			context.close();
//		}
//		catch ( NamingException e ) {
//			LOGGER.error(e);
//		}
//	}

	public String getDefaultUnitName() {
		return defaultUnitName;
	}

	public void setDefaultUnitName(String defaultUnit) {
		this.defaultUnitName = defaultUnit;
	}

}
