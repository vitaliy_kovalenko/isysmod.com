package com.isysmod.common.security;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Named;
import javax.servlet.http.Cookie;

import org.springframework.security.crypto.codec.Hex;

@Named
public class HMacEncoder {

	final public static Charset CHARSET = StandardCharsets.US_ASCII;
	
	private String algorithm = "HmacMD5";
	
	private String authCookieName = "secret";
	
	private Integer authCookieMaxAge = 3600;
	
	String encode(String secretKey, String secret)
			throws NoSuchAlgorithmException, InvalidKeyException {
		SecretKeySpec secretKeySpec = new SecretKeySpec(CHARSET.encode(secretKey).array(), algorithm);
		Mac mac = Mac.getInstance(algorithm);
    	mac.init(secretKeySpec);
    	byte[] hmacData = mac.doFinal(CHARSET.encode(secret).array());
		
		return new String(Hex.encode(hmacData));
	}
	
	public boolean matches(String secretKey, String secret, String signature)
			throws InvalidKeyException, NoSuchAlgorithmException {
		if (
				secretKey == null || secretKey.isEmpty() ||
				secret == null || secret.isEmpty() ||
				signature == null || signature.isEmpty()) {
			return false;
		}
		
		return encode(secretKey, secret).equals(signature);
	}

	public String createAuthHeader(String serverName, String secret) {
		String nonce = Base64.getEncoder().encodeToString(secret.getBytes(CHARSET));
		
		return "Digest "
				+ "realm=\"" + serverName + "\", "
				+ "nonce=\"" + nonce + "\"" ;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		if (algorithm != null && !algorithm.isEmpty()) {
			this.algorithm = "Hmac" + algorithm.toUpperCase();
		}
	}

	public void setAuthCookieName(String authCookieName) {
		this.authCookieName = authCookieName;
	}
	
	public void setAuthCookieMaxAge(Integer authCookieMaxAge) {
		this.authCookieMaxAge = authCookieMaxAge;
	}
	
	public Cookie createAuthCookie(String secret) {
		String value = Base64.getEncoder().encodeToString(secret.getBytes(CHARSET));
		Cookie cookie = new Cookie(authCookieName, value);
		cookie.setMaxAge(authCookieMaxAge);
		
		return cookie;
	}

}
