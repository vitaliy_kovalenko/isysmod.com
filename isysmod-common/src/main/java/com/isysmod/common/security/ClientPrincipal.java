package com.isysmod.common.security;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.servlet.http.Cookie;

import com.isysmod.common.jdo.DatasourceManager;

public class ClientPrincipal implements Principal {

	private Serializable primaryKey;
	
	private DatasourceManager datasourceManager;
	
	private List<String> datasources;
	
	private Map<String, EntityManager> sessionMap;
	
	private String authHeader;
	
	private Cookie authCookie;
	
	public ClientPrincipal(Serializable primaryKey, DatasourceManager datasourceManager, String authHeader) {
		this.primaryKey = primaryKey;
		this.datasourceManager = datasourceManager;
		datasources = new ArrayList<>();
		sessionMap = new HashMap<>();
		this.authHeader = authHeader;
	}
	
	@Override
	public String getName() {
		return "ClientPrincipal";
	}

	public String getAuthHeader() {
		return authHeader;
	}

	public void setDatasourceManager(DatasourceManager datasourceManager) {
		this.datasourceManager = datasourceManager;
	}
	
	public List<String> getDatasources() {
		return datasources;
	}

	public void setDatasources(List<String> datasources) {
		this.datasources = datasources;
	}

	public void setSessionMap(Map<String, EntityManager> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public Collection<EntityManager> getOpenedSessions() {
		return sessionMap.values();
	}

	public EntityManager currentSession(String factoryName) {
		if (!sessionMap.containsKey(factoryName)) {
			sessionMap.put(factoryName, (EntityManager) datasourceManager.openSession(factoryName));
		}
		
		return sessionMap.get(factoryName);
	}

	public Cookie getAuthCookie() {
		return authCookie;
	}

	public void setAuthCookie(Cookie authCookie) {
		this.authCookie = authCookie;
	}

	public Serializable getPrimaryKey() {
		return primaryKey;
	}

}
