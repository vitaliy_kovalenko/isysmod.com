package com.isysmod.common;

public interface IC {
	
	final String ISC_DATA_FORMAT_PARAMETER = "isc_dataFormat";
	final String ISC_COMMON_MODEL_PACKAGE = "com.isysmod.common.model";
	final String ISC_COMMON_JAXB_PACKAGE = "com.isysmod.common.jaxb.isc";
	final String ISYS_ORM_BASE_PACKAGE = "com.isysmod.orm";
	final String ISYS_MAIN_FACTORY_NAME = "modeling";
	
	final String ISC_ID_PROPERTY_NAME = "id";
	final String ISC_PASSWORD_PROPERTY_NAME = "password";
		
	final String METHOD_FETCH = "fetch";
	final String METHOD_ADD = "add";
	final String METHOD_UPDATE = "update";
	final String METHOD_REMOVE = "remove";
	
	/*** AdvancedCriteria ***/
	/**
	 * exactly equal to
	 */
	final String TEXT_MATCH_EQUAL = "equals";
	/**
	 * not equal to
	 */
	final String TEXT_MATCH_NOTEQUAL = "notEqual";
	/**
	 * exactly equal to, if case is disregarded
	 */
	final String TEXT_MATCH_IEQUAL = "iEquals";
	/**
	 * not equal to, if case is disregarded
	 */
	final String TEXT_MATCH_INOTEQUAL = "iNotEqual";
	/**
	 * Greater than
	 */
	final String TEXT_MATCH_GREATERTHAN = "greaterThan";
	/**
	 * Less than
	 */
	final String TEXT_MATCH_LESSTHAN = "lessThan";
	/**
	 * Greater than or equal to
	 */
	final String TEXT_MATCH_GREATEROREQUAL = "greaterOrEqual";
	/**
	 * Less than or equal to
	 */
	final String TEXT_MATCH_LESSOREQUAL = "lessOrEqual";
	/**
	 * Contains as sub-string (match case)
	 */
	final String TEXT_MATCH_CONTAINS = "contains"; // !
	/**
	 * Starts with (match case)
	 */
	final String TEXT_MATCH_STARTWITH = "startsWith";
	/**
	 * Ends with (match case)
	 */
	final String TEXT_MATCH_ENDSWITH = "endsWith";
	/**
	 * Contains as sub-string (case insensitive)
	 */
	final String TEXT_MATCH_ICONTAINS = "iContains";
	/**
	 * Starts with (case insensitive)
	 */
	final String TEXT_MATCH_ISTARTWITH = "iStartsWith";
	/**
	 * Ends with (case insensitive)
	 */
	final String TEXT_MATCH_IENDSWITH = "iEndsWith";
	/**
	 * Does not contain as sub-string (match case)
	 */
	final String TEXT_MATCH_NOTCONTAINS = "notContains"; // !
	/**
	 * Does not start with (match case)
	 */
	final String TEXT_MATCH_NOTSTARTWITH = "notStartsWith"; // !
	/**
	 * Does not end with (match case)
	 */
	final String TEXT_MATCH_NOTENDSWITH = "notEndsWith"; // !
	/**
	 * Does not contain as sub-string (case insensitive)
	 */
	final String TEXT_MATCH_INOTCONTAINS = "iNotContains";
	/**
	 * Does not start with (case insensitive)
	 */
	final String TEXT_MATCH_INOTSTARTWITH = "iNotStartsWith";
	/**
	 * Does not end with (case insensitive)
	 */
	final String TEXT_MATCH_INOTENDSWITH = "iNotEndsWith";
	/**
	 * shortcut for "greaterOrEqual" + "and" + "lessOrEqual" (case insensitive)
	 */
	final String TEXT_MATCH_IBETWEENINCLUSIVE = "iBetweenInclusive"; // !
	
	/*** wildcards ***/
	/**
	 * Basic GLOB matching using wildcards
	 */
	final String TEXT_MATCH_MATCHES_WILDCARD = "matchesPattern"; // +
	/**
	 * Basic GLOB matching using wildcards (case insensitive)
	 */
	final String TEXT_MATCH_IMATCHES_WILDCARD = "iMatchesPattern"; // +
	/**
	 * GLOB matching using wildcards. Value is considered to meet the criterion if it contains the pattern.
	 */
	final String TEXT_MATCH_CONTAINS_WILDCARD = "containsPattern";
	/**
	 * GLOB mathcing using wildcards. Value is considered to meet the criterion if it starts with the pattern.
	 */
	final String TEXT_MATCH_STARTSWITH_WILDCARD = "startsWithPattern";
	/**
	 * GLOB mathcing using wildcards. Value is considered to meet the criterion if it starts with the pattern
	 */
	final String TEXT_MATCH_ENDSWITH_WILDCARD = "endsWithPattern";
	/**
	 * GLOB matching using wildcards. Value is considered to meet the criterion if it contains the pattern. Matching is case insensitive.
	 */
	final String TEXT_MATCH_ICONTAINS_WILDCARD = "iContainsPattern";
	/**
	 * GLOB matching using wildcards. Value is considered to meet the criterion if it starts with the pattern. Matching is case insensitive.
	 */
	final String TEXT_MATCH_ISTARTSWITH_WILDCARD = "iStartsWithPattern";
	/**
	 * GLOB matching using wildcards. Value is considered to meet the criterion if it ends with the pattern. Matching is case insensitive.
	 */
	final String TEXT_MATCH_IENDSWITH_WILDCARD = "iEndsWithPattern";
	
	
	/**
	 * Regular expression match
	 */
	final String TEXT_MATCH_REGEXP = "regexp"; // !
	/**
	 * Regular expression match (case insensitive)
	 */
	final String TEXT_MATCH_IREGEXP = "iregexp"; // !
	/**
	 * value is null
	 */
	final String TEXT_MATCH_ISNULL = "isNull";
	/**
	 * value is non-null. Note empty string ("") is non-null
	 */
	final String TEXT_MATCH_NOTNULL = "notNull";
	/**
	 * alue is in a set of values. Specify criterion.value as an Array
	 */
	final String TEXT_MATCH_INSET = "inSet"; // !
	/**
	 * value is not in a set of values. Specify criterion.value as an Array
	 */
	final String TEXT_MATCH_NOTINSET = "notInSet"; // !
	
	/*** Fields ***/
	/**
	 * matches another field (match case, specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_EQUAL_FIELD = "equalsField";
	/**
	 * does not match another field (match case, specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_NOTEQUAL_FIELD = "notEqualField";
	/**
	 * matches another field (case insensitive, specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_IEQUAL_FIELD = "iEqualsField";
	/**
	 * does not match another field (case insensitive, specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_INOTEQUAL_FIELD = "iNotEqualField";
	/**
	 * Greater than another field (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_GREATERTHAN_FIELD = "greaterThanField";
	/**
	 * Less than another field (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_LESSTHAN_FIELD = "lessThanField";
	/**
	 * Greater than or equal to another field (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_GREATEROREQUAL_FIELD = "greaterOrEqualField";
	/**
	 * Less than or equal to another field (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_LESSOREQUAL_FIELD = "lessOrEqualField";
	/**
	 * Contains as sub-string (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_CONTAINS_FIELD = "containsField";
	/**
	 * Starts with (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_STARTSWITH_FIELD = "startsWithField";
	/**
	 * Ends with (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_ENDSWITH_FIELD = "endsWithField";
	/**
	 * Contains as sub-string (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_TCONTAINS_FIELD = "iContainsField";
	/**
	 * Starts with (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_ISTARTSWITH_FIELD = "iStartsWithField";
	/**
	 * Ends with (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_IENDSWITH_FIELD = "iEndsWithField";
	/**
	 * Does not contain as sub-string (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_NOTCONTAINS_FIELD = "notContainsField";
	/**
	 * Does not start with (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_NOTSTARTSWITH_FIELD = "notStartsWithField";
	/**
	 * Does not end with (match case) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_NOTENDSWITH_FIELD = "notEndsWithField";
	/**
	 * Does not contain as sub-string (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_INOTCONTAINS_FIELD = "iNotContainsField";
	/**
	 * Does not start with (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_INOTSTARTSWITH_FIELD = "iNotStartsWithField";
	/**
	 * Does not end with (case insensitive) another field value (specify fieldName as criterion.value)
	 */
	final String TEXT_MATCH_INOTENDSWITH_FIELD = "iNotEndsWithField";
	
	/**
	 * all subcriteria (criterion.criteria) are true
	 */
	final String TEXT_MATCH_AND = "and";
	/**
	 * all subcriteria (criterion.criteria) are false
	 */
	final String TEXT_MATCH_OR = "or";
	/**
	 * at least one subcriteria (criterion.criteria) is true
	 */
	final String TEXT_MATCH_NOT = "not";
	/**
	 * shortcut for "greaterThan" + "lessThan" + "and". Specify criterion.start and criterion.end
	 */
	final String TEXT_MATCH_BETWEEN = "between";
	/**
	 * shortcut for "greaterOrEqual" + "lessOrEqual" + "and". Specify criterion.start and criterion.end
	 */
	final String TEXT_MATCH_BETWEENINCLUSIVE = "betweenInclusive";
	
	/*** встречаются в коде ***/
	final String TEXT_MATCH_SUBSTRING = "substring";
	final String TEXT_MATCH_EXACT = "exact";
	final String TEXT_MATCH_EXACT_CASE = "exactCase";
	
	/*** Isys Add-ons ***/
	final String TEXT_MATCH_NOTEXISTS = "notExists";
	
	final String JMS_ACTION = "action";
	final String JMS_STATUS = "status";
	final String JMS_FACTORY = "factory";
	final String JMS_ACTION_REBUILD_FACTORY = "rebuildFactory";
	final Integer JMS_STATUS_OK = 0;
	
	/*** Response codes ***/
	final Integer STATUS_SUCCESS = 0;
	final Integer STATUS_OFFLINE = 1;
	final Integer STATUS_FAILURE = -1;
	final Integer STATUS_AUTHORIZATION_FAILURE = -3;
	final Integer STATUS_VALIDATION_ERROR = -4;
	final Integer STATUS_LOGIN_INCORRECT = -5;
	final Integer STATUS_MAX_LOGIN_ATTEMPTS_EXCEEDED = -6;
	final Integer STATUS_LOGIN_REQUIRED = -7;
	final Integer STATUS_LOGIN_SUCCESS = -8;
	final Integer STATUS_UPDATE_WITHOUT_PK_ERROR = -9;
	final Integer STATUS_TRANSACTION_FAILED = -10;
	final Integer STATUS_MAX_FILE_SIZE_EXCEEDED = -11;
	final Integer STATUS_MAX_POST_SIZE_EXCEEDED = -12;
	final Integer STATUS_FILE_REQUIRED_ERROR = -15;
	final Integer STATUS_TRANSPORT_ERROR = -90;
	final Integer STATUS_UNKNOWN_HOST_ERROR = -91;
	final Integer STATUS_CONNECTION_RESET_ERROR = -92;
	final Integer STATUS_SERVER_TIMEOUT = -100;

	final String APIKEY = "apiKey";
	final String SIGNATURE = "signature";
	final String SECRET = "secret";

}
