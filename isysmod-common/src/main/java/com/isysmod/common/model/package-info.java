@XmlSchema(
		xmlns = {
				@XmlNs(prefix = "xsi", // for MOXy - xsi, for JAXB "" empty
						namespaceURI = XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI)
		},
		elementFormDefault = XmlNsForm.QUALIFIED,
		attributeFormDefault = XmlNsForm.UNQUALIFIED) 

@XmlJavaTypeAdapters({
    @XmlJavaTypeAdapter(type = java.time.LocalDateTime.class, 
                        value = LocalDateTimeAdapter.class),
    @XmlJavaTypeAdapter(type = java.time.LocalDate.class, 
    					value = LocalDateAdapter.class),
    @XmlJavaTypeAdapter(type = java.time.LocalTime.class, 
						value = LocalTimeAdapter.class)
})

package com.isysmod.common.model;

import javax.xml.XMLConstants;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.isysmod.common.jaxb.adapters.LocalDateAdapter;
import com.isysmod.common.jaxb.adapters.LocalDateTimeAdapter;
import com.isysmod.common.jaxb.adapters.LocalTimeAdapter;
