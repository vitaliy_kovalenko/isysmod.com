package com.isysmod.common.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

public class Entity extends EntityBean implements java.io.Serializable {

	private static final long serialVersionUID = -4578726753198700707L;

	@XmlElement(nillable = true)
	private String id;
	
	@XmlElement(nillable = true)
	private String code;
	
	@XmlElement(nillable = true)
	private String name;
	
	@XmlElement(nillable = true)
	private String databaseId;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Database database;
	
	public Entity() {
	}
	
	public Entity(String code, String name, String databaseId) {
		this.code = code;
		this.name = name;
		this.databaseId = databaseId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDatabaseId() {
		return databaseId;
	}

	public void setDatabaseId(String databaseId) {
		this.databaseId = databaseId;
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

}
