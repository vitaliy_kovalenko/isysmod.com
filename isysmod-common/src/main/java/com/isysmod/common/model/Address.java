package com.isysmod.common.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

public class Address extends EntityBean implements Serializable {

	private static final long serialVersionUID = 9177946638547104069L;

	@XmlElement(nillable = true)
	private String id;
	
	@XmlElement(nillable = true)
	private Integer postcode;
	
	@XmlElement(nillable = true)
	private String country;
	
	@XmlElement(nillable = true)
	private String province;
	
	@XmlElement(nillable = true)
	private String city;
	
	@XmlElement(nillable = true)
	private String street;
	
	@XmlElement(nillable = true)
	private String house;
	
	@XmlElement(nillable = true)
	private String apartment;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getPostcode() {
		return postcode;
	}

	public void setPostcode(Integer postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public String getApartment() {
		return apartment;
	}

	public void setApartment(String apartment) {
		this.apartment = apartment;
	}
	
}
