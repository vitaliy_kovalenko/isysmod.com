package com.isysmod.common.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

public class Profile extends EntityBean implements Serializable {

	private static final long serialVersionUID = -1343040243454394334L;

	@XmlElement(nillable = true)
	private String id;

	@XmlElement(nillable = true)
	private String skype;
	
	@XmlElement(nillable = true)
	private String mobilePhone;
	
	@XmlElement(nillable = true)
	private String homePhone;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Address address;

	@XmlElement(nillable = true)
	private LocalDate birthDate;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSkype() {
		return skype;
	}

	public void setSkype(String skype) {
		this.skype = skype;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		return "Profile [id=" + id + ", skype=" + skype + ", mobilePhone=" + mobilePhone + ", homePhone=" + homePhone
				+ ", birthDate=" + birthDate + "]";
	}
	
}
