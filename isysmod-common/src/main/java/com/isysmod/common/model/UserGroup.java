package com.isysmod.common.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

public class UserGroup extends EntityBean implements Serializable, Hierarchical, ChangeTracking {

	private static final long serialVersionUID = 27244852487747439L;

	@XmlElement(nillable = true)
	private String id;
	
	@XmlElement(nillable = true)
	private String name;
	
	@XmlElement(nillable = true)
	private String parentId;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private User createdBy;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private User updatedBy;
	
	@XmlElement(nillable = true)
	private LocalDateTime createdAt;
	
	@XmlElement(nillable = true)
	private LocalDateTime updatedAt;
	
	@XmlElement(nillable = true)
	private String createdById;
	
	@XmlElement(nillable = true)
	private String updatedById;

	@Override
	public String getParentId() {
		return parentId;
	}

	@Override
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public EntityBean getCreatedBy() {
		return createdBy;
	}

	@Override
	public void setCreatedBy(EntityBean createdBy) {
		this.createdBy = (User) createdBy;
	}

	@Override
	public User getUpdatedBy() {
		return updatedBy;
	}

	@Override
	public void setUpdatedBy(EntityBean updatedBy) {
		this.updatedBy = (User) updatedBy;
	}

	@Override
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	@Override
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	@Override
	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	@Override
	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Override
	public String getCreatedById() {
		return createdById;
	}

	@Override
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}

	@Override
	public String getUpdatedById() {
		return updatedById;
	}

	@Override
	public void setUpdatedById(String updatedById) {
		this.updatedById = updatedById;
	}

}
