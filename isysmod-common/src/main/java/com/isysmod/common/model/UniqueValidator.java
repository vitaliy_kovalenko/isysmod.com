package com.isysmod.common.model;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Restrictions;

import com.isysmod.common.SpringContextUtil;
import com.isysmod.common.dao.HibernateDAO;

public class UniqueValidator implements ConstraintValidator<Unique, Serializable> {
	
	private final Logger LOGGER = Logger.getLogger(UniqueValidator.class.getName());
	
	private HibernateDAO<EntityBean> hibernateDAO;

	private String[] properties;
	
	private String identifier;
	
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(Unique unique) {
		this.hibernateDAO = (HibernateDAO<EntityBean>) SpringContextUtil.getApplicationContext().getBean(SpringContextUtil.DAO_BEAN_NAME);
		this.properties = unique.properties();
		this.identifier = unique.identifier();
	}

	@Override
	public boolean isValid(Serializable target, ConstraintValidatorContext context) {
		boolean isValid = false;
		Session session = hibernateDAO.getSessionFactory().openSession();
		
        if (properties.length > 0) {
        	try {
				isValid = this.validate(target, session);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| IntrospectionException e) {
				LOGGER.error(e);
			}
        }
        
		session.close();
		
		if (! isValid) {
			context.disableDefaultConstraintViolation();
			
			for (int i= 0; i < properties.length; i++) {
				context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
					.addPropertyNode(properties[i]).addConstraintViolation();
			}
		}
		
		return isValid;
	}

	private boolean validate(Serializable target, Session session)
			throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    	Class<?> entityClass = target.getClass();
		Criteria criteria = session.createCriteria(entityClass);
		Conjunction conjunction = Restrictions.conjunction();
    	PropertyDescriptor descriptor = new PropertyDescriptor(this.identifier, entityClass);
		Method method = descriptor.getReadMethod();
		Object id = method.invoke(target);
		
		if (id != null) {
			conjunction.add(Restrictions.ne(this.identifier, id));
		}
		
		for (int i= 0; i < properties.length; i++) {
			String propertyName = properties[i];
			descriptor = new PropertyDescriptor(propertyName, entityClass);
			method = descriptor.getReadMethod();
			Object value = method.invoke(target);
			conjunction.add(Restrictions.eq(propertyName, value));
		}

		criteria.add(conjunction);
		
		return criteria.list().size() == 0;
	}

}
