package com.isysmod.common.model;
// Generated Jun 13, 2016 12:28:34 PM by Hibernate Tools 5.0.0.Final


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

/**
 * ClientDataSource generated by hbm2java
 */
public class ClientDatabase extends EntityBean implements java.io.Serializable {
	
	private static final long serialVersionUID = 2967518687347184330L;
	
	@XmlElement(nillable = true)
	private String id;
	
	@XmlElement(nillable = true)
	private String clientId;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Client client;
	
	@XmlElement(nillable = true)
	private String databaseId;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Database database;
	
	@XmlElement(nillable = true)
	private Boolean isEnabled;

    public ClientDatabase() {
    }

	
    public ClientDatabase(String clientId, String databaseId) {
        this.clientId = clientId;
        this.databaseId = databaseId;
    }
    public ClientDatabase(String clientId, String databaseId, Boolean enabled) {
       this.clientId = clientId;
       this.databaseId = databaseId;
       this.isEnabled = enabled;
    }
   
    public String getId() {
        return this.id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    public String getClientId() {
        return this.clientId;
    }
    
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}

    public String getDatabaseId() {
        return this.databaseId;
    }
    
    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

	public Database getDatabase() {
		return database;
	}


	public void setDatabase(Database database) {
		this.database = database;
	}


	public Boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    public void setIsEnabled(Boolean enabled) {
        this.isEnabled = enabled;
    }

}


