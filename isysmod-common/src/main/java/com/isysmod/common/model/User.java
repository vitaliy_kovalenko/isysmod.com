package com.isysmod.common.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

@Unique.List(value = {@Unique(properties = { "username" }), @Unique(properties = { "email" })})
public class User extends EntityBean implements Serializable {

	private static final long serialVersionUID = -4485844521270567169L;

	@XmlElement(nillable = true)
	private String id;

	@NotBlank
	@Length(min = 6, max = 255)
	@XmlElement(nillable = true)
	private String username;

	@NotBlank
	@Length(min = 6, max = 255)
	@XmlElement(nillable = true)
	private String pubPassword = "******";

	@NotBlank
	@Email
	@XmlElement(nillable = true)
	private String email;
	
	@XmlTransient
	private String password;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Profile profile;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPubPassword() {
		return pubPassword;
	}

	public void setPubPassword(String pubPassword) {
		this.pubPassword = pubPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		if (!"******".equals(pubPassword) && pubPassword != null) {
			BCryptPasswordEncoder e = new BCryptPasswordEncoder();
			this.password = e.encode(pubPassword);
		}
		
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", pubPassword=" + pubPassword + ", email=" + email
				+ ", password=" + password + ", profile=" + profile + "]";
	}

}
