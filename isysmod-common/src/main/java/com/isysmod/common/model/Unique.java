package com.isysmod.common.model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Usage:
 * <ul>
 * <li>@Unique(identifier="id", properties={"username"}) one unique key</li>
 * <li>@Unique(properties={"username", "email"}) composite unique key</li>
 * <li>@Unique.List(value = {@Unique(properties = { "username" }), @Unique(properties = { "email" })}) more than one unique keys</li>
 * </ul>
 * @author vitaliy
 *
 */
@Constraint(validatedBy={UniqueValidator.class})
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Unique {

    String message() default "{com.isysmod.common.model.Unique.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String identifier() default "id";
    
    String[] properties();

    @Target({ ElementType.TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
    	Unique[] value();
    }
	
}
