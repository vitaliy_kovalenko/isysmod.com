package com.isysmod.common.model;

import java.time.LocalDateTime;

public interface ChangeTracking {

	public EntityBean getCreatedBy();
	
	public void setCreatedBy(EntityBean createdBy);
	
	public EntityBean getUpdatedBy();
	
	public void setUpdatedBy(EntityBean updatedBy);
	
	public LocalDateTime getCreatedAt();
	
	public void setCreatedAt(LocalDateTime createdAt);
	
	public LocalDateTime getUpdatedAt();
	
	public void setUpdatedAt(LocalDateTime updatedAt);

	public String getCreatedById();
	
	public void setCreatedById(String createdById);
	
	public String getUpdatedById();
	
	public void setUpdatedById(String updatedById);
	
}
