package com.isysmod.common.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.PersistentAdapter;

public class EntityField extends EntityBean implements java.io.Serializable {

	private static final long serialVersionUID = 7706598740618479022L;

	@XmlElement(nillable = true)
	private String id;
	
	@XmlElement(nillable = true)
	private String code;
	
	@XmlElement(nillable = true)
	private String name;
	
	@XmlElement(nillable = true)
	private String entityId;
	
	@XmlElement(nillable = true)
	@XmlJavaTypeAdapter(PersistentAdapter.class)
	private Entity entity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

}
