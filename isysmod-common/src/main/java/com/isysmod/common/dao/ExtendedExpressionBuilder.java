package com.isysmod.common.dao;

import org.hibernate.criterion.Criterion;

import com.isysmod.common.jaxb.isc.IscCriterion;

public interface ExtendedExpressionBuilder {

	public Criterion buildExpression(IscCriterion iscCriterion, String rootAlias);
	
}
