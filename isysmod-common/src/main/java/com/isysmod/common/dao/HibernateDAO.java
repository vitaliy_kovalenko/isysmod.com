package com.isysmod.common.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import com.isysmod.common.IC;
import com.isysmod.common.IsysUtils;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.exceptions.UpdateWithoutPrimaryKeyException;
import com.isysmod.common.model.ChangeTracking;
import com.isysmod.common.model.EntityBean;
import com.isysmod.common.security.UserPrincipal;

public class HibernateDAO<E extends EntityBean> {
	
	private final Logger LOGGER = Logger.getLogger(HibernateDAO.class.getName());

	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	private Criteria rootAliasAndCriteria(QueryParams<E> queryParams) {
		Class<E> entityClass = queryParams.getEntityClass();
		String rootAlias = IsysUtils.entityAlias(entityClass);
		Session s = this.sessionFactory.getCurrentSession();
		Criteria c = s.createCriteria(entityClass, rootAlias);
		
		if (queryParams.getAliases().size() > 0) {
			for (Map.Entry<String, String> entry : queryParams.getAliases().entrySet()) {
				c.createAlias( entry.getKey(), entry.getValue() );
			}
		}
		
		if (queryParams.getCriterion() != null) {
			c.add(queryParams.getCriterion());
		}
		
		return c;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public QueryResult<E> fetch(QueryParams<E> queryParams) {
		List<E> data = new ArrayList<>(0);
		Class<E> entityClass = queryParams.getEntityClass();
		LOGGER.debug("Fetching " + entityClass.getName() + ", start " + queryParams.getFirstResult() + ", end " + queryParams.getEndResult());
		Criteria c = rootAliasAndCriteria(queryParams);
		c.setProjection(Projections.rowCount());
		Long totalRows = (Long) c.uniqueResult();
		
		if (totalRows > 0) {
			c.setProjection(null);
			c.setResultTransformer(Criteria.ROOT_ENTITY);
			
			for (Order order : queryParams.getOrders()) {
				c.addOrder(order);
			}
			
			c.setFirstResult(queryParams.getFirstResult());
			c.setMaxResults(queryParams.getEndResult() - queryParams.getFirstResult());
			
			data = (List<E>) c.list();
		}
		
		QueryResult<E> result = new QueryResult<E>();
		result.setStartRow(queryParams.getFirstResult());
		result.setEndRow(queryParams.getFirstResult() + data.size());
		result.setTotalRows(totalRows);
		result.setData(data);
		
		return result;
	}
	
	@Transactional()
	public QueryResult<E> add(QueryParams<E> queryParams) {
		E detached = queryParams.getEntity();
		Class<?> entityClass = detached.getClass();
		LOGGER.debug("Adding " + entityClass.getName());
		
		if (detached instanceof ChangeTracking) {
			setCreatedByAndCreatedAt(detached);
		}
		
		Session s = this.sessionFactory.getCurrentSession();
		s.persist(detached);
		
		s.flush();
		Serializable id = IsysUtils.getEntityId(detached);
		s.getTransaction().commit();
		s.clear();
		s.getTransaction().begin(); // Will be closed by Spring
		@SuppressWarnings("unchecked")
		E persistent = (E) s.get(entityClass, id);

		List<E> data = new ArrayList<>();
		data.add(persistent);
		
		QueryResult<E> result = new QueryResult<E>();
		result.setData(data);
		
		return result;
	}

	@Transactional()
	public QueryResult<E> update(QueryParams<E> queryParams) throws ParseException {
		E persistent;
		
		if (queryParams.isPartialUpdate()) {
			persistent = performPartialUpdate(queryParams);
		} else {
			persistent = performFullUpdate(queryParams);
		}
		
		List<E> data = new ArrayList<>();
		data.add(persistent);
		
		QueryResult<E> result = new QueryResult<E>();
		result.setData(data);
		
		return result;
	}

	@Transactional()
	public QueryResult<E> remove(QueryParams<E> queryParams) {
		E transientWithId = queryParams.getEntity();
		LOGGER.debug("Deleting " + transientWithId.getClass().getName());
		Session s = this.sessionFactory.getCurrentSession();
		s.delete(transientWithId);

		List<E> data = new ArrayList<>();
		data.add(transientWithId);
		
		return new QueryResult<E>();
	}

	private E performPartialUpdate(QueryParams<E> queryParams) throws ParseException {
		Class<E> entityClass = queryParams.getEntityClass();
		LOGGER.debug("Performing partial update " + entityClass.getName());
		Serializable id = queryParams.getEntityId();
		
		if (id == null) {
			throw new UpdateWithoutPrimaryKeyException();
		}
		
		Session s = this.sessionFactory.getCurrentSession();
		E persistent = s.get(entityClass, id);
		
		if (persistent == null) {
			throw new IsysException("Entity data with id = " + id + " not found.");
		}
		
		boolean wasChanged = false;
		
		try {
			wasChanged = updateValuesFromMap(persistent, entityClass, queryParams.getRequestData());
		} catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
			throw new IsysException(e);
		}
		
		if (wasChanged) {
			if (persistent instanceof ChangeTracking) {
				setUpdatedByAndUpdatedAt(persistent);
			}
			// Поскольку я вынужден производить обновление внешних ключей через идентификатор,
			// а не присвоением объекта (как рекомендовано), то приходится сохранять данные
			// до закрытия транзакции и заново вытаскивать объект, чтобы вывести на клиент
			// возможные изменения связанных сущностей.
			s.flush();
			s.getTransaction().commit();
			s.clear();
			s.getTransaction().begin(); // Will be closed by Spring
			persistent = (E) s.get(entityClass, queryParams.getEntityId());
		}
		
		return persistent;
	}
	
	private E performFullUpdate(QueryParams<E> queryParams) {
		E detached = queryParams.getEntity();
		
		if (detached == null) {
			throw new IsysException("Entity data have not been received with the request.");
		}
		
		Serializable id = queryParams.getEntityId();
		
		if (id == null) {
			throw new UpdateWithoutPrimaryKeyException();
		}
		
		Session s = this.sessionFactory.getCurrentSession();
		
		if ("User".equals(detached.getClass().getSimpleName())) {
			Object password;
			
			try {
				password = IsysUtils.getObjectProperty(detached, IC.ISC_PASSWORD_PROPERTY_NAME);
				
				if (password == null) {
					Criteria c = s.createCriteria(detached.getClass())
							.add(Restrictions.eq(IC.ISC_ID_PROPERTY_NAME, queryParams.getEntityId()))
							.setProjection(Projections.property(IC.ISC_PASSWORD_PROPERTY_NAME));
					password = c.uniqueResult();
					IsysUtils.setObjectProperty(detached, IC.ISC_PASSWORD_PROPERTY_NAME, password);
				}
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				throw new IsysException(e);
			}
		}
		
		if (detached instanceof ChangeTracking) {
			setUpdatedByAndUpdatedAt(detached);
		}
		
		s.merge(detached);
		// Долбанный клиент не умеет работать с объектами. Производит обновление связей по ID.
		// Чтобы выдать обновленные дочерние объекты, я вынужден ичистить сессию.
		s.flush();
		s.getTransaction().commit();
		s.clear();
		s.getTransaction().begin(); // Will be closed by Spring
		
		return (E) s.get(queryParams.getEntityClass(), queryParams.getEntityId());
	}

	@SuppressWarnings("unchecked")
	private boolean updateValuesFromMap(E persistent, Class<E> entityClass, Map<String, Object> data)
			throws IllegalArgumentException, IllegalAccessException, InstantiationException, ParseException {
		boolean wasChanged = false;
		
		for (Map.Entry<String, Object> entry : data.entrySet()) {
			String fieldNme = entry.getKey();
			Field[] fields = entityClass.getDeclaredFields();
			Field field = null;
			
			for (Field f : fields) {
				if (f.getName().equals(fieldNme)) {
					field = f;
					break;
				}
			}
			
			if (field == null) {
				continue;
			}
			
			Class<?> fieldType = field.getType();
			Object value;
			
			if (EntityBean.class.isAssignableFrom(fieldType)) {
				value = entry.getValue();
				
				if (value != null && ! (value instanceof Map)) {
					throw new IsysException("Expected value type of " + fieldNme + " is a Map but found " + value.getClass().getName());
				}

				field.setAccessible(true);
				E child = (E) field.get(persistent);
				
				if (value == null && child == null) {
					continue;
				} else if (value == null) {
					field.set(persistent, null);
					wasChanged = true;
					field.setAccessible(false);
					continue;
				} else if (child == null) {
					child = (E) fieldType.newInstance();
					field.set(persistent, child);
					wasChanged = true;
				}

				if (updateValuesFromMap(child, (Class<E>) fieldType, (Map<String,Object>) value)) {
					wasChanged = true;
				}
				
				field.setAccessible(false);
				continue;
			} else {
				value = IsysUtils.parseValue(fieldType, entry.getValue());
			}
			
			field.setAccessible(true);
			Object oldValue = field.get(persistent);
			
			if (
					(value != null && !value.equals(oldValue)) ||
					(value == null && oldValue != null)
				) {
				field.set(persistent, value);
				wasChanged = true;
			}
			
			field.setAccessible(false);
			LOGGER.debug(fieldType.getSimpleName() + " " + fieldNme + " = " + value);
		}
		
		return wasChanged;
	}
	
	private void setCreatedByAndCreatedAt(E detached) {
		ChangeTracking tracking = (ChangeTracking) detached;
		UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		tracking.setCreatedById((String) principal.getId());
		tracking.setCreatedAt(LocalDateTime.now(ZoneOffset.UTC));
	}
	
	private void setUpdatedByAndUpdatedAt(E entity) {
		ChangeTracking tracking = (ChangeTracking) entity;
		UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		tracking.setUpdatedById((String) principal.getId());
		tracking.setUpdatedAt(LocalDateTime.now(ZoneOffset.UTC));
	}

}
