package com.isysmod.common.dao;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;

public class ValidationEventListener implements PreInsertEventListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6705096703141317400L;
	
	@Override
	public boolean onPreInsert(PreInsertEvent event) {
		return false;
	}

}
