package com.isysmod.common.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.NullPrecedence;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.isysmod.common.IC;
import com.isysmod.common.IsysUtils;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.jaxb.isc.IscAdvancedCriteria;
import com.isysmod.common.jaxb.isc.IscCriterion;
import com.isysmod.common.jaxb.isc.IscSingleRequest;
import com.isysmod.common.model.EntityBean;

public class QueryParams<E extends EntityBean> {
	
	private final Logger LOGGER = Logger.getLogger(QueryParams.class.getName());
	
	private static final String ALIASES_SEPARATOR = ":";
	
	private Class<E> entityClass;
	
	private IscSingleRequest<E> iscRequest;
	
	private Criterion criterion;
	
	private List<Order> orders = new ArrayList<>();
	
	private Map<String, String> aliases = new LinkedHashMap<>();
	
	private boolean partialUpdate = false;
	
	private ExtendedExpressionBuilder expressionBuilder;

	
	public QueryParams(Class<E> entityClass, IscSingleRequest<E> iscRequest)
			throws NoSuchFieldException, SecurityException {
		this.entityClass = entityClass;
		this.iscRequest = iscRequest;
	}
	
	public void prepareParams()
			throws NoSuchFieldException, SecurityException, ParseException {
		switch (this.iscRequest.getOperationType()) {
			case IC.METHOD_FETCH:
				if (this.iscRequest.getRequestData() != null && this.iscRequest.getRequestData().size() > 0) {
					this.buildSimpleCriterias();
				} else if (this.iscRequest.getAdvancedCriteria() != null) {
					buildAdvancedCriteria(this.iscRequest.getAdvancedCriteria());
				}
				
				buildOrdersAndAliases( this.iscRequest.getSortBy() );
			break;
			case IC.METHOD_ADD:
			case IC.METHOD_REMOVE:
			break;
			case IC.METHOD_UPDATE:
				partialUpdate = this.iscRequest.getRequestData() != null && this.iscRequest.getRequestData().size() > 0;
			break;
		}
	}

	public E getEntity() {
		return this.iscRequest.getEntity();
	}

	public Class<E> getEntityClass() {
		return this.entityClass;
	}
	
	public Map<String,Object> getRequestData() {
		return iscRequest.getRequestData();
	}
	
	public String getOperationType() {
		return this.iscRequest.getOperationType();
	}
	
	public Integer getFirstResult() {
		return this.iscRequest.getStartRow();
	}

	
	public Integer getEndResult() {
		return this.iscRequest.getEndRow();
	}

	public Criterion getCriterion() {
		return criterion;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	
	public Serializable getEntityId() {
		Serializable id = null;
		
		if (this.iscRequest.getRequestData() != null) {
			id = IsysUtils.getEntityId( this.iscRequest.getRequestData() );
		} else {
			id = IsysUtils.getEntityId( getEntity() );
		}
		
		return id;
	}
	
	public Map<String, String> getAliases() {
		return aliases;
	}
	
	public boolean isPartialUpdate() {
		return partialUpdate;
	}
	
	public void setExpressionBuilder(ExtendedExpressionBuilder expressionBuilder) {
		this.expressionBuilder = expressionBuilder;
	}
	
	private void buildSimpleCriterias() throws NoSuchFieldException, SecurityException, ParseException {
		if (this.criterion == null || this.criterion instanceof Junction) { // AND
			Junction junction = Restrictions.conjunction();
			
			if (this.criterion == null) {
				this.criterion = junction;
			} else {
				((Junction) this.criterion).add(junction);
			}
	
			for (Map.Entry<String, Object> entry : this.iscRequest.getRequestData().entrySet()) {
				String fieldNme = entry.getKey();
				String textMatchStyle = this.iscRequest.getTextMatchStyle();
				
				if (entry.getValue() == null && !nullExpressionAllowed(textMatchStyle)) {
					textMatchStyle = IC.TEXT_MATCH_ISNULL;
				}
				
				LOGGER.debug(fieldNme + " " + textMatchStyle + " " + entry.getValue());
				
				Criterion expression = buildExpression(
						fieldNme,
						entry.getValue(),
						textMatchStyle,
						null,
						null);
				
				if (expression != null) {
					junction.add( expression );
				}
			}
		}
	}
	
	private void appendExpressions(Junction junction, List<EntityBean> elements)
			throws NoSuchFieldException, SecurityException, ParseException {
		for (EntityBean elem : elements) {
			if (elem instanceof IscAdvancedCriteria) {
				IscAdvancedCriteria criteria = (IscAdvancedCriteria) elem;
				buildAdvancedCriteria( criteria );
			} else if (elem instanceof IscCriterion) {
				IscCriterion iscCriterion = (IscCriterion) elem;
				
				Criterion expression;
				
				if (iscCriterion.isExtended()) {
					String rootAlias = IsysUtils.entityAlias(entityClass);
					expression = this.expressionBuilder.buildExpression(iscCriterion, rootAlias);
				} else {
					String fieldNme = iscCriterion.getFieldName();
					String operator = iscCriterion.getOperator();
					
					if (iscCriterion.getValue() == null && !nullExpressionAllowed(operator)) {
						operator = IC.TEXT_MATCH_ISNULL;
					}
					
					LOGGER.debug(fieldNme + " " + operator + " " + iscCriterion.getValue());
					
					expression = buildExpression(
							fieldNme,
							iscCriterion.getValue(),
							operator,
							null,
							null);
				}
				
				if (expression != null) {
					junction.add( expression );
				}
			}
		}
	}
	
	private void buildAdvancedCriteria(IscAdvancedCriteria advancedCriteria)
			throws NoSuchFieldException, SecurityException, ParseException {
		Criterion criterion;
		
		if (IC.TEXT_MATCH_NOT.equals(advancedCriteria.getOperator())) {
			Junction junction = Restrictions.disjunction();
			appendExpressions( junction, advancedCriteria.getElements() );
			criterion = Restrictions.not( junction );
		} else if (IC.TEXT_MATCH_OR.equals(advancedCriteria.getOperator())) {
			criterion = Restrictions.disjunction();
		} else {
			criterion = Restrictions.conjunction();
		}
		
		if (this.criterion == null) {
			this.criterion = criterion; // AND/OR/NOT
		} else if (this.criterion instanceof Junction) {
			((Junction) this.criterion).add(criterion); // AND/OR
		}
		
		if (!IC.TEXT_MATCH_NOT.equals(advancedCriteria.getOperator())) {
			appendExpressions( (Junction) criterion, advancedCriteria.getElements() );
		}
	}
	
	private void addOrder(String orderPath, boolean descending) {
		if (descending) {
			orders.add(Order.desc(orderPath).nulls(NullPrecedence.LAST));
		} else {
			orders.add(Order.asc(orderPath).nulls(NullPrecedence.LAST));
		}
		
		LOGGER.debug( (descending ? "Order.desc(" : "Order.asc(") + orderPath + ")" );
	}
	
	private void buildOrdersAndAliases(String[] fields) {
		if (fields != null && fields.length > 0) {
			for (int i = 0; i < fields.length; i++) {
				String orderPath = fields[i];
				boolean isDescending = false;
				
				if (orderPath.startsWith("-")) {
					orderPath = orderPath.substring(1);
					isDescending = true;
				}

				orderPath = propertyAlias(orderPath);
				addOrder(orderPath, isDescending);
			}
		}
	}
	
	private String propertyAlias(String fieldPath) {
		String propertyAlias = fieldPath;
		
		if (fieldPath.contains("/")) {
			fieldPath = fieldPath.replace('/', '.');
		}
		
		if (fieldPath.contains(".")) {
			String fieldName = fieldPath.substring(fieldPath.lastIndexOf(".") + 1, fieldPath.length());
			String associationPath = fieldPath.substring(0, fieldPath.lastIndexOf("."));
			String associationAlias = associationPath.replace(".", ALIASES_SEPARATOR) + "_";
			propertyAlias = associationAlias + "." + fieldName;
			
			if (!equals(aliases.containsKey(associationPath))) {
				aliases.put(associationPath, associationAlias);
				LOGGER.debug( "createAlias(\"" + associationPath + "\", \"" + associationAlias + "\")" );
			}
		}
		
		return propertyAlias;
	}
	
	private Class<?> propertyType(Class<?> entityClass, String propertyName) throws NoSuchFieldException, SecurityException {
		return entityClass.getDeclaredField(propertyName).getType();
	}
	
	private Class<?> propertyType(String propertyPath) throws NoSuchFieldException, SecurityException {
		propertyPath = propertyPath.replace('/', '.');
		Class<?> propertyType = entityClass;
		
		if (propertyPath.contains(".")) {
			String[] path = propertyPath.split("\\.");
			
			for (int i = 0; i < path.length; i ++) {
				Class<?> type = propertyType(propertyType, path[i]);
				
				if (Collection.class.isAssignableFrom(type)) {
					Field field = propertyType.getDeclaredField(path[i]);
					Type genericType = field.getGenericType();
					ParameterizedType parameterizedType = (ParameterizedType) genericType;
					Type[] argumentTypes = parameterizedType.getActualTypeArguments();
					propertyType = (Class<?>) argumentTypes[0];
				} else {
					propertyType = type;
				}
			}
		} else {
			propertyType = propertyType(propertyType, propertyPath);
		}
		
		return propertyType;
	}
	
	private boolean nullExpressionAllowed(String matchStyle) {
		return IC.TEXT_MATCH_ISNULL.equals(matchStyle) ||
				IC.TEXT_MATCH_NOTNULL.equals(matchStyle) ||
				IC.TEXT_MATCH_BETWEEN.equals(matchStyle) ||
				IC.TEXT_MATCH_BETWEENINCLUSIVE.equals(matchStyle) ||
				IC.TEXT_MATCH_IBETWEENINCLUSIVE.equals(matchStyle);
	}
	
	@SuppressWarnings("unchecked")
	private Criterion buildExpression(String propertyPath, Object value, String matchStyle, String start, String end)
			throws NoSuchFieldException, SecurityException, ParseException {
		String propertyAlias = propertyAlias(propertyPath);
		Class<?> propertyType = propertyType(propertyPath);
		value = IsysUtils.parseValue(propertyType, value);
		
		if (
				value == null && !nullExpressionAllowed(matchStyle)
			) {
			return null;
		}
		
		Criterion criterion;
		
		switch (matchStyle) {
			case IC.TEXT_MATCH_STARTWITH:
				criterion = Restrictions.like(propertyAlias, value + "%");
			break;
			case IC.TEXT_MATCH_ISTARTWITH: // начинается с
				criterion = Restrictions.ilike(propertyAlias, value + "%");
			break;
			case IC.TEXT_MATCH_ENDSWITH:
				criterion = Restrictions.like(propertyAlias, "%" + value);
			break;
			case IC.TEXT_MATCH_IENDSWITH: // заканчивается на
				criterion = Restrictions.ilike(propertyAlias, "%" + value);
			break;
			case IC.TEXT_MATCH_INOTSTARTWITH: // не начинается с
				criterion = Restrictions.not(Restrictions.ilike(propertyAlias, value + "%"));
			break;
			case IC.TEXT_MATCH_INOTENDSWITH: // не заканчивается на
				criterion = Restrictions.not(Restrictions.ilike(propertyAlias, "%" + value));
			break;
			case IC.TEXT_MATCH_SUBSTRING:
			case IC.TEXT_MATCH_ICONTAINS: // содержит
				criterion = Restrictions.ilike(propertyAlias, "%" + value + "%");
			break;
			case IC.TEXT_MATCH_INOTCONTAINS: // не содержит
				criterion = Restrictions.not(Restrictions.ilike(propertyAlias, "%" + value + "%"));
			break;
			case IC.TEXT_MATCH_EXACT:
			case IC.TEXT_MATCH_IEQUAL: // равно (без учета регистра)
				criterion = Restrictions.ilike(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_INOTEQUAL: // не равно (без учета регистра)
				criterion = Restrictions.not(Restrictions.ilike(propertyAlias, value));
			break;
			case IC.TEXT_MATCH_EXACT_CASE:
				criterion = Restrictions.like(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_LESSTHAN: // меньше чем
				criterion = Restrictions.lt(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_GREATERTHAN: // больше чем
				criterion = Restrictions.gt(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_LESSOREQUAL: // меньше или равно
				criterion = Restrictions.le(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_GREATEROREQUAL: // больше или равно
				criterion = Restrictions.ge(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_ISNULL: // пусто
				criterion = Restrictions.isNull(propertyAlias);
			break;
			case IC.TEXT_MATCH_NOTNULL: // не пусто
				criterion = Restrictions.isNotNull(propertyAlias);
			break;
			case IC.TEXT_MATCH_CONTAINS_WILDCARD: // содержит шаблон
				criterion = Restrictions.like(propertyAlias, ((String) value).replaceAll("\\*", "%"));
			break;
			case IC.TEXT_MATCH_ICONTAINS_WILDCARD: // содержит шаблон
				criterion = Restrictions.ilike(propertyAlias, ((String) value).replaceAll("\\*", "%"));
			break;
			case IC.TEXT_MATCH_EQUAL: // равно
				criterion = Restrictions.eq(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_NOTEQUAL: // не равно
				criterion = Restrictions.ne(propertyAlias, value);
			break;
			case IC.TEXT_MATCH_BETWEENINCLUSIVE:
			case IC.TEXT_MATCH_IBETWEENINCLUSIVE:
			case IC.TEXT_MATCH_BETWEEN:
				Object lo;
				Object hi;
				
				try {
					lo = IsysUtils.parseValue(entityClass.getDeclaredField(propertyAlias).getType(), start);
					hi = IsysUtils.parseValue(entityClass.getDeclaredField(propertyAlias).getType(), end);
				} catch (NoSuchFieldException | SecurityException e) {
					throw new IsysException(e);
				}

				if (IC.TEXT_MATCH_BETWEEN.equals(matchStyle)) {
					criterion = Restrictions.conjunction()
							.add(Restrictions.gt(propertyAlias, lo))
							.add(Restrictions.lt(propertyAlias, hi));
				} else {
					criterion = Restrictions.conjunction()
							.add(Restrictions.ge(propertyAlias, lo))
							.add(Restrictions.le(propertyAlias, hi));
				}
			break;
			case IC.TEXT_MATCH_INSET:
				criterion = Restrictions.in(propertyAlias, (List<Object>) value);
			break;
			case IC.TEXT_MATCH_NOTINSET:
				criterion = Restrictions.not(Restrictions.in(propertyAlias, (List<Object>) value));
			break;
			case IC.TEXT_MATCH_EQUAL_FIELD:
				criterion = Restrictions.eqProperty(propertyAlias, (String) value);
			break;
			case IC.TEXT_MATCH_NOTEQUAL_FIELD:
				criterion = Restrictions.neProperty(propertyAlias, (String) value);
			break;
			case IC.TEXT_MATCH_GREATERTHAN_FIELD:
				criterion = Restrictions.gtProperty(propertyAlias, (String) value);
			break;
			case IC.TEXT_MATCH_LESSTHAN_FIELD:
				criterion = Restrictions.ltProperty(propertyAlias, (String) value);
			break;
			case IC.TEXT_MATCH_GREATEROREQUAL_FIELD:
				criterion = Restrictions.geProperty(propertyAlias, (String) value);
			break;
			case IC.TEXT_MATCH_LESSOREQUAL_FIELD:
				criterion = Restrictions.leProperty(propertyAlias, (String) value);
			break;
			default:
				throw new IsysException("Unknown matching operation.");
		}
		
		return criterion;
	}

}
