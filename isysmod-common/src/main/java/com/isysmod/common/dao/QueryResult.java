package com.isysmod.common.dao;

import java.util.List;

import com.isysmod.common.model.EntityBean;

public class QueryResult<E extends EntityBean> {
	
	private Integer startRow;
	
	private Integer endRow;
	
	private Long totalRows;
	
	private List<E> data;

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public void setEndRow(Integer endRow) {
		this.endRow = endRow;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public void setTotalRows(Long totalRows) {
		this.totalRows = totalRows;
	}

	public List<E> getData() {
		return data;
	}

	public void setData(List<E> data) {
		this.data = data;
	}
	
}
