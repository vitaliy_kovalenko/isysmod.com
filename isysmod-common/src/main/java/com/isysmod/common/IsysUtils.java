package com.isysmod.common;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.model.EntityBean;

public class IsysUtils {
	
	static final Logger LOGGER = Logger.getLogger(IsysUtils.class.getName());

	@SuppressWarnings("unchecked")
	static public Object parseValue(Class<?> fieldType, Object value) throws ParseException {
		Object result;
		String valueStr;
		
		if (value == null) {
			return null;
		}
		
		if (value instanceof List) {
			result = new ArrayList<>();
			
			for (Object object : ((List<String>) value)) {
				((List<Object>) result).add(parseValue(fieldType, object));
			}
			
			return result;
		} else {
			valueStr = ((String) value).trim();
		}
		
		if (String.class.equals(fieldType)) {
			result = valueStr.length() > 0 ? valueStr : null;
		} else if (Boolean.class.equals(fieldType)) {
			result = Boolean.parseBoolean(valueStr);
		} else if (Integer.class.equals(fieldType)) {
			result = Integer.parseInt(valueStr);
		} else if (Long.class.equals(fieldType)) {
			result = Long.parseLong(valueStr);
		} else if (Float.class.equals(fieldType)) {
			result = Float.parseFloat(valueStr);
		} else if (Double.class.equals(fieldType)) {
			result = Double.parseDouble(valueStr);
		} else if (Short.class.equals(fieldType)) {
			result = Short.parseShort(valueStr);
		} else if (Byte.class.equals(fieldType)) {
			result = Byte.parseByte(valueStr);
		} else if (Character.class.equals(fieldType)) {
			result = valueStr != null && valueStr.length() == 1 ? valueStr.charAt(0) : null;
		} else if (LocalDateTime.class.equals(fieldType)) {
			result = LocalDateTime.parse(valueStr);
		} else if (LocalDate.class.equals(fieldType)) {
			result = LocalDate.parse(valueStr);
		} else if (LocalTime.class.equals(fieldType)) {
			result = LocalTime.parse(valueStr);
		} else {
			throw new IsysException(fieldType.getName() + " not supported.");
		}
		
		return result;
	}
	
	public static Object getObjectProperty(Object object, String propertyName)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		if (object == null || propertyName == null || propertyName.length() < 1) {
			throw new IllegalArgumentException( "Parameters cannot be null." );
		}
		
		Field field = object.getClass().getDeclaredField(propertyName);
		field.setAccessible(true);
		Object value = field.get(object);
		field.setAccessible(false);
		
		return value;
	}
	
	public static void setObjectProperty(Object object, String propertyName, Object value)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		if (object == null || propertyName == null || propertyName.length() < 1) {
			throw new IllegalArgumentException( "Parameters cannot be null." );
		}
		
		Field field = object.getClass().getDeclaredField(propertyName);
		field.setAccessible(true);
		field.set(object, value);
		field.setAccessible(false);
	}
	
	public static Serializable getEntityId(Map<String,Object> data) {
		if (data != null && data.containsKey(IC.ISC_ID_PROPERTY_NAME)) {
			Object value = data.get(IC.ISC_ID_PROPERTY_NAME);
			
			if (value instanceof String) {
				return (String) data.get(IC.ISC_ID_PROPERTY_NAME);
			}
		}
		
		return null;
	}
	
	public static Serializable getEntityId(EntityBean detached) {
		Serializable id = null;
		
		if (detached != null) {
			try {
				Field field = detached.getClass().getDeclaredField(IC.ISC_ID_PROPERTY_NAME);
				field.setAccessible(true);
				id = (Serializable) field.get(detached);
				field.setAccessible(false);
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				LOGGER.debug(e);
			}
		}
		
		return id;
	}

	public static String entityAlias(String relationBean) {
		return relationBean.substring(0, 1).toLowerCase() + relationBean.substring(1) + "_";
	}

	public static String entityAlias(Class<?> entityClass) {
		return entityAlias( entityClass.getSimpleName() );
	}
	
}
