package com.isysmod.common.exceptions;

public class IsysException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5803740886655034601L;

	public IsysException(String string) {
		super(string);
	}

	public IsysException(Exception e) {
		super( e );
	}

}
