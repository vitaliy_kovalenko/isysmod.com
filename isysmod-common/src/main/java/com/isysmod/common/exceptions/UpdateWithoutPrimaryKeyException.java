package com.isysmod.common.exceptions;

public class UpdateWithoutPrimaryKeyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7271880592786859924L;

	public UpdateWithoutPrimaryKeyException() {
		super( "Trying to update without a primary key." );
	}

}
