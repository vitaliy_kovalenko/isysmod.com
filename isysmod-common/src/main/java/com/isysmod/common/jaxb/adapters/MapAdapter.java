package com.isysmod.common.jaxb.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.isysmod.common.model.EntityBean;

public class MapAdapter extends XmlAdapter<MapAdapter.MapWrapper, Map<String, Object>> {

	private DocumentBuilder documentBuilder;

	public MapAdapter() throws ParserConfigurationException, FactoryConfigurationError {
		documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "map")
	public static class MapWrapper extends EntityBean {
		@XmlAnyElement
		public List<Element> elements = new ArrayList<Element>();
	}

	@Override
	public MapAdapter.MapWrapper marshal(Map<String, Object> map) throws Exception {
		if (map == null) {
			return null;
		}
		
		Document document = documentBuilder.newDocument();
		MapWrapper adaptedMap = new MapWrapper();
		
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			Element element = document.createElement(entry.getKey());
			
			if (entry.getValue() == null) {
				element.setAttribute("xsi:nil", "true");
			} else {
				element.appendChild(document.createTextNode( String.valueOf( entry.getValue() ) ));
			}
			
			adaptedMap.elements.add(element);
		}
		
		return adaptedMap;
	}

	@Override
	public Map<String, Object> unmarshal(MapWrapper adaptedMap) throws Exception {
		if (adaptedMap == null) {
			return null;
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		for (Element element : adaptedMap.elements) {
			map.put(element.getLocalName(), element.getFirstChild().getNodeValue());
		}
		
		return map;
	}

}
