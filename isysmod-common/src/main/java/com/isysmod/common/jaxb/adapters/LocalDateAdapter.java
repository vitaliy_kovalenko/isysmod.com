package com.isysmod.common.jaxb.adapters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public LocalDate unmarshal(String v) throws Exception {
		return v == null ? null : LocalDate.parse(v);
	}

	@Override
	public String marshal(LocalDate v) throws Exception {
        return v == null ? null : v.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}

}
