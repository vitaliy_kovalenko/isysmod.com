package com.isysmod.common.jaxb.adapters.isc;

import java.util.ArrayList;
import java.util.List;

import javax.naming.OperationNotSupportedException;
import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class IscCriterionValueAdapter extends XmlAdapter<Object, Object> {

	@Override
	public Object unmarshal(Object v) throws Exception {
		Element element = (Element) v;
		
		if (!element.hasChildNodes()) {
			return null;
		}
		
		if (element.getFirstChild().getNodeType() == Element.TEXT_NODE) {
			return element.getFirstChild().getNodeValue();
		}
		
		NodeList nodeList = element.getChildNodes();
		List<String> list = new ArrayList<>();
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			
			if (node.hasChildNodes() && node.getFirstChild().getNodeType() == Element.TEXT_NODE) {
				list.add(node.getFirstChild().getNodeValue());
			}
		}
		
		return list;
	}

	@Override
	public Object marshal(Object v) throws Exception {
		throw new OperationNotSupportedException(); // TODO: if need...
	}

}
