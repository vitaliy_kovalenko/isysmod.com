package com.isysmod.common.jaxb.adapters.isc;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.isysmod.common.jaxb.adapters.MapAdapter;
import com.isysmod.common.jaxb.adapters.MapAdapter.MapWrapper;
import com.isysmod.common.jaxb.isc.IscRequestData;

public class IscRequestDadaAdapter<E> extends XmlAdapter<E, E> {

	private static class IscNodeList implements NodeList {
		private Node[] nodes;
		
		IscNodeList(List<Element> elements) {
			Node[] nodes = new Node[elements.size()];
			int length = 0;
			
			for (Element element : elements) {
				element.normalize();
				
				switch (element.getNodeType()) {
					case Node.ELEMENT_NODE:
					case Node.TEXT_NODE:
					case Node.ATTRIBUTE_NODE:
						nodes[length] = (Node) element;
						length ++;
					break;
				}
			}
			
			this.nodes = new Node[length];
			System.arraycopy(nodes, 0, this.nodes, 0, length);
		}
		
		@Override
		public Node item(int index) {
			return nodes[index];
		}
		
		@Override
		public int getLength() {
			return nodes.length;
		}
	}
	
	private DocumentBuilder documentBuilder;
	
	public IscRequestDadaAdapter() throws ParserConfigurationException, FactoryConfigurationError {
		documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	}

	@Override
	@SuppressWarnings("unchecked")
	public E marshal(E v) throws Exception {
		if (v instanceof IscRequestData) {
			IscRequestData<String, Object> map = (IscRequestData<String, Object>) v;
			Document document = documentBuilder.newDocument();
			MapWrapper adaptedMap = new MapWrapper();
			
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				Element element = document.createElement(entry.getKey());
				
				if (entry.getValue() == null) {
					element.setAttribute("xsi:nil", "true");
				} else {
					element.appendChild(document.createTextNode( String.valueOf( entry.getValue() ) ));
				}

				adaptedMap.elements.add(element);
			}
			
			return (E) adaptedMap;
		}
		
		return v;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public E unmarshal(E v) throws Exception {
		if (v instanceof MapAdapter.MapWrapper) {
			final MapWrapper mapWrapper = (MapWrapper) v;
			IscRequestData<String, Object> map = null;
			NodeList nodeList = new IscNodeList(mapWrapper.elements);
			
			if (nodeList.getLength() > 0) {
				map = performNodeListParsing( nodeList );
			}
			
			return (E) map;
		}
		
		return v;
	}
	
	private Object getNodeValue(final Node node) {
		if (node.hasAttributes() && node.getAttributes().getNamedItem("xsi:nil") != null) {
			return null;
		} else if (node.getNodeType() == Node.ELEMENT_NODE) {
			if (node.getFirstChild().getNodeType() == Node.TEXT_NODE) {
				return getNodeValue(node.getFirstChild());
			}
			
			return performNodeListParsing(node.getChildNodes());
		} else {
			String value = node.getNodeValue().trim();
			return value.length() > 0 ? value : null;
		}
	}
	
	private IscRequestData<String, Object> performNodeListParsing(NodeList nodeList) {
		IscRequestData<String, Object> map = new IscRequestData<String, Object>();
		
		for (int i = 0; i < nodeList.getLength(); i++) {
			final Node node = nodeList.item(i);

			if (node.getNodeType() != Node.TEXT_NODE) {
				map.put(node.getLocalName(), getNodeValue( node ));
			}
		}
		
		return map;
	}
	
}
