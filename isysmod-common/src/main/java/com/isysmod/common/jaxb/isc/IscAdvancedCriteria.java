package com.isysmod.common.jaxb.isc;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import com.isysmod.common.model.EntityBean;

@XmlType(name = "AdvancedCriteria")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscAdvancedCriteria extends EntityBean {

	/***
	 * "and"/"or"/"not"
	 */
	@XmlElement
	private String operator;
	
	@XmlElementWrapper(name = "criteria")
	@XmlElement(name = "elem")
	private List<EntityBean> elem;

	public String getOperator() {
		return operator;
	}

	public List<EntityBean> getElements() {
		return elem;
	}
	
	public void addCriterion(IscCriterion criterion) {
		if (this.elem == null) {
			this.elem = new ArrayList<>();
		}
		
		this.elem.add(criterion);
	}

	@Override
	public String toString() {
		return "{operator=" + String.valueOf(this.operator) +
				",elem=" + String.valueOf(this.elem) + "}";
	}
}
