package com.isysmod.common.jaxb.adapters;

public enum JoinRole {
	
	SELECT, FILTER, ALL
	
}
