package com.isysmod.common.jaxb.adapters;

import java.util.List;

import com.isysmod.common.jaxb.isc.JoinedField;

public class JoinListener extends AbstractJoinListener {
	
	private JoinDetermination determination;

	public JoinListener(Package p, List<JoinedField> joins) {
		super(p, joins);
	}

	@Override
    public void beforeMarshal(Object source) {
    	if (isEntityOrCollectionOfEntity(source)) {
    		if (this.depth == 0) {
    			cloneGraph();
    		}
    		
    		this.stack.add(source);
    		MapKey key = new MapKey(source.getClass(), this.depth);
    		
    		if (this.graphCopy.containsKey(key)) {
    			determination = this.graphCopy.get(key);
    		} else if (determination != null) { // Bug CXF contains/remove
    			determination = null;
    		}
    		
    		this.depth ++;
    	}
    }
    
    @Override
    public void afterMarshal(Object source) {
    	if (isEntityOrCollectionOfEntity(source) && this.depth > 0) {
    		this.depth --;
    		source = previousFromStack();
    		
    		if (source != null && this.depth > 0) {
        		Class<?> ownerClass = source.getClass();
        		MapKey key = new MapKey(ownerClass, this.depth - 1);
	    		
	    		if (this.graphCopy.containsKey(key)) {
	    			determination = this.graphCopy.get(key);
	    		}
    		}
    	}
    }

    public JoinDetermination getDetermination() {
		return determination;
	}

}
