package com.isysmod.common.jaxb.isc;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.isysmod.common.model.EntityBean;

@XmlType(name = "PersistentCollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class IsysPersistentCollection<E extends EntityBean> {

	@XmlElement(name = "record")
	private Collection<E> collection;

	public IsysPersistentCollection() {
	}
	
	public IsysPersistentCollection(Collection<E> collection) {
		this.collection = collection;
	}

	public Collection<E> getCollection() {
		return collection;
	}

	public void setCollection(Collection<E> collection) {
		this.collection = collection;
	}

}
