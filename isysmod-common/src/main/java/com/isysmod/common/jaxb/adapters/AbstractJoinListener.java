package com.isysmod.common.jaxb.adapters;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.xml.bind.Marshaller;

import com.isysmod.common.jaxb.isc.IsysProxy;
import com.isysmod.common.jaxb.isc.JoinedField;
import com.isysmod.common.model.EntityBean;

public class AbstractJoinListener extends Marshaller.Listener {

	protected int depth = 0;
	
	protected Package p;
    
	protected Map<MapKey, JoinDetermination> graph;
    
	protected Map<MapKey, JoinDetermination> graphCopy;
    
	protected List<Object> stack;

    public AbstractJoinListener(Package p, List<JoinedField> joins) {
		this.p = p;
		this.graph = new HashMap<>(1);
		this.stack = new ArrayList<>(1);
		
		try {
			Map<Class<?>, JoinDetermination> map = vertex(joins);
			
			if (map != null && map.size() > 0) {
				Map.Entry<Class<?>, JoinDetermination> entry = map.entrySet().iterator().next();
				MapKey key = new MapKey(entry.getKey(), this.depth);
				this.graph.put(key, entry.getValue());
			}
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException e) {
			throw new WebApplicationException(e);
		}
	}
    
    protected boolean children(List<JoinedField> joins) throws ClassNotFoundException, NoSuchFieldException, SecurityException {
    	if (joins == null || joins.size() < 1) {
    		return false;
    	}

    	this.depth ++;
    	Map<Class<?>, JoinDetermination> map = vertex(joins);
    	
    	if (map != null && map.size() > 0) {
    		Map.Entry<Class<?>, JoinDetermination> entry = map.entrySet().iterator().next();
			MapKey key = new MapKey(entry.getKey(), this.depth);
			this.graph.put(key, entry.getValue());
		}
    	
    	return true;
    }
    
    protected Map<Class<?>, JoinDetermination> vertex(List<JoinedField> joins) throws ClassNotFoundException, NoSuchFieldException, SecurityException {
    	if (joins == null || joins.size() < 1) {
    		return null;
    	}
    	
    	Map<Class<?>, JoinDetermination> map = new HashMap<>(1);
    	JoinDetermination determination;
    	
    	for (JoinedField jaxbField : joins) {
    		Class<?> ownerClass = jaxbField.getOwnerClass(this.p);
    		
    		if (! map.containsKey(ownerClass)) {
    			determination = new JoinDetermination();
    			map.put(ownerClass, determination);
    		} else {
    			determination = map.get(ownerClass);
    		}
    		
    		node(jaxbField, determination);
    		
    		if (children(jaxbField.getJoins())) {
    			this.depth --;
    		}
    	}
    	
    	return map;
    }
    
    protected void node(JoinedField jaxbField, JoinDetermination determination) throws ClassNotFoundException, NoSuchFieldException, SecurityException {
		Map<Class<?>, List<String>> map = getFieldsForClassSomeAsFieldClass(jaxbField);
		Map.Entry<Class<?>, List<String>> entry = map.entrySet().iterator().next();
		Class<?> entityClass = entry.getKey();
		List<String> fieldNames = entry.getValue();
		
		for (String fn : fieldNames) {
			Boolean wanted = fn.equals(jaxbField.getFieldName()) && jaxbField.isSelectable();
			
			if (wanted) {
				determination.addWanted(entityClass.getName());
			} else {
				determination.addUnwanted(entityClass.getName());
			}
		}
    }

    protected Map<Class<?>, List<String>> getFieldsForClassSomeAsFieldClass(JoinedField jaxbField) throws NoSuchFieldException, SecurityException, ClassNotFoundException {
		List<String> fieldNames = new ArrayList<>();
		Class<?> ownerClass = jaxbField.getOwnerClass(this.p);
		Class<?> entityClass = jaxbField.getEntityClass(this.p);
		
		for (Field f : ownerClass.getDeclaredFields()) {
			if (f.getGenericType() instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) f.getGenericType();
				Type[] types = parameterizedType.getActualTypeArguments();
				
				if (types.length > 0) {
					Class<?> collectionParameterClass = (Class<?>) types[0];
					
					if (collectionParameterClass.equals(entityClass)) {
						fieldNames.add(f.getName());
					}
				}
			} else if (f.getType().equals(entityClass)) {
				fieldNames.add(f.getName());
			}
		}
		
		Map<Class<?>, List<String>> map = new HashMap<>(1);
		map.put(entityClass, fieldNames);
		
		return map;
	}
	
	protected boolean isEntityOrCollectionOfEntity(Object source) {
		return (source instanceof EntityBean && !(source instanceof IsysProxy)) || source instanceof Collection<?>;
	}

	protected Object previousFromStack() {
		int lastIndex = stack.size() - 1;
		
		if (lastIndex < 0) {
			return null;
		}
		
		stack.remove(lastIndex);
		
		if (stack.size() < 1) {
			return null;
		}
		
		return stack.get(lastIndex - 1);
	}
	
	protected void cloneGraph() {
		this.graphCopy = new HashMap<>(this.graph.size());
		
		for (Map.Entry<MapKey, JoinDetermination> entry : this.graph.entrySet()) {
			this.graphCopy.put(entry.getKey(), entry.getValue().clone());
		}
	}
	
	public class MapKey {
		
		Class<?> ownerClass;
		
		int depth = 0;
		
		public MapKey(Class<?> ownerClass, int depth) {
			this.ownerClass = ownerClass;
			this.depth = depth;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + depth;
			result = prime * result + ((ownerClass == null) ? 0 : ownerClass.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MapKey other = (MapKey) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (depth != other.depth)
				return false;
			if (ownerClass == null) {
				if (other.ownerClass != null)
					return false;
			} else if (!ownerClass.getName().equals(other.ownerClass.getName()))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return ownerClass.getName() + "#" + depth;
		}

		private AbstractJoinListener getOuterType() {
			return AbstractJoinListener.this;
		}
	}
	
}
