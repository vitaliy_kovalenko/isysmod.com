package com.isysmod.common.jaxb.adapters;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {

	@Override
	public LocalTime unmarshal(String v) throws Exception {
		return v == null ? null : LocalTime.parse(v);
	}

	@Override
	public String marshal(LocalTime v) throws Exception {
		return v == null ? null : v.format(DateTimeFormatter.ISO_LOCAL_TIME);
	}

}
