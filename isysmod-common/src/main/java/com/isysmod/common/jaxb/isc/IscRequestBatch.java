package com.isysmod.common.jaxb.isc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscRequestBatch<E> implements IscRequest {

	@XmlAttribute
	private Boolean transactional = Boolean.FALSE;
	
	@XmlElementWrapper(name = "operations")
	@XmlElement(name = "request")
	private List<IscSingleRequest<E>> requests;

	public List<IscSingleRequest<E>> getRequests() {
		return requests;
	}

	public boolean isTransactional() {
		return transactional;
	}

	@Override
	public String toString() {
		return "IscRequestBatch [transactional=" + transactional + ", requests=" + requests + "]";
	}
	
}
