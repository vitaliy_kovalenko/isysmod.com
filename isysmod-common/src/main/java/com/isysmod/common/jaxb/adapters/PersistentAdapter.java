package com.isysmod.common.jaxb.adapters;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;

import com.isysmod.common.jaxb.isc.IsysProxy;
import com.isysmod.common.model.EntityBean;

public class PersistentAdapter<E extends EntityBean> extends JoinXmlAdapter<E, E> {

	public PersistentAdapter() {}
	
	public PersistentAdapter(JoinListener joinListener) {
		super(joinListener);
	}

	@Override
	public E unmarshal(E v) throws Exception {
		return v;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E marshal(E v) throws Exception {
		if (v == null) {
			return v;
		}
		
		Object result = new IsysProxy();
		JoinDetermination determination;
		
		if (joinListener != null) {
			determination = joinListener.getDetermination();
		} else {
			Message message = PhaseInterceptorChain.getCurrentMessage();
			determination =  message.get(JoinDetermination.class);
		}
    	
		if (determination != null) {
			String entityName;
			
	    	if (v instanceof HibernateProxy) {
	    		HibernateProxy proxy = (HibernateProxy) v;
				LazyInitializer initializer = proxy.getHibernateLazyInitializer();
				entityName = initializer.getEntityName();
				
				if (determination.isWanted(entityName)) {
					result = initializer.getImplementation();
				}
	    	} else {
	    		entityName = v.getClass().getCanonicalName();
	    		
	    		if (determination.isWanted(entityName)) {
					result = v;
				}
	    	}
	    	
	    	determination.remove(entityName);
		}
		
		return (E) result;
	}

}
