package com.isysmod.common.jaxb.isc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.IC;
import com.isysmod.common.jaxb.adapters.isc.IscCriterionValueAdapter;
import com.isysmod.common.model.EntityBean;

@XmlType(name = "Criterion")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscCriterion extends EntityBean {

	private String fieldName;
	
	private String operator;
	
	@XmlJavaTypeAdapter(IscCriterionValueAdapter.class)
	private Object value;
	
	private String start;
	
	private String end;
	
	private String relationBean;
	
	private String selfKey;
	
	private String foreignKey;

	public String getFieldName() {
		return fieldName;
	}

	public String getOperator() {
		return operator;
	}

	/**
	 * 
	 * @return List<String> | String
	 */
	@SuppressWarnings("unchecked")
	public Object getValue() {
		if (value == null) {
			return null;
		} else if (value instanceof List) {
			return (List<String>) value;
		}
		
		return (String) value;
	}
	
	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public String getRelationBean() {
		return relationBean;
	}

	public String getSelfKey() {
		return selfKey;
	}

	public String getForeignKey() {
		return foreignKey;
	}

	@Override
	public String toString() {
		return "IscCriterion [fieldName=" + fieldName + ", operator=" + operator + ", value=" + value + ", start="
				+ start + ", end=" + end + ", relationBean=" + relationBean + ", selfKey=" + selfKey + ", foreignKey="
				+ foreignKey + "]";
	}

	public boolean isExtended() {
		switch (this.operator) {
		case IC.TEXT_MATCH_NOTEXISTS:
			return true;
		}
		
		return false;
	}

	
}
