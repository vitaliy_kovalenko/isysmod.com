package com.isysmod.common.jaxb.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public abstract class JoinXmlAdapter<ValueType,BoundType> extends XmlAdapter<ValueType,BoundType> {

	protected JoinListener joinListener;
	
	public JoinXmlAdapter() {}
	
	public JoinXmlAdapter(JoinListener joinListener) {
		this.joinListener = joinListener;
	}

}
