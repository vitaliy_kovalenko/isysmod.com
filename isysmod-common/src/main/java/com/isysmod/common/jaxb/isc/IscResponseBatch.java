package com.isysmod.common.jaxb.isc;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.isysmod.common.model.EntityBean;

@XmlRootElement(name = "responses")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscResponseBatch<T extends EntityBean> implements IscResponse {
	
	@XmlElement(name = "response")
	private List<IscSingleResponse<T>> responses = new ArrayList<>();

	public List<IscSingleResponse<T>> getResponses() {
		return responses;
	}

	public void setResponses(List<IscSingleResponse<T>> responses) {
		this.responses = responses;
	}

	public void add(IscSingleResponse<T> iscResponse) {
		responses.add(iscResponse);
	}
	
}
