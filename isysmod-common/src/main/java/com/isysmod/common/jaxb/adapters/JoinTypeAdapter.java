package com.isysmod.common.jaxb.adapters;

import javax.persistence.criteria.JoinType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JoinTypeAdapter extends XmlAdapter<String, JoinType> {

	@Override
	public JoinType unmarshal(String v) throws Exception {
		if (v != null) {
			switch (v) {
				case "inner":
					return JoinType.INNER;
				case "outer":
					return JoinType.LEFT;
			}
		}
		
		return JoinType.LEFT;
	}

	@Override
	public String marshal(JoinType v) throws Exception {
		if (v != null) {
			switch (v) {
				case INNER:
					return "inner";
				case LEFT:
				case RIGHT:
					return "outer";
			}
		}
		
		return "outer";
	}

}
