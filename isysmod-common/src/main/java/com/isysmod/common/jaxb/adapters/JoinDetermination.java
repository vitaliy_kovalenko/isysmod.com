package com.isysmod.common.jaxb.adapters;

import java.util.ArrayList;
import java.util.List;

public class JoinDetermination implements Cloneable {

	private List<Item> queue;

	public JoinDetermination() {
		queue = new ArrayList<>();
	}

	public void addWanted(String entityName) {
		this.queue.add(new Item(entityName, true));
	}
	
	public void addUnwanted(String entityName) {
		this.queue.add(new Item(entityName, false));
	}

	public boolean isWanted(String entityName) {
		Item item = getFirst(entityName);
		
		return item != null && item.wanted == true;
	}
	
	public void remove(String entityName) {
		Item item = getFirst(entityName);
		
		if (item != null) {
			this.queue.remove(item);
		}
	}
	
	public JoinDetermination clone() {
		JoinDetermination determination = new JoinDetermination();
		
		for (Item item : this.queue) {
			determination.queue.add(item.clone());
		}
		
		return determination;
	}
	
	private Item getFirst(String entityName) {
		if (entityName != null) {
			for (Item item : queue) {
				if (entityName.equals(item.entityName)) {
					return item;
				}
			}
		}
		
		return null;
	}
	
	private class Item implements Cloneable {
		String entityName;
		Boolean wanted;

		public Item(String entityName, Boolean wanted) {
			this.entityName = entityName;
			this.wanted = wanted;
		}
		
		public Item clone() {
			return new Item(this.entityName, this.wanted);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((entityName == null) ? 0 : entityName.hashCode());
			result = prime * result + ((wanted == null) ? 0 : wanted.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Item other = (Item) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (entityName == null) {
				if (other.entityName != null)
					return false;
			} else if (!entityName.equals(other.entityName))
				return false;
			if (wanted == null) {
				if (other.wanted != null)
					return false;
			} else if (!wanted.equals(other.wanted))
				return false;
			return true;
		}

		private JoinDetermination getOuterType() {
			return JoinDetermination.this;
		}

	}

}
