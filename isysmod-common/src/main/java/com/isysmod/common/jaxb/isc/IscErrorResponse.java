package com.isysmod.common.jaxb.isc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "errorResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscErrorResponse {

	private Integer status;
	
	private String data;
	
	public IscErrorResponse() {
	}
	
	public IscErrorResponse(Integer status, String errorMessage) {
		this.status = status;
		this.data = errorMessage;
	}

	public Integer getStatus() {
		return status;
	}

	public Object getData() {
		return data;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setData(String data) {
		this.data = data;
	}

}
