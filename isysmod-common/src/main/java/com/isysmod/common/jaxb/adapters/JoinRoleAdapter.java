package com.isysmod.common.jaxb.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JoinRoleAdapter extends XmlAdapter<String, JoinRole> {

	@Override
	public JoinRole unmarshal(String v) throws Exception {
		if (v != null) {
			switch (v) {
				case "select":
					return JoinRole.SELECT;
				case "filter":
					return JoinRole.FILTER;
				case "all":
					return JoinRole.ALL;
			}
		}
		
		return JoinRole.FILTER;
	}

	@Override
	public String marshal(JoinRole v) throws Exception {
		if (v != null) {
			switch (v) {
				case ALL:
					return "all";
				case FILTER:
					return "filter";
				case SELECT:
					return "select";
			}
		}
		
		return "filter";
	}

}
