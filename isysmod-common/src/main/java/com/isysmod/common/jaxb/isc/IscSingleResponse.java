package com.isysmod.common.jaxb.isc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.dao.QueryResult;
import com.isysmod.common.jaxb.adapters.MapAdapter;
import com.isysmod.common.model.EntityBean;

@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscSingleResponse<E extends EntityBean> implements IscResponse {
	
	private Integer status;
	
	private Integer startRow;
	
	private Integer endRow;
	
	private Long totalRows;
	
	@XmlElementWrapper(name = "data")
	@XmlElement(name = "record")
	private List<E> data;
	
	@XmlJavaTypeAdapter(MapAdapter.class)
	private Map<String,String> errors;

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void implementQueryResult(QueryResult<E> queryResult) {
		this.startRow = queryResult.getStartRow();
		this.endRow = queryResult.getEndRow();
		this.totalRows = queryResult.getTotalRows();
		this.data = queryResult.getData();
	}
	
	public Integer getStatus() {
		return status;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public Long getTotalRows() {
		return totalRows;
	}

	public List<E> getData() {
		return data;
	}

	public void setErrorFields(Set<ConstraintViolation<?>> errorFields) {
		if (errorFields != null && !errorFields.isEmpty()) {
			errors = new HashMap<String,String>();
			
			for (ConstraintViolation<?> v : errorFields) {
				errors.put(v.getPropertyPath().toString(), v.getMessage());
			}
		}
	}

}
