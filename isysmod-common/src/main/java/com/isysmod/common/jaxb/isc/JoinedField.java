package com.isysmod.common.jaxb.isc;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.JoinType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.JoinTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class JoinedField {

	/**
	 * Entity class name which contains this field.
	 */
	private String owner;
	
	/**
	 * The name of this field in the entity class.
	 */
	private String field;
	
	/**
	 * If this field joins for filtering only, not need to select related entity.
	 */
	private Boolean select = Boolean.FALSE;
	
	/**
	 * @see javax.persistence.criteria.JoinType
	 */
	@XmlJavaTypeAdapter(JoinTypeAdapter.class)
	private JoinType type = JoinType.LEFT;
	
	@XmlElementWrapper(name = "joins")
	@XmlElement(name = "join")
	protected List<JoinedField> joins;
	
	@XmlTransient
	private Class<?> ownerClass;
	
	public JoinedField() {
	}
	
	public JoinedField(Class<?> ownerClass, String fieldName, Boolean select) {
		this.ownerClass = ownerClass;
		this.field = fieldName;
		this.select = select;
	}

	public String getOwner() {
		return owner;
	}

	public String getFieldName() {
		return field;
	}

	public List<JoinedField> getJoins() {
		return joins;
	}
	
	public JoinedField addJoin(JoinedField joinedField) {
		if (this.joins == null) {
			this.joins = new ArrayList<>();
		}
		
		this.joins.add(joinedField);
		
		return this;
	}
	
	public Boolean isSelectable() {
		return select;
	}

	public JoinType getType() {
		return type;
	}
	
	public void setOwnerClass(Class<?> ownerClass) {
		this.ownerClass = ownerClass;
	}
	
	public void setFieldName(String field) {
		this.field = field;
	}

	public Class<?> getOwnerClass(Package p) throws ClassNotFoundException {
		if (this.ownerClass == null) {
			this.ownerClass = Class.forName( p.getName() + "." + this.owner );
		}
		
		return this.ownerClass;
	}

	public Class<?> getEntityClass(Package p) throws NoSuchFieldException, SecurityException, ClassNotFoundException {
		Field field = getOwnerClass(p).getDeclaredField(this.field);
		Class<?> entityClass = field.getType();
		
		if (Collection.class.isAssignableFrom(entityClass)) {
			ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
    		Type[] types = parameterizedType.getActualTypeArguments();
    		entityClass = (Class<?>) types[0];
		}
		
		return entityClass;
	}
	
	public boolean isCollection(Package p) throws NoSuchFieldException, SecurityException, ClassNotFoundException {
		Field field = getOwnerClass(p).getDeclaredField(this.field);
		Class<?> entityClass = field.getType();
		
		return Collection.class.isAssignableFrom(entityClass);
	}

}
