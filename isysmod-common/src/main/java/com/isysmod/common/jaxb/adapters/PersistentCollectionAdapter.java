package com.isysmod.common.jaxb.adapters;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.hibernate.collection.spi.PersistentCollection;

import com.isysmod.common.jaxb.isc.IsysPersistentCollection;
import com.isysmod.common.jaxb.isc.IsysProxy;
import com.isysmod.common.model.EntityBean;

public class PersistentCollectionAdapter<E extends EntityBean> extends JoinXmlAdapter<Object, Collection<E>> {
	
	public PersistentCollectionAdapter() {}
	
	public PersistentCollectionAdapter(JoinListener joinListener) {
		super(joinListener);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<E> unmarshal(Object v) throws Exception {
		if (v instanceof IsysPersistentCollection) {
			return ((IsysPersistentCollection<E>) v).getCollection();
		}
		
		return (Collection<E>) v;
	}

	@Override
	public Object marshal(Collection<E> v) throws Exception {
		if (v != null && v instanceof PersistentCollection) {
			Object result = new IsysProxy();
			JoinDetermination determination;
			
			if (joinListener != null) {
				determination = joinListener.getDetermination();
			} else {
				Message message = PhaseInterceptorChain.getCurrentMessage();
				determination =  message.get(JoinDetermination.class);
			}

	    	if (determination != null) {
	    		PersistentCollection collection = (PersistentCollection) v;
	    		String role = collection.getRole();
	    		Class<?> ownerClass = collection.getOwner().getClass();
	    		String fieldName = role.substring(role.lastIndexOf('.') + 1);
	    		Field field = ownerClass.getDeclaredField(fieldName);
	    		ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
	    		Type[] types = parameterizedType.getActualTypeArguments();
	    		Class<?> entityClass = (Class<?>) types[0];
	    		String entityName = entityClass.getName();
	    		
				if (determination.isWanted(entityName)) {
					result = new IsysPersistentCollection<E>( v );
		    	}
				
				determination.remove(entityName);
	    	}
	    	
	    	return result;
		}
		
		return null;
	}

}
