package com.isysmod.common.jaxb.isc;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.isysmod.common.jaxb.adapters.isc.IscRequestDadaAdapter;

@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class IscSingleRequest<E> implements IscRequest {

	/***
	 * Possible types:
	 * 1. Entity - add/remove/full update
	 * 2. IscRequestData (Map) - fetch/partial update
	 * 3. IscAdvancedCriteria - fetch (enhanced filtering)
	 * 4. RelativeDate (planned)
	 */
	@XmlJavaTypeAdapter(IscRequestDadaAdapter.class)
	private E data;
	
	private String database;
	
	private String beanClassName;
	
	private String operationType;
	
	private String operationId;
	
	private Integer startRow = 0;
	
	private Integer endRow = 75;
	
	private String textMatchStyle;
	
	private String[] sortBy;
	
	private String[] groupBy;
	
	@XmlElementWrapper(name = "joins")
	@XmlElement(name = "join")
	protected List<JoinedField> joins;

	@SuppressWarnings("unchecked")
	public Map<String,Object> getRequestData() {
		if (data instanceof IscRequestData) {
			return (Map<String,Object>) data;
		}
		
		return null;
	}
	
	public IscAdvancedCriteria getAdvancedCriteria() {
		if (data instanceof IscAdvancedCriteria) {
			return (IscAdvancedCriteria) data;
		}
		
		return null;
	}
	
	public E getEntity() {
		if (
				data instanceof IscRequestData ||
				data instanceof IscAdvancedCriteria
			) {
			return null;
		}
		
		return data;
	}
	
	// for testing
	public void setData(E data) {
		this.data = data;
	}
	
	public E getData() {
		return data;
	}
	
	public String getDatabase() {
		return database;
	}
	
	public String getBeanClassName() {
		return beanClassName;
	}

	public String getOperationType() {
		return operationType;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public Integer getEndRow() {
		return endRow;
	}

	public String getTextMatchStyle() {
		return textMatchStyle;
	}

	public String[] getSortBy() {
		return sortBy;
	}

	public String[] getGroupBy() {
		return groupBy;
	}

	public String getOperationId() {
		return operationId;
	}

	public List<JoinedField> getJoins() {
		return joins;
	}

}
