package com.isysmod.modeling.dao;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import com.isysmod.common.IC;
import com.isysmod.common.IsysUtils;
import com.isysmod.common.dao.ExtendedExpressionBuilder;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.jaxb.isc.IscCriterion;

public class ExtendedExpressionBuilderImpl implements ExtendedExpressionBuilder {

	@Override
	public Criterion buildExpression(IscCriterion iscCriterion, String rootAlias) {
		Criterion criterion = null;
		String operator = iscCriterion.getOperator();
		Object value = iscCriterion.getValue();
		
		switch (operator) {
		case IC.TEXT_MATCH_NOTEXISTS:
			String relationBean = iscCriterion.getRelationBean();
			String selfKey = iscCriterion.getSelfKey();
			String foreignKey = iscCriterion.getForeignKey();
			
			if (
					relationBean == null ||
					selfKey == null ||
					foreignKey == null) {
				throw new IsysException("Incorrect criteria.");
			}
			
			String relationBeanAlias = IsysUtils.entityAlias(relationBean);
			
			DetachedCriteria existsCriteria = DetachedCriteria.forEntityName(IC.ISC_COMMON_MODEL_PACKAGE + "." + relationBean, relationBeanAlias);
			existsCriteria.add(Restrictions.conjunction()
					.add(Property.forName(relationBeanAlias + "." + foreignKey).eqProperty(rootAlias + ".id"))
					.add(Restrictions.eq(relationBeanAlias + "." + selfKey, (String) value)));
			criterion = Subqueries.notExists(existsCriteria.setProjection(Projections.property(relationBeanAlias + ".id")));
			//criterion = Restrictions.sqlRestriction("not exists (select id from client_datasource where client_datasource.client_id = {alias}.id and client_datasource.datasource_id = '" + value + "')");
			break;

		default:
			break;
		}
		
		return criterion;
	}

}
