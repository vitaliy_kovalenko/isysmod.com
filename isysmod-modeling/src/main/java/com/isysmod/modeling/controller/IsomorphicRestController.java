package com.isysmod.modeling.controller;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.isysmod.common.IC;
import com.isysmod.common.dao.ExtendedExpressionBuilder;
import com.isysmod.common.dao.HibernateDAO;
import com.isysmod.common.dao.QueryParams;
import com.isysmod.common.dao.QueryResult;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.jaxb.adapters.JoinListener;
import com.isysmod.common.jaxb.adapters.PersistentAdapter;
import com.isysmod.common.jaxb.adapters.PersistentCollectionAdapter;
import com.isysmod.common.jaxb.isc.IscRequest;
import com.isysmod.common.jaxb.isc.IscRequestBatch;
import com.isysmod.common.jaxb.isc.IscResponse;
import com.isysmod.common.jaxb.isc.IscResponseBatch;
import com.isysmod.common.jaxb.isc.IscSingleRequest;
import com.isysmod.common.jaxb.isc.IscSingleResponse;
import com.isysmod.common.model.EntityBean;

@Controller
@RequestMapping(value = "/isc")
public class IsomorphicRestController<E extends EntityBean> {
	
	private static class IsysSchemaOutputResolver extends SchemaOutputResolver {
		
		String filePath;
		
		@Override
		public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
			File file = new File(suggestedFileName);
	        StreamResult result = new StreamResult(file);
	        filePath = file.toURI().toURL().toString();
	        result.setSystemId(filePath);
	        
	        return result;
		}

	}
	
	private final Logger LOGGER = Logger.getLogger(IsomorphicRestController.class.getName());
	
	private ExtendedExpressionBuilder expressionBuilder;
	
	private HibernateDAO<E> hibernateDAO;
	
	private JAXBContext jaxbContext;
	
	public IsomorphicRestController() throws JAXBException {
		jaxbContext = JAXBContext.newInstance(IC.ISC_COMMON_JAXB_PACKAGE + ":" + IC.ISC_COMMON_MODEL_PACKAGE);
	}

	@RequestMapping(
			value = "/xjc",
			method = RequestMethod.GET)
	public @ResponseBody String xjc() throws IOException {
		IsysSchemaOutputResolver resolver = new IsysSchemaOutputResolver();
		jaxbContext.generateSchema(resolver);
		
		return resolver.filePath;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(
			value = "/debug",
			method = RequestMethod.POST,
			consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.TEXT_XML_VALUE+";charset=UTF-8", MediaType.APPLICATION_JSON_UTF8_VALUE})
	public @ResponseBody String debug(
			@RequestHeader(HttpHeaders.CONTENT_TYPE) String contentType,
			@RequestBody String payload)
			throws JAXBException, NoSuchFieldException, SecurityException, ParseException {
		LOGGER.debug("Request (" + contentType + ") received:\n" + payload);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		
		if (contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE);
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE);
		}
		
		IscRequest request = (IscRequest) unmarshaller.unmarshal( new StringReader( payload ) );
		//System.out.println(request);
		IscResponse response;

		if (request instanceof IscRequestBatch) {
			IscRequestBatch<E> requestBatch = (IscRequestBatch<E>) request;
			response = new IscResponseBatch<>();
			
			for (IscSingleRequest<E> iscRequest : requestBatch.getRequests()) {
				IscSingleResponse<E> iscResponse = single( iscRequest, marshaller );
				((IscResponseBatch<E>) response).add(iscResponse);
			}

		} else if (request instanceof IscSingleRequest) {
			IscSingleRequest<E> iscRequest = (IscSingleRequest<E>) request;
			response = single( iscRequest, marshaller );
		} else {
			throw new IsysException("Unknown request.");
		}

		Writer writer = new StringWriter();
		
		if (request instanceof IscRequestBatch && contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			marshaller.marshal(((IscResponseBatch<E>) response).getResponses(), writer);
		} else {
			marshaller.marshal(response, writer);
		}
		
		String result = writer.toString();
		LOGGER.debug("Response created:\n" + result);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(
			method = RequestMethod.POST,
			consumes = {MediaType.TEXT_XML_VALUE, MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.TEXT_XML_VALUE+";charset=UTF-8", MediaType.APPLICATION_JSON_UTF8_VALUE})
	public void production(
			@RequestHeader(HttpHeaders.CONTENT_TYPE) String contentType,
			HttpServletRequest httpRequest,
			HttpServletResponse httpResponse)
			throws JAXBException, NoSuchFieldException, SecurityException, IOException, ParseException {	
		LOGGER.debug("Request (" + contentType + ") received.");
		
		if (contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			httpResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
		} else {
			httpResponse.setContentType(MediaType.TEXT_XML_VALUE+";charset=UTF-8");
		}
		
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
		
		if (contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE);
			marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE);
		}
		
		IscRequest request = (IscRequest) unmarshaller.unmarshal( httpRequest.getInputStream() );
		IscResponse response;
		
		if (request instanceof IscRequestBatch) {
			IscRequestBatch<E> requestBatch = (IscRequestBatch<E>) request;
			response = new IscResponseBatch<>();
			
			for (IscSingleRequest<E> iscRequest : requestBatch.getRequests()) {
				IscSingleResponse<E> iscResponse = single( iscRequest, marshaller );
				((IscResponseBatch<E>) response).add(iscResponse);
			}

		} else if (request instanceof IscSingleRequest) {
			IscSingleRequest<E> iscRequest = (IscSingleRequest<E>) request;
			response = single( iscRequest, marshaller );
		} else {
			throw new IsysException("Unknown request.");
		}
		
		if (request instanceof IscRequestBatch && contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			marshaller.marshal(((IscResponseBatch<E>) response).getResponses(), httpResponse.getOutputStream());
		} else {
			marshaller.marshal(response, httpResponse.getOutputStream());
		}
		
		LOGGER.debug("Response created.");
	}
	
	@SuppressWarnings("unchecked")
	private IscSingleResponse<E> single(IscSingleRequest<E> iscRequest, Marshaller marshaller)
			throws NoSuchFieldException, SecurityException, ParseException {
		LOGGER.debug("Request has been read.");
		String beanClassName = IC.ISC_COMMON_MODEL_PACKAGE + "." + iscRequest.getBeanClassName();
		Class<E> entityClass;
		
		try {
			entityClass = (Class<E>) Class.forName(beanClassName);
		} catch (ClassNotFoundException e) {
			throw new IsysException("Unknown entity: " + beanClassName);
		}
		
		IscSingleResponse<E> singleResponse = new IscSingleResponse<E>();
		QueryParams<E> queryParams = new QueryParams<>(entityClass, iscRequest);
		queryParams.setExpressionBuilder(this.expressionBuilder);
		queryParams.prepareParams();
		QueryResult<E> queryResult = null;
		
		JoinListener joinListener = new JoinListener(entityClass.getPackage(), iscRequest.getJoins());
		PersistentAdapter<E> persistentAdapter = new PersistentAdapter<>(joinListener);
		PersistentCollectionAdapter<E> collectionAdapter = new PersistentCollectionAdapter<>(joinListener);
		marshaller.setAdapter(PersistentAdapter.class, persistentAdapter);
		marshaller.setAdapter(PersistentCollectionAdapter.class, collectionAdapter);
		marshaller.setListener(joinListener);
		
		try {
			switch (iscRequest.getOperationType()) {
				case IC.METHOD_FETCH:
					queryResult = this.hibernateDAO.fetch(queryParams);
				break;
				case IC.METHOD_ADD:
					queryResult = this.hibernateDAO.add(queryParams);
				break;
				case IC.METHOD_UPDATE:
					queryResult = this.hibernateDAO.update(queryParams);
				break;
				case IC.METHOD_REMOVE:
					queryResult = this.hibernateDAO.remove(queryParams);
				break;
				default:
					throw new IsysException("The method should be one of: fetch, add, update or remove.");
			}
			
			LOGGER.debug("Query result recieved.");
			singleResponse.implementQueryResult(queryResult);
			singleResponse.setStatus(IC.STATUS_SUCCESS);
		} catch (ConstraintViolationException e) {
			Set<ConstraintViolation<?>> errorFields = ((ConstraintViolationException) e).getConstraintViolations();
			singleResponse.setStatus(IC.STATUS_VALIDATION_ERROR);
			singleResponse.setErrorFields(errorFields);
		}
		
		return singleResponse;
	}

	public HibernateDAO<E> getHibernateDAO() {
		return hibernateDAO;
	}

	public void setHibernateDAO(HibernateDAO<E> hibernateDAO) {
		this.hibernateDAO = hibernateDAO;
	}

	public ExtendedExpressionBuilder getExpressionBuilder() {
		return expressionBuilder;
	}

	public void setExpressionBuilder(ExtendedExpressionBuilder expressionBuilder) {
		this.expressionBuilder = expressionBuilder;
	}
	
}
