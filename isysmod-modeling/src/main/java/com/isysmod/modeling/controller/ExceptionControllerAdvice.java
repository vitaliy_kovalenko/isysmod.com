package com.isysmod.modeling.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.isysmod.common.IC;
import com.isysmod.common.exceptions.IsysException;
import com.isysmod.common.exceptions.UpdateWithoutPrimaryKeyException;

@ControllerAdvice
public class ExceptionControllerAdvice {
	
	private final Logger LOGGER = Logger.getLogger(ExceptionControllerAdvice.class.getName());

	@ExceptionHandler(value = Exception.class)     
	public ModelAndView exceptionHandler(
			Exception exception,
			HttpServletRequest request) {   
		FlashMap outputFlashMap = RequestContextUtils.getOutputFlashMap(request);
		
		if (exception instanceof HibernateException) {
			outputFlashMap.put("errorCode", IC.STATUS_TRANSACTION_FAILED);
			LOGGER.error(exception);
		} else if (exception instanceof UpdateWithoutPrimaryKeyException) {
			outputFlashMap.put("errorCode", IC.STATUS_UPDATE_WITHOUT_PK_ERROR);
			LOGGER.error(exception);
		} else if (exception instanceof AccessDeniedException) {
			outputFlashMap.put("errorCode", IC.STATUS_LOGIN_REQUIRED);
			LOGGER.warn(exception.getMessage());
		} else {
			outputFlashMap.put("errorCode", IC.STATUS_FAILURE);
			
			if (! (exception instanceof IsysException)) {
				LOGGER.error(exception.getMessage(), exception);
			}
		}
		
		outputFlashMap.put("errorMessage", exception.getClass().getSimpleName() + ": " + exception.getMessage());
		String contentType = request.getContentType();
		String q = "";
		
		if (contentType != null && contentType.startsWith(MediaType.APPLICATION_JSON_VALUE)) {
			q = "?" + IC.ISC_DATA_FORMAT_PARAMETER + "=json";
		} else {
			q = "?" + IC.ISC_DATA_FORMAT_PARAMETER + "=xml";
		}
	
		return new ModelAndView("redirect:/error/" + q);
	}
}
