package com.isysmod.modeling.controller;

import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.springframework.security.crypto.codec.Hex;

public class NoNamespaceWriter extends StringWriter {
	
	private static final NamespaceContext EMPTY_NAMESPACE_CONTEXT = new NamespaceContext() {

        @Override
        public String getNamespaceURI(String prefix) {
               return "";
        }

        @Override
        public String getPrefix(String namespaceURI) {
               return "";
        }

        @SuppressWarnings("rawtypes")
		@Override
        public Iterator getPrefixes(String namespaceURI) {
               return null;
        }

	 };
	
	 public static XMLStreamWriter filter(Writer writer)
	               throws XMLStreamException {
		 XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(writer);
		 xmlStreamWriter.setNamespaceContext(EMPTY_NAMESPACE_CONTEXT);
		 
		 return xmlStreamWriter;
	 }


//	@Override
//	public String toString() {
//		String result = super.toString();
//		
//		if (result != null) {
//			result = result.replaceAll(" xmlns:xsi=\"[^\"]*\"", "");
//			result = result.replaceAll(" xsi:type=\"[^\"]*\"", "");
//			result = result.replaceAll(" xsi:nil=\"true\"", "");
//		}
//		
//		return result;
//	}
	
	public static void main(String[] args) throws Exception {
//		System.out.println(ZoneId.systemDefault());
		
//		final String maxBinary = "1111111111111111111111111111111";
//		System.out.println(Integer.parseInt(maxBinary, 2));
//
		String secret = "The quick brown fox jumps over the lazy dog";
		String secretKey = "key";
		String encoded = null;
		String charsetName = "US-ASCII";
		String algorithm = "HmacSHA256";
		
		try {
			final Charset ascii = Charset.forName(charsetName);
			SecretKeySpec secretKeySpec = new SecretKeySpec(ascii.encode(secretKey).array(), algorithm);
			Mac mac = Mac.getInstance(algorithm);
	    	mac.init(secretKeySpec);
	    	byte[] hmacData = mac.doFinal(ascii.encode(secret).array());
	    	encoded = new String(Hex.encode(hmacData));
		} catch (NoSuchAlgorithmException | InvalidKeyException e) {
			e.printStackTrace();
		}
		
		System.out.println(encoded);
		
//		final String newSecret = UUID.randomUUID().toString().replaceAll("-", "");
//		System.out.println(newSecret);
//		String nonce = Base64.getEncoder().encodeToString(newSecret.getBytes(Charset.forName("UTF-8")));
//		System.out.println(nonce);

//		String xml = "<xsi:request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
//			    "<dataSource>Client</dataSource>" +
//			    "<operationType>fetch</operationType>" +
//			    "<startRow>0</startRow>" +
//			    "<endRow>75</endRow>" +
//			    "<textMatchStyle>substring</textMatchStyle>" +
//			    "<sortBy>name</sortBy>" +
//			    "<componentId>listGrid</componentId>" +
//			    "<data xsi:type=\"AdvancedCriteria\">" +
//			    	"<operator>and</operator>" +
//			    	"<criteria>" +
//			    		"<elem xsi:type=\"Object\">" +
//			    			"<fieldName>name</fieldName>" +
//			    			"<operator>iNotContains</operator>" +
//			    			"<value>qqq</value>" +
//			    		"</elem>" +
//			    		"<elem xsi:type=\"Object\">" +
//			    			"<fieldName>apiKey</fieldName>" +
//			    			"<operator>iStartsWith</operator>" +
//			    			"<value>www</value>" +
//			    		"</elem>" +
//			    	"</criteria>" +
//			    "</data>" +
//			    "<oldValues></oldValues>" +
//			"</xsi:request>";
		
//		String xml = "<request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
//			    "	<dataSource>Client</dataSource>" +
//			    "	<operationType>add</operationType>" +
//			    "	<textMatchStyle>exact</textMatchStyle>" +
//			    "	<componentId>editForm</componentId>" +
//			    "	<data xsi:type=\"client\">" +
//			    "		<apiKey>c5f804c6-2c9c-31d6-2e13-05871f338986</apiKey>" +
//			    "		<secretKey>be4e9145-8e32-97ed-0b0b-461327287bab</secretKey>" +
//			    "		<name>qwerty</name>" +
//			    "	</data>" +
//			    "</request>";
		
//		String xml = "<xsi:request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
//		"	<dataSource>User</dataSource>" +
//		"	<operationType>update</operationType>" +
//		"	<textMatchStyle>exact</textMatchStyle>" +
//		"	<componentId>listGrid</componentId>" +
//		"	<data xsi:type=\"map\">" +
//		"		<id>51d0d975-799d-4457-837d-51b3753da254</id>" +
//		"		<profile xsi:type=\"Object\">" +
//		"			<skype>qwerty</skype>" +
//		"			<mobilePhone>8 (910) 821-19-95</mobilePhone>" +
//		"			<homePhone>+7 (432) 352-60-48</homePhone>" +
//		"			<address xsi:type=\"Object\">" +
//		"				<city>Переславль</city>" +
//		"				<street>Октябрьская</street>" +
//		"				<house>39А</house>" +
//		"			</address>" +
//		"		</profile>" +
//		"	</data>" +
//		"</xsi:request>";
		
//		String xml = "<transaction>" +
//				"		<operations>" +
//				"			<request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
//				"				<dataSource>User</dataSource>" +
//				"				<operationType>fetch</operationType>" +
//				"				<endRow>75</endRow>" +
//				"				<textMatchStyle>exact</textMatchStyle>" +
//				"				<componentId>listGrid</componentId>" +
//				"				<sortBy>email</sortBy>" +
//				"				<data xsi:type=\"map\">" +
//				"				</data>" +
//				"			</request>" +
//				"		</operations>" +
//				"	</transaction>";
		
//		String json = "{\"transaction\":{\"operations\":[{\"request\":{\"dataSource\":\"User\",\"operationType\":\"fetch\",\"startRow\":0,\"endRow\":75,\"textMatchStyle\":\"exact\",\"componentId\":\"listGrid\",\"sortBy\":[\"email\"],\"data\":{}}}]}}";
//		String json = "{\"transaction\":{\"operations\":{\"request\":{\"startRow\":0,\"endRow\":20}}}}";
		
//		JAXBContext jc = JAXBContext.newInstance(IC.ISC_MODEL_PACKAGE + ":" + IC.ISC_COMMON_JAXB_PACKAGE);
//
//		IscSingleRequest request = new IscSingleRequest<>();
//		IscAdvancedCriteria criteria = new IscAdvancedCriteria();
//		IscCriterion criterion = new IscCriterion();
//		criterion.setOperator(IC.TEXT_MATCH_ISTARTWITH);
//		criterion.setValue("qwerty");
//		criterion.setFieldName("skype");
//		criteria.addCriterion(criterion);
//		request.setData(criteria);
//		
//		Marshaller marshaller = jc.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//		marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
//		marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON_VALUE);
//
//
//		Writer writer = new StringWriter();
//		marshaller.marshal(request, writer);
//		String xml = writer.toString();
//		System.out.println(xml);

//		Unmarshaller unmarshaller = jc.createUnmarshaller();
//		IscRequest iscRequest = (IscRequest) unmarshaller.unmarshal( new StringReader( xml ) );
//		System.out.println(iscRequest.toString());
		
//		JSONObject jsonObject = new JSONObject(json);
//		XMLStreamReader xmlStreamReader = new MappedXMLStreamReader(jsonObject, convention);
//		Unmarshaller unmarshaller = jc.createUnmarshaller();
//		IscRequestBatch requestBatch = (IscRequestBatch) unmarshaller.unmarshal(xmlStreamReader);
//		System.out.println(requestBatch);
		
//		IscRequestBatch requestBatch = new IscRequestBatch<>();
//		List<IscRequest> iscRequests = new ArrayList<>();
//		iscRequests.add(new IscRequest<>());
//		requestBatch.setRequests(iscRequests);
//		
//		IscResponseBatch responseBatch = new IscResponseBatch<>();
//		responseBatch.add(new IscFetchResponse<>());

	}

}
