package com.isysmod.modeling.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.isysmod.common.IC;
import com.isysmod.common.jaxb.isc.IscErrorResponse;

@Controller
@RequestMapping(method = RequestMethod.GET, value = "/error/")
public class ErrorController {
	
	@RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public ModelAndView html(
			@ModelAttribute("errorCode") String errorCode,
			@ModelAttribute("errorMessage") String errorMessage) {
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("errorCode", errorCode);
		modelAndView.addObject("errorMessage", errorMessage);
		
		return new ModelAndView("error");
	}
	
	@RequestMapping(params = IC.ISC_DATA_FORMAT_PARAMETER + "=xml", produces = MediaType.TEXT_XML_VALUE)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public IscErrorResponse xml(
			@ModelAttribute("errorCode") Integer errorCode,
			@ModelAttribute("errorMessage") String errorMessage) {
		
		return new IscErrorResponse(errorCode, errorMessage);
	}
	
	@RequestMapping(params = IC.ISC_DATA_FORMAT_PARAMETER + "=json", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public IscErrorResponse json(
			@ModelAttribute("errorCode") Integer errorCode,
			@ModelAttribute("errorMessage") String errorMessage) {
		
		return new IscErrorResponse(errorCode, errorMessage);
	}
	
}
