package com.isysmod.modeling.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DispatcherController {
	
	private MessageSource messageSource;

	private String skin = "Enterprise";
	
	private Boolean jsDebug = false;
	
	public DispatcherController(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView welcomePage(@RequestParam(name = "skin", required = false) String skin) {
		if (skin != null) {
			this.skin = skin;
		}
		
		ModelAndView model = new ModelAndView();
		model.addObject("skin", this.skin);
		model.addObject("debug", this.jsDebug ? true : false);
		model.setViewName("isc");
		
		return model;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
    		@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			HttpServletRequest request)
	{
		ModelAndView model = new ModelAndView();
		model.addObject("skin", this.skin);
		model.addObject("debug", this.jsDebug ? "-debug" : "");
		
		if (request.getParameter("error") != null) {
			model.addObject("serverMessage", messageSource.getMessage("login.error", null, LocaleContextHolder.getLocale()));
		} else if (request.getParameter("logout") != null) {
			model.addObject("serverMessage", messageSource.getMessage("login.logout", null, LocaleContextHolder.getLocale()));
		}
		
		model.setViewName("login");
		//model.setViewName("login.failsave");
		
		return model;
    }

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public void setJsDebug(Boolean jsDebug) {
		this.jsDebug = jsDebug;
	}

}
