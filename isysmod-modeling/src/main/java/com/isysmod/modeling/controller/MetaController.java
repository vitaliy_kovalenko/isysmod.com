package com.isysmod.modeling.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.security.Principal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.isysmod.modeling.i18n.FreemarkerMessageResolver;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Controller
public class MetaController {

	private FreeMarkerConfigurer freemarkerConfig;
	
	private FreemarkerMessageResolver messageResolver;
	
	private String suffix = ".ftl";
	
	private static final String[] ANONYMOUSLY_ALLOWED_TEMPLATES = new String[] {"constants" , "messages"};
	
	private static final String[] CONSTRUCTOR_TEMPLATES = new String[] {"constants", "messages", "datasources"};
	
	public MetaController(FreeMarkerConfigurer freemarkerConfig, FreemarkerMessageResolver messageResolver) {
		this.freemarkerConfig = freemarkerConfig;
		this.messageResolver = messageResolver;
	}
	
	@RequestMapping(
			value = "/meta",
			method = RequestMethod.GET,
			produces = "application/javascript;charset=UTF-8")
	public @ResponseBody String meta(
			HttpServletRequest request,
			Principal principal) throws IOException, TemplateException {
		Locale locale = LocaleContextHolder.getLocale();
		messageResolver.setLocale(locale);
		CsrfToken token = (CsrfToken) request.getAttribute("_csrf");
		Map<String,Object> map = new HashMap<>();
		map.put("messages", messageResolver);
		map.put("_csrf", token);
		map.put("username", principal.getName());
		
		Configuration configuration = freemarkerConfig.getConfiguration();
		StringWriter writer = new StringWriter();
		
		for (String tplName : CONSTRUCTOR_TEMPLATES) {
			writer.write("//--> " + tplName + suffix + "\n");
			Template template = configuration.getTemplate(tplName + suffix);
			template.process(map, writer);
			writer.write("//<-- " + tplName + suffix + "\n");
		}
		
		return writer.toString();
	}
	
	@RequestMapping(
			value = "/anonymous", // Constructor login page
			method = RequestMethod.GET,
			produces = "application/javascript;charset=UTF-8")
	public @ResponseBody String anonymous(
			HttpServletRequest request) throws IOException, TemplateException {
		Locale locale = LocaleContextHolder.getLocale();
		messageResolver.setLocale(locale);
		CsrfToken token = (CsrfToken) request.getAttribute("_csrf");
		Map<String,Object> map = new HashMap<>();
		map.put("messages", messageResolver);
		map.put("_csrf", token);
		map.put("username", "Guest");
		
		Configuration configuration = freemarkerConfig.getConfiguration();
		StringWriter writer = new StringWriter();
		
		for (String tplName : ANONYMOUSLY_ALLOWED_TEMPLATES) {
			writer.write("//--> " + tplName + suffix + "\n");
			Template template = configuration.getTemplate(tplName + suffix);
			template.process(map, writer);
			writer.write("//<-- " + tplName + suffix + "\n");
		}
		
		return writer.toString();
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

}
