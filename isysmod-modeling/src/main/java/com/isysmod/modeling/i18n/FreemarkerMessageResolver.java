package com.isysmod.modeling.i18n;

import java.util.List;
import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

import freemarker.template.TemplateMethodModel;
import freemarker.template.TemplateModelException;

public class FreemarkerMessageResolver implements TemplateMethodModel {

	private MessageSource messageSource;
	
	private Locale locale = Locale.ENGLISH;
	
	public FreemarkerMessageResolver(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	@SuppressWarnings("rawtypes") 
	public Object exec(List arguments) throws TemplateModelException {
		if (arguments.size() < 1) {
			throw new TemplateModelException("Wrong number of arguments.");
		}
		
		String code = (String) arguments.get(0);
		Object[] params = arguments.toArray();
		System.arraycopy(params, 1, params, 0, params.length - 1);
		String message;
		
		try {
			message = messageSource.getMessage(code, params, locale);
		} catch (NoSuchMessageException e) {
			message = code;
		}
		
		return message;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

}
