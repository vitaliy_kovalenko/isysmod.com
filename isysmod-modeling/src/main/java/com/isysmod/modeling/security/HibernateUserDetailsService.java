package com.isysmod.modeling.security;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.isysmod.common.model.User;
import com.isysmod.common.security.UserPrincipal;

public class HibernateUserDetailsService implements UserDetailsService {

	Logger logger = Logger.getLogger(HibernateUserDetailsService.class.getName());
	
	private SessionFactory sessionFactory;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		User user = null;
		
		if (username != null && username.length() > 0) {
			Session s = this.sessionFactory.getCurrentSession();
			user = (User) s.createCriteria(User.class)
				.add(Restrictions.eq("username", username))
				.uniqueResult();
		}
		
		if (user == null) {
			logger.info("Authentication failed for user " + username + ". No such user.");
			throw new UsernameNotFoundException( "User " + username + " not found" );
		}
		
		logger.info("Found user " + user.getUsername());
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority( "ROLE_AUTHENTICATED" ));
		
		UserPrincipal principal = new UserPrincipal(user.getUsername(), user.getPassword(), true, true, true, true, authorities);
		principal.setId(user.getId());
		
		return principal;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
