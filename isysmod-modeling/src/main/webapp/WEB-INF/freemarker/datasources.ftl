isc.IsysDataSource.create({
	ID:"Profile",
	database:"modeling",
	beanClassName: "Profile",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, canEdit:false, disabled:true, hidden:true},
		{name:"birthDate", title:"Birth Date", type:"date", defaultChooserDate:null},
		{name:"skype", title:"Skype"},
		{
			name:"mobilePhone", title:"Mobile phone",
			type:"phoneNumber", mask:"# (###) ###-##-##", hint:"# (###) ###-##-##", showHintInField:true <#-- maskSaveLiterals:true не работает поиск-->
		},
		{	name:"homePhone", title:"Home phone",
			type:"phoneNumber", mask:"+# (###) ###-##-##", hint:"+# (###) ###-##-##", showHintInField:true
		},
		{name:"address", type:"Address", dataPath:"address", canFilter:false}
	],
	requestProperties:{httpHeaders:isc.headers}
});

isc.IsysDataSource.create({
	ID:"Address",
	database:"modeling",
	beanClassName: "Address",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, canEdit:false, disabled:true, hidden:true},
		{name:"postcode", title:"Postcode", type:"number", titleOrientation:"top"},
		{name:"country", title:"Country", titleOrientation:"top"},
		{name:"province", title:"Province", titleOrientation:"top"},
		{name:"city", title:"City", titleOrientation:"top"},
		{name:"street", title:"Street", titleOrientation:"top"},
		{name:"house", title:"House", titleOrientation:"top"},
		{name:"apartment", title:"Apartment", titleOrientation:"top"}
	],
	requestProperties:{httpHeaders:isc.headers}
});

isc.IsysDataSource.create({
	ID:"User",
	database:"modeling",
	beanClassName: "User",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	allowAdvancedCriteria:true,
	fields:[
		{title:"ID", name:"id", type:"text", primaryKey:true, canEdit:false, disabled:true, hidden:true, width:"*", canEdit:false},
		{title:"${messages('Username')}", name:"username", type:"text", required:true, frozen:true, width:"*"},
		{title:"${messages('Password')}", name:"pubPassword", type:"password", required:true, width:"*"},
		{title:"Email", name:"email", required:true, width:"*"},<#-- The input element's type ('email') does not support selection -->
		{name:"profile", type:"Profile", hidden:true, canEdit:false, canFilter:false, canView:false},
		{name:"skype", includeFrom:"Profile.skype", dataPath:"profile/skype", criteriaPath:"profile", width:"*"},
		{
			name:"mobilePhone", title:"Mobile Phone",
			includeFrom:"Profile.mobilePhone", dataPath:"profile/mobilePhone", criteriaPath:"profile",
			type:"phoneNumber", mask:"# (###) ###-##-##", hint:"# (###) ###-##-##", showHintInField:true,
			width:"*"
		},
		{
			name:"homePhone", title:"Home Phone",
			includeFrom:"Profile.homePhone", dataPath:"profile/homePhone", criteriaPath:"profile",
			type:"phoneNumber", mask:"+# (###) ###-##-##", hint:"+# (###) ###-##-##", showHintInField:true,
			width:"*"
		},
		{
			name:"address", type:"Address", dataPath:"profile/address",
			formatCellValue:formatComplexCellValue,
			minWidth:300
		},
		{
			name:"birthDate", title:"Birth Date",
			includeFrom:"Profile.birthDate", dataPath:"profile/birthDate", criteriaPath:"profile",
			type:"date", startDate:"1/1/1930", useTextField:true, useMask:true
		},
		{
			name:"age", type:"number",
			canFilter:false, canEdit:false, shouldSaveValue:false,
			userFormula:{formulaVars:{A:"birthDate"}, text:"record.profile.birthDate ? record.profile.birthDate.relativeYear() : ''"},
			width:50
		}
	],
	sortBy: [{ property: "username", direction: "ascending" }],
	joins: [
		{
			owner:"User",
			field:"profile",
			select:true,
			type:"outer",
			joins: [
				{
					owner:"Profile",
					field:"address",
					select:true,
					type:"outer"
				}
			]
		}
	],
	requestProperties:{httpHeaders:isc.headers}
});

isc.IsysDataSource.create({
	ID:"Client",
	database:"modeling",
	beanClassName: "Client",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{title:"ID", name:"id", type:"text", primaryKey:true, canEdit:false, disabled: true, hidden:true, width:"*", canEdit:false},
		{title:"${messages('Name')}", name:"name", type:"text", required:true, frozen:true, width:"*"},
		{startRow:true, title:"${messages('Public_key')}", name:"apiKey", type:"text", required:true,
			icons:[{src:"[APPFILES]/icons/16/edit-6.png",
				click:"isc.uuid(form, item, icon)",
				prompt:"${messages('GenerateKey')}"}],
				width:"*"},
		{title:"${messages('Private_key')}", name:"secretKey", type:"text", required:true,
			icons:[{src:"[APPFILES]/icons/16/edit-6.png",
				click:"isc.uuid(form, item, icon)",
				prompt:"${messages('GenerateKey')}"}],
				width:"*"},
		<#include "./change_tracking.ftl">
	],
	sortBy: [{ property: "name", direction: "ascending" }],
	requestProperties:{httpHeaders:isc.headers},
	relations:[
		{type:"manyToMany", title:"${messages('Databases')}", foreignDataSource:"Database", foreignKey:"clients.id",
			manyToManyConfig:{
			relationBean:"ClientDatabase",
			selfKey:"clientId",
			foreignKey:"databaseId"}
		}
	]
});

isc.IsysDataSource.create({
	ID:"Database",
	database:"modeling",
	beanClassName: "Database",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{title:"ID", name:"id", type:"text", primaryKey:true, canEdit:false, disabled:true, hidden:true, width:"*", canEdit:false},
		{title:"${messages('Name')}", name:"name", type:"text", required:true, frozen:true, width:"*"},
		{title:"${messages('Code')}", name:"code", type:"text", required:true, width:"*"},
		{startRow:true, title:"URL", name:"url", type:"text", required:true, width:"*"}
	],
	sortBy: [{ property: "name", direction: "ascending" }],
	requestProperties:{httpHeaders:isc.headers},
	relations:[
		{type:"manyToMany", title:"${messages('Clients')}", foreignDataSource:"Client", foreignKey:"databases.id",
			manyToManyConfig:{
			relationBean:"ClientDatabase",
			selfKey:"databaseId",
			foreignKey:"clientId"}
		},
		{type:"oneToMany", title:"Entities", foreignDataSource:"Entity", foreignKey:"databaseId"}
	]
});

isc.IsysDataSource.create({
	ID:"ClientDatabase",
	database:"modeling",
	beanClassName: "ClientDatabase",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{title:"ID", name:"id", type:"text", primaryKey:true, canEdit:false, disabled:true, hidden:true, width:"*", canEdit:false},
		{
			title:"${messages('Client')}", name:"clientId", type:"text", required:true, width:"*",
			editorType: "IsysComboBoxItem", wrapTitle: false,
			//filterEditorProperties: {
			//	grid properties
			//},
			//Option Properties
			optionDataSource: "Client",
			valueField:"id",
			displayField:"name",
			sortField:"name",
			pickListFields:[{name:"name"}],
			addUnknownValues:false,
			allowEmptyValue:false,
			//valueXPath:"client/name", //это говно ломает форму
			displayValueXPath:"client/name" // Мое свойство
		},
		{
			title:"${messages('Database')}", name:"databaseId", type:"text", required:true, width:"*",
			editorType: "IsysComboBoxItem", wrapTitle: false,
			//Option Properties
			optionDataSource: "Database",
			valueField:"id",
			displayField:"name",
			sortField:"name",
			pickListFields:[{name:"name"}],
			addUnknownValues:false,
			allowEmptyValue:false,
			displayValueXPath:"database/name"
		},
		{title:"${messages('Enabled')}", name:"isEnabled", type:"boolean", width:80}
	],
	// Сортировку в запросе создает IsysDataSource.transformRequest, исходя из наличия sortBy и displayValueXPath
	sortBy: [{property:"clientId", direction:"ascending"},{property:"databaseId", direction:"ascending"}],
	//groupBy: ["clientId"],
	requestProperties: {httpHeaders:isc.headers},
	relations:[
		{type:"manyToOne", title:"${messages('Client')}", foreignDataSource:"Client", foreignKey:"clientId"},
		{type:"manyToOne", title:"${messages('Database')}", foreignDataSource:"Database", foreignKey:"databaseId"}
	]
});

isc.IsysDataSource.create({
	ID:"Group",
	database:"modeling",
	beanClassName: "UserGroup",
	dataFormat:isc.dataFormat,
	dataURL:isc.dataURL,
	fields:[
		{name:"id", primaryKey:true, hidden:true, width:"*"},
		{name:"name", treeField:true, title:"${messages('Name')}", required:true, frozen:true, width:"*"},
		{
			name:"parentId", title:"Parent", detail:true,
			editorType: "pickTree",
			dataSource:"Group",
			foreignKey:"id",
			valueField:"id",
			displayField:"name",
			sortField:"name",
			canSelectParentItems:true
		},
		<#include "./change_tracking.ftl">
	],
	sortBy: [{property:"name", direction:"ascending"}],
	requestProperties: {httpHeaders:isc.headers}
});

isc.IsysDataSource.create({
	ID:"Entity",
	database:"modeling",
	beanClassName: "Entity",
	fields:[
		{name:"id", hidden:true, primaryKey:true, width:"*"},
		{name:"name", title:"${messages('Name')}", required:true, width:"*"},
		{
			title:"${messages('Database')}", name:"databaseId", type:"text", required:true, width:"*",
			editorType: "IsysComboBoxItem", wrapTitle: false,
			//Option Properties
			optionDataSource: "Database",
			valueField:"id",
			displayField:"name",
			sortField:"name",
			pickListFields:[{name:"name"}],
			addUnknownValues:false,
			allowEmptyValue:false,
			displayValueXPath:"database/name"
		},
		{name:"code", title:"${messages('Code')}", required:true, width:"*"}/***,
		{
			detail:true, name:"description",
			editorType:"RichTextItem", filterEditorType:"TextItem",
			showTitle:true,
			bulletControls:["indent", "outdent", "orderedList", "unorderedList"],
			controlGroups:["formatControls", "styleControls", "bulletControls"],
			height:200, width:"100%"
		}***/
	],
	relations:[
		{type:"oneToMany", title:"Entity Fields", foreignDataSource:"EntityField", foreignKey:"entityId"}
	],
	sortBy: [{ property: "name", direction: "ascending" }],
	requestProperties:{httpHeaders:isc.headers}
});

isc.IsysDataSource.create({
	ID:"EntityField",
	database:"modeling",
	beanClassName: "EntityField",
	fields:[
		{name:"id", primaryKey:true, hidden:true, canEdit:false},
		{name:"entityId", foreignKey:"Entity.id", hidden:true, required:true},
		{name:"name", title:"${messages('Name')}", required:true, width:"*"},
		{name:"code", title:"${messages('Code')}", required:true, width:"*"}
	],
	sortBy: [{ property: "name", direction: "ascending" }],
	requestProperties:{httpHeaders:isc.headers}
});

isc.DataSource.create({
	ID:"MenuDS",
	clientOnly: true,
	fields:[
		{name:"id", primaryKey:true, hidden:true, required:true},
		{name:"parentId", foreignKey:"MenuDS.id", hidden:true},
		{name:"datasource", hidden:true, required:true},
		{name:"name", title:"Название", required:true}
	],
	testData:[
		{ name:"${messages('Users')}", id:"UserItem", datasource:"User", icon:"[APPFILES]/icons/16/administrator.png", parentId:null, isFolder:false },
		{ name:"${messages('Clients')}", id:"ClientItem", datasource:"Client", icon:"[APPFILES]/icons/16/network.png", parentId:null, isFolder:false },
		{ name:"${messages('Databases')}", id:"DatabaseItem", datasource:"Database", icon:"[APPFILES]/icons/16/data-configuration.png", parentId:null, isFolder:true,
			childrenProperties:{
				Database: {datasource:"Entity", isFolder:true, foreignKey:"databaseId"},
				Entity: {datasource:"EntityField", isFolder:false, foreignKey:"entityId"}
			}
		},
		{ name:"${messages('Clients_Databases')}", id:"ClientDatabaseItem", datasource:"ClientDatabase", icon:"[APPFILES]/icons/16/servers-network.png", parentId:null, isFolder:false},
		{ name:"Groups", id:"Groups", datasource:"Group", parentId:null, isFolder:false}
	]
});
