/*** ChangeTracking ***/
{
	startRow:true,
	name:"createdById", title:"Created By", valueXPath:"createdBy/id", type:"creator", width:"*",
	editorType: "IsysComboBoxItem", wrapTitle: false,
	optionDataSource: "User",
	valueField:"id",
	displayField:"username",
	sortField:"username",
	pickListFields:[{name:"username"}],
	addUnknownValues:false,
	allowEmptyValue:false,
	displayValueXPath:"createdBy/username",
	canEdit:false
},
{
	name:"updatedById", title:"Updated By", valueXPath:"updatedBy/id", type:"modifier", width:"*",
	editorType: "IsysComboBoxItem", wrapTitle: false,
	optionDataSource: "User",
	valueField:"id",
	displayField:"username",
	sortField:"username",
	pickListFields:[{name:"username"}],
	addUnknownValues:false,
	allowEmptyValue:false,
	displayValueXPath:"updatedBy/username",
	canEdit:false
},
<#-- some comment...
{name:"createdByUsername", type:"text", canView:false, valueXPath:"createdBy/username"},
{name:"createdById", title:"Created By",
	//editorType:"ComboBoxItem", wrapTitle: false,
	foreignKey:"User.id",
	displayField:"createdByUsername",
	pickListFields:[{name:"username"}],
	sortField:"username",
	pickListProperties: {
		dataFetchMode: "paged"
	},
	filterEditorProperties: {
		autoFetchData: false
	}
},
{name:"updatedByUsername", type:"text", canView:false, valueXPath:"updatedBy/username"},
{name:"updatedById", title:"Updated By",
	//editorType:"ComboBoxItem", wrapTitle: false,
	foreignKey:"User.id",
	displayField:"updatedByUsername",
	pickListFields:[{name:"username"}],
	sortField:"username",
	pickListProperties: {
		dataFetchMode: "paged"
	},
	filterEditorProperties: {
		autoFetchData: false
	}
},
-->
{name:"createdAt", type:"creatorTimestamp", canEdit:false, canFilter:true},
{name:"updatedAt", type:"modifierTimestamp", canEdit:false, canFilter:true}
