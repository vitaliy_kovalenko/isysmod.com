isc.headers = {
	"${_csrf.headerName}":"${_csrf.token}"
};

isc.dataFormat = "xml";
isc.serverType = "hibernate";
isc.csrf = {};
isc.csrf.headerName = "${_csrf.headerName}";
isc.csrf.parameterName = "${_csrf.parameterName}";
isc.csrf.token = "${_csrf.token}";
//isc.dataURL = "/isc/";
isc.dataURL = "/isc/debug";
isc.RPCManager.actionURL = isc.dataURL;
isc.RPCManager.credentialsURL = "/login";
isc.setAutoDraw(false);
isc.Comm.formatedOutput = true;
