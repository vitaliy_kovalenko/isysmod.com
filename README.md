## Цели проекта
1. Предоставить разработчику инструмент работы с уровнем данных, который позволяет без написания кода создавать
новый проект и подключать клиента к уже дествующей системе. В которой уже интегрированы:  
	* полнотекстовый поиск
	* распределенные транзакции
	* серверная валидация данных
	* кеширование данных
	* экспорт / импорт данных в различные форматы
	* почтовые и др. сервисы
	* файловое хранилище

2. Оптимизировать, ускорить выборку данных и снизить нагрузку на уже действующие серверы, реализованные на
популярных платформах, таких как Birtix и WordPress, за счет механизма кеширования и полнотектового поиска.
При этом проект подключается к действующей системе параллельно, не нарушая работы уже имеющегося функционала.

3. Предоставить административную часть для новых проектов с максимально возможным функционалом, отвечающую
требованиям большинства заказчиков.

***

## Задачи проекта
* Ориентация на уровень предприятия, как потенциального заказчика (XA-транзакции, иерархические группы пользователей и т.п.)
* Обеспечить производительность не мнее 1000 запросов в секунду (в то время, как на PHP едва ли достигается 100)
* Максимально минимизировать затраты на выделяемую память машины за счет отказа от использования J2EE серверов

***

## Структура проекта
#### Проект состоит из трех основных частей:
1. Конструктор (Доступ для разработчкиов)
2. Администратор (Доступ для администраторов и контент-менеджеров)
3. Службы (Доступ для клиентов: сайтов, мобильных и десктопных приложений)

#### И дополнительных:
1. Статический контент (js, css, images, etc)
2. Системная шина (обеспечивает взаимодействие между *Конструктора* и *Служб*)
3. Генератор модели (создает Java классы по маппингу)
4. Обратная инженерия (создает маппинг по таблицам БД)

*Конструктор* — сайт, клиентская часть реализована на SmartClient LGPL, серверная на Spring +
Hibernate + JAXB. Позволяет конструировать модели БД для служб, хранить и выдавать мета-информацию о них.
А также редактировать модели, созданные посредством *Обратной инженерии*.

*Администратор* — опциональный сайт, может быть развернут и использован заказчиком, как дефолтная административная
часть управления контентом и настройками клиента. Получает мета-информацию о моделях клиента от *Служб*
и редактирует контент через *Службы*. Использование целесообразно, если модель создана посредством *Конструктора*.
В случае *Обратной инженерии* целесообразность использования сомнительна, так как вероятно административная
часть уже существует у клиента, поскольку имеем дело с действующей БД. Kлиентская часть реализована на SmartClient
LGPL, серверная одностраничная (скрипты подключаются от *Статический контент*), может быть развернута на сайте
клинета (если клиент — сайт).

*Службы* — JAX-RS сервисы, интегрирующие распределенные источники данных, основная часть проекта. Отличительной
особенностью является горячее подключение модели, позволяещее редактировать модель без необходимости перезапуска
сервера (использован J2EE JPA контейнер и реализована логика его перезапуска). Сериализация моделей реализована
на JAXB, что крайне удобно для разработки мобильных приложений — позволяет импортировать в приложение байт-код
модели и не требует библиотек сторонних провайдеров (реализация JAXB предоставлена в JRE). Среди источников
данных, всегда есть соединение с основным, который предоставляет аутентификационные данные клиентов. В процессе
аутентификации и авторизации клиента определяются модели, доступ к которым разрешен данному клиенту.

***

## Используемые технологии
#### List of J2EE 1.3 specifications - JSR58
* Java Naming and Directory Interface (JNDI) - 1.2  
Provider: Apache https://tomcat.apache.org/tomcat-8.0-doc/jndi-resources-howto.html

#### List of Java EE 6 specifications - JSR316
* Java Servlet 3.0 - JSR315  
Provider: Apache CXF http://cxf.apache.org/
* Java Transaction API (JTA) 1.1 - JSR907  
Provider: Bitronix https://github.com/bitronix/btm

#### List of Java EE 7 specifications - JSR342
* Java Persistence API (JPA) 2.1 - JSR338  
Provider: Hibernate http://hibernate.org/
* Bean Validation	1.1 - JSR349  
Provider: Hibernate http://hibernate.org/
* Java Message Service API (JMS) 2.0 - JSR343  
Provider: Apache ActiveMQ http://activemq.apache.org/ 
* Dependency Injection for Java (CDI) 1.0 - JSR330  
Provider: JBoss Weld http://weld.cdi-spec.org/
* Java API for RESTful Web Services (JAX-RS) 2.0 - JSR339  
Provider: Apache CXF http://cxf.apache.org/
* Java Architecture for XML Binding (JAXB) 2.2 - JSR222  
Provider: Oracle https://docs.oracle.com/javase/tutorial/jaxb/intro/
* Java API for JSON Processing - JSR353  
Provider: Jettison

#### UUID
* RFC 4122 http://www.ietf.org/rfc/rfc4122.txt

### XA transactional caches
http://www.ehcache.org/documentation/3.0/xa.html

#### What is supported and what are the limitations
* Bitronix TM 2.1.4 is the only supported JTA implementation. Other JTA implementations may work but have not yet been tested.
* Read-Committed is the only supported isolation level.
* The isolation level is guaranteed by the use of the Copier mechanism. When no copiers are configured for either the key or the value, default ones are automatically used instead. You cannot disable the Copier mechanism for a transactional cache.
* Accessing a cache access outside of a JTA transaction context is forbidden.
* There is no protection against the ABA problem.
* Everything else works orthogonally.
